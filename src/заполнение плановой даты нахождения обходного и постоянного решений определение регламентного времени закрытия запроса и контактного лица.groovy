//Скрипт на заполнение плановой даты нахождения обходного и постоянного решений, определение регламентного времени закрытия запроса и контактного лица
def contact = 'clientName'
def clientEmail = 'clientEmail'
def clientPhone = 'clientPhone'
// Переводим время в милисекунды
def ms = subject.resolutionTime.toMiliseconds()
//Создаем переменную, которая хранит необходимый временной интервал умноженный на коэффициент
def resolTime30 = (ms * 0.30) / 1000 / 60
def resolTime50 = (ms * 0.50) / 1000 / 60
// Создаем новый временной промежуток
def intervalWA = new ru.naumen.common.shared.utils.DateTimeInterval(resolTime30.longValue(), 'MINUTE')
def intervalSol = new ru.naumen.common.shared.utils.DateTimeInterval(resolTime50.longValue(), 'MINUTE')
// Редактируем новый временной промежуток
// serviceTime элемент или код элемента справочника "Классы обслуживания"
def SERVICE_TIME = 'serviceTime';
def regworkAroundDate = 'diskPrDate'
def regSolvedDate = 'solPrDate'
def deadLineTime = 'deadLineTime'
// timeZone элемент или код элемента справочника "Часовые пояса"
// startTime время от которого необходимо начать отсчитывать период обслуживания (Date)
// interval период обслуживания (кол-во милисекунд, целое число)
// Массив атрибутов
def attrs = [:]
attrs[regworkAroundDate] = api.timing.serviceEndTime(subject[SERVICE_TIME], subject.timeZone, subject.registrationDate, intervalWA.toMiliseconds())
attrs[regSolvedDate] = api.timing.serviceEndTime(subject[SERVICE_TIME], subject.timeZone, subject.registrationDate, intervalSol.toMiliseconds())
attrs[deadLineTime] = api.timing.serviceEndTime(subject[SERVICE_TIME], subject.timeZone, subject.registrationDate, subject.resolutionTime.toMiliseconds())
// Заполняем контактное лицо: куратор сервиса
attrs[contact] = subject.failedService.responsibleTeam.leader.title
attrs[clientEmail] = subject.failedService.responsibleTeam.leader.email
attrs[clientPhone] = subject.failedService.responsibleTeam.leader.cityPhoneNumber
utils.edit(subject, attrs)