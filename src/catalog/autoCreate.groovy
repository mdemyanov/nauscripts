// package catalog
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Создание элементов справочника
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def CATALOG_CODE = 'urgency';
def ELEMENTS = ['1': 'Минимальны',
        '2': 'Средний',
        '3': 'Высокий'];
//main------------------------------------------------------------
ELEMENTS.each {
    try {
        utils.create(CATALOG_CODE, ['code': it.key,
                'title': it.value]);
    }
    catch (Exception e) {
        logger.info(e);
    }
}
return "Сценарий окончен"