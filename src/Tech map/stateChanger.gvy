//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT_ATTR = 'serviceCall'; /*АТрибут-ссылка на ГО*/
def OBJECT = subject[OBJECT_ATTR]; /*Копируем ГО в атрибут*/
def CLASS_TO_FIND = ['task$tasksInSCalls', 'negotiation$callNegot']; /*Типы объектов для поиска*/
def CURRENT_STAGE = OBJECT['stage']; /*Номер текущего этапа*/
def TARGET_STATE = 'techMap'; /*Целевой статус: куда переводить ГО*/
/*Коллекция статусов, в которых могут находиться подчиненные объекты*/
def STATES = ['registered','inAction','delayed','inWork','paused'];
/*Параметры для поиска подчиненных объектов: ссылка на ГО и статусы*/ 
def PARAMS = [
	'serviceCall' : OBJECT,
	'state' : STATES
];
/*Контейнер для найденых объектов*/
def OBJECTS = [];
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК------------------------------------------------------------
log("Этап ${CURRENT_STAGE}: Поиск незавершенных работ для запроса ${OBJECT.title}");
CLASS_TO_FIND.each{objClass->
	log("Этап ${CURRENT_STAGE}: Поиск незавершенных ${objClass}");
	OBJECTS.addAll(utils.find(objClass, PARAMS));
}
log("Этап ${CURRENT_STAGE}: Найдено ${OBJECTS?.size()} объектов");
OBJECTS.each{
	log("Этап ${CURRENT_STAGE}: Найден объект ${it?.title}");

};
/*Если есть незакрытые незавершенные работы, кроме текущего, то ничего не делаем*/
if(OBJECTS?.size() > 1) {
	log("Этап ${CURRENT_STAGE}: есть незакрытые задачи или другие объекты в действии");
	return '';
} else {
	log("Этап ${CURRENT_STAGE}: Все работы завершены, объект переводится на следующий этап");
	try {
		CURRENT_STAGE++;
		api.tx.call{
			utils.edit(OBJECT, ['state' : TARGET_STATE,
			'stage' : CURRENT_STAGE]);
		};		
	}
	catch(Exception e) {
		log("Объект не изменен по причине: ${e.toString()}");
	}
	return '';
}
return '';