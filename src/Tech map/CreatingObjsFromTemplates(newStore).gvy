//Автор: mdemyanov
//Дата создания: 19.05.2013
//Назначение:
/**
 * 
 */
//Версия: 4.0.10
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject; /*Записываем головной объект в атрибут*/
def TECH_MAP = OBJECT['techMap']; /*Запишем ссылку на тех. карту*/
def CURRENT_STAGE = OBJECT['stage']; /*Зафиксируем текущий этап работ (1 по умолчанию)*/
def TEMPLATES_CLASS = 'template'; /*Класс (тип) шаблона*/
/*Параметры для поиска шаблона: карта и номер этапа*/
def TEMPLATES_FIND_PARAMS = ['parent' : TECH_MAP, 'stageNumber' : CURRENT_STAGE];
def TEMPLATES = []; /*Коллекция с шаблонами работ на текущем этапе*/
def CREATED_OBJECTS = []; /*Коллекция созданных объектов*/
def FINISH_STATE = 'resolved';
def WORK_STATE = 'working';

//ФУНКЦИИ--------------------------------------------------------------
/*Создает согласование из шаблона*/
def negotiationCreator = { template->
	/*Определение метакласса создаваемого объекта, если null - то объект не будет создан*/
	def metaClass = template.type.code ?: null;
	log("Подготовка к созданию: ${metaClass}");
	/**Атрибуты для копирования:
	* Здесь указывается соответсвие копируемых атрибутов:
	* в key (первый атрибут) - значение из шаблона,
	* в value (второй) - атрибут в создаваемом объекте
	*/
	def attrsToCreate = [
		'serviceTime' : 'serviceTime', /*Класс обслуживания*/
		'title' : 'title', /*Название*/
		'negotiationTM' : 'negotiationTM', /*Время на выполнение*/
		'description' : 'description', /*Описание*/
		'cabTeams' : 'cabTeams', /*Кабинет: команда*/
		'cabOU' : 'cabOU', /*Кабинет: отдел*/
		'cabEmploers' : 'cabEmploers', /*Кабинет: сотрудник*/
		'responsibleTeam' : 'responsibleTeam', /*ОТВ команда*/
		'responsibleEmployee' : 'responsibleEmployee' /*ОТВ сотрудник*/
	];
	def attrs = [:]; /*Контейнер, в который записываются значения атрибутов*/
	/*Запись значений атрибутов контейнер*/
	log("Произведем запись значений атрибутов в контейнер.");
	attrsToCreate.each{ attr->
		attrs.put(attr.value, template[attr.key]);
	}
	/*Заполним ссылку на запрос*/
	attrs.put('serviceCall', OBJECT);
	def object;
	log("Создание объекта типа: ${metaClass}");
	try {
		object = utils.create(metaClass, attrs);
	}
	catch(Exception e) {
		log("Объект не создан по причине: ${e.toString()}");
	}
	return object;
};
/*Создает задачу из шаблона*/
def taskCreator = { template->
	/*Определение метакласса создаваемого объекта, если null - то объект не будет создан*/
	def metaClass = template.type.code ?: null;
	log("Подготовка к созданию: ${metaClass}");
	/**Атрибуты для копирования:
	* Здесь указывается соответсвие копируемых атрибутов:
	* в key (первый атрибут) - значение из шаблона,
	* в value (второй) - атрибут в создаваемом объекте
	*/
	def attrsToCreate = [
		'serviceTime' : 'serviceTime', /*Класс обслуживания*/
		'title' : 'title', /*Название*/
		'shortDeskr' : 'shortDeskr', /*Краткое описание*/
		'timeToDo' : 'timeToDo', /*Время на выполнение*/
		'description' : 'description', /*Описание*/
		'responsibleTeam' : 'responsibleTeam', /*ОТВ команда*/
		'responsibleEmployee' : 'responsibleEmployee' /*ОТВ сотрудник*/
	];
	def attrs = [:]; /*Контейнер, в который записываются значения атрибутов*/
	/*Запись значений атрибутов контейнер*/
	log("Произведем запись значений атрибутов в контейнер.");
	attrsToCreate.each{ attr->
		attrs.put(attr.value, template[attr.key]);
	}
	/*Заполним ссылку на запрос*/
	attrs.put('serviceCall', OBJECT);
	def object;
	log("Создание объекта типа: ${metaClass}");
	try {
		object = utils.create(metaClass, attrs);
	}
	catch(Exception e) {
		log("Объект не создан по причине: ${e.toString()}");
	}
	return object;
};
/*Создает задачу из шаблона*/
def costWorkCreator = { template->
	/*Определение метакласса создаваемого объекта, если null - то объект не будет создан*/
	def metaClass = template.type.code ?: null;
	log("Подготовка к созданию: ${metaClass}");
	/**Атрибуты для копирования:
	* Здесь указывается соответсвие копируемых атрибутов:
	* в key (первый атрибут) - значение из шаблона,
	* в value (второй) - атрибут в создаваемом объекте
	*/
	def attrsToCreate = [
		'serviceTime' : 'serviceTime', /*Класс обслуживания*/
		'title' : 'title', /*Название*/
		'shortDeskr' : 'shortDeskr', /*Краткое описание*/
		'cost' : 'cost', /*Стоимость*/
		'timeToDo' : 'timeToDo', /*Время на выполнение*/
		'description' : 'description', /*Описание*/
		'responsibleTeam' : 'responsibleTeam', /*ОТВ команда*/
		'responsibleEmployee' : 'responsibleEmployee' /*ОТВ сотрудник*/
	];
	def attrs = [:]; /*Контейнер, в который записываются значения атрибутов*/
	/*Запись значений атрибутов контейнер*/
	log("Произведем запись значений атрибутов в контейнер.");
	attrsToCreate.each{ attr->
		attrs.put(attr.value, template[attr.key]);
	}
	/*Заполним ссылку на запрос*/
	attrs.put('serviceCall', OBJECT);
	def object;
	log("Создание объекта типа: ${metaClass}");
	try {
		object = utils.create(metaClass, attrs);
	}
	catch(Exception e) {
		log("Объект не создан по причине: ${e.toString()}");
	}
	return object;
};
/*Замыкание для определения последнего номера этапа*/
def maxStage = {
	/*Поиск всех шаблонов по текущей тех. карте*/
	def templates = utils.find(TEMPLATES_CLASS, ['parent' : TECH_MAP]);
	log("Этап ${CURRENT_STAGE}: найдено ${templates.size()} этапов");
	def stages = []; /*Контейнер для хранения всех этапов*/
	templates.each{template->
		stages.add(template.stageNumber);		
	}
	log("Этап ${CURRENT_STAGE}: номер последнего этапа ${stages.max()}");
	return stages.max() ?: 'Этапы не определены';
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
/*Поиск шаблонов для создания*/
TEMPLATES = utils.find(TEMPLATES_CLASS, TEMPLATES_FIND_PARAMS);
if (!TEMPLATES) {
	log("Этап ${CURRENT_STAGE}: шаблоны не найдены, завершаем работу");
	try {
		utils.edit(OBJECT, ['state' : FINISH_STATE,
			'stageDescr' : "Работы по карте ${TECH_MAP.title} завершены",
			'stage' : 1]);
	}
	catch(Exception e) {
		log("Объект не изменен по причине: ${e.toString()}");
	}	
} else {
	TEMPLATES.each{ template->
		switch(template.metaClass.getCase()) {
			case 'task': CREATED_OBJECTS.add(taskCreator(template));				
			break
			case 'costWork': CREATED_OBJECTS.add(costWorkCreator(template));				
			break
			case 'negotiation': CREATED_OBJECTS.add(negotiationCreator(template));				
			break
			default:
				log("Невозможно создать объект по шаблону ${template.title}");
			break
		}
	};
	log("Этап ${CURRENT_STAGE}: создано ${CREATED_OBJECTS.size()} обектов");
	log("Этап ${CURRENT_STAGE}: объект ${OBJECT.title} переводится в дежурный статус");
	try {
		utils.edit(OBJECT, ['state' : WORK_STATE,
			'stageDescr' : "Этап ${CURRENT_STAGE} из ${maxStage()}.",
			'stage' : CURRENT_STAGE]);
	}
	catch(Exception e) {
		log("Объект не изменен по причине: ${e.toString()}");
	}
	
}
return '';