/*! UTF8 */
/*& 3600 cache */
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Код: PartNumber из naupp
//Назначение:
/**
 * назначение скрипта в виде краткого словесного описания
 */
//Версия: 4.0.1.x
//Категория:

// Логгирование:
/*По умолчанию логгирование выключено*/
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК--------------------------------------------------------
def scalls = utils.find('serviceCall$incidentCall', ['state': 'closed']);
scalls.each { scall ->
    def cies = scall.units;
    api.tx.call {
        cies.each {
            utils.edit(it, ['scalls': null, 'tasks': null]);
        };
    };
    api.tx.call {
        scalls.each { scall ->
            try {
                utils.edit(scall, ['toIssue': null, 'removed': true]);
                //utils.delete(scall);
            }
            catch (Exception e) {

            }
        };
    };
};
