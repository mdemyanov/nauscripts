// package Collegues.CH_O
//Автор: sefimov
//Дата создания: 25.03.2013
//Код: script_computing_responsible_supporting
//Назначение:
/**
 * Назначение ответственного запроса в зависимости 
 * от субъекта поддержки с учетом централизованности
 */
//Версия: 4.0
//Категория: Статусы запроса, действие на вход/выход из статуса
//ФУНКЦИИ--------------------------------------------------------------
// ответственный за услугу
def serviceResponsible(def service) {
    return [responsibleEmployee: service.responsibleEmployee, responsibleTeam: service.responsibleTeam];
}

// ответственный из отвественного за субъект поддержки (объект)
def supportResponsible(def supportRespObj, def attrCode) {
    return [responsibleEmployee: supportRespObj["${attrCode}_em"], responsibleTeam: supportRespObj["${attrCode}_te"]];
}

//ОСНОВНОЙ БЛОК--------------------------------------------------------
/*Статусы (коды), из которых должно происходить назначение ответсвенного*/
def STATES = ['firstLine', 'firstline2'];
/*Значение предыдущего статуса (код)*/
def LAST_STATE = oldSubject.state;

//пользовательские атрибуты
def SERVICE_TYPE = 'typeService'
def SC_INFSYS = 'infSys';
def SC_SUPPORT = 'support';
def SUPPORTED_ATTR = 'Supported';

def SUPPORTED = 'supported';
//Ответственные за субъект поддержки услуги и ИС
def RESP_PARTN_GROUP = 'RespPartnGroup$RespPartnGroup';
def RESP_IS = 'RespPartnGroup$RespIS';
//Агрегирующимй атрибут ответственного
def RESP_PART_GR = 'respPartGr';

def infSys = subject[SC_INFSYS];
def service = subject.service;
def newResponsibleAttrs;


if (service[SERVICE_TYPE]) {
    if (infSys == null) {
        newResponsibleAttrs = serviceResponsible(service);
    } else {
        newResponsibleAttrs = infSys.responsible ? serviceResponsible(infSys) : serviceResponsible(service);
    }
} else {
    def scSuppObjects = utils.find(SUPPORTED, [(SUPPORTED): subject[SC_SUPPORT]]);
    if (scSuppObjects.size() != 1) {
        logger.warn("для элемента справочника ${subject[SC_SUPPORT].code} найдено ${scSuppObjects.size()} объектов типа ${SUPPORTED}}");
        return;
    }

    if (infSys != null) {
        def list = utils.find(RESP_IS, [(SUPPORTED_ATTR): scSuppObjects.get(0), service: infSys]);
        if (list.size() != 0) {
            newResponsibleAttrs = supportResponsible(list.get(0), RESP_PART_GR);
        }
    }
    if (newResponsibleAttrs == null) {
        def list = utils.find(RESP_PARTN_GROUP, [(SUPPORTED_ATTR): scSuppObjects.get(0), service: service]);
        if (list.size() == 0) {
            logger.warn("не найдено ни одного отвественного за субъект поддержки.");
            return;
        }
        newResponsibleAttrs = supportResponsible(list.get(0), RESP_PART_GR);
    }
}
if (STATES.contains(LAST_STATE)) {
    logger.info("Назначение не производится, так как запрос пришел не из целевого статуса");
    return '';
} else {
    utils.edit(subject, newResponsibleAttrs);
    return '';
}