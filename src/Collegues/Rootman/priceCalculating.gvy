//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
def MANIPULATIONS = subject['manipulation'];
def PRICE_CODE = 'price';
//ФУНКЦИИ--------------------------------------------------------------
def priceSum = {manipulations->
	def cost = 0.0;
	manipulations.each{manipulation->
		cost+=manipulation[PRICE_CODE];
	}
	log("Общая стоимость составляет ${cost}");
	return cost;
} 
//ОСНОВНОЙ БЛОК------------------------------------------------------------
try {
	log("Запись значения в объект ${OBJECT?.title}");
	utils.edit(OBJECT, ['cost' : priceSum(MANIPULATIONS)]);
}
catch(Exception e) {
	log("Стоимость не рассчитанна по причине: ${e.toString()}")
}
return '';
