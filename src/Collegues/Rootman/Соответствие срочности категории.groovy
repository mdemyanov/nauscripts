// package Collegues.Rootman
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def URGENCY_CATALOG_CODE = 'urgency';
def URGENCY_HI = ['1cAccess'];
def URGENCY_MEDIUM = ['1cPass'];
def URGENCY_LOW = ['1cSetting'];
def CATEGORY = subject.callCategory;
def URGENCY = utils.get(URGENCY_CATALOG_CODE, ['code': 'medium']);
//main------------------------------------------------------------
logger.info("!!!!!!!!!!!!!!!!!!! start")
if (URGENCY_HI.contains(CATEGORY) :
URGENCY = utils.get (URGENCY_CATALOG_CODE, ['code': 'hi'])
break
case URGENCY_MEDIUM.contains (CATEGORY):
URGENCY = utils.get (URGENCY_CATALOG_CODE, ['code': 'medium'])
break
case URGENCY_LOW.contains (CATEGORY):
URGENCY = utils.get (URGENCY_CATALOG_CODE, ['code': 'low'])
break
}
logger.info("!!!!!!!!!!!!!!!!!!! ok")
utils.edit(subject, ['urgency': URGENCY]);