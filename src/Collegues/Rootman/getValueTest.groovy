// package Collegues.Rootman

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.naumen.advimport.server.engine.AdvImportException;
import ru.naumen.advimport.server.engine.datasource.AbstractDataSourceProvider;
import ru.naumen.advimport.shared.config.datasource.Column;
import ru.naumen.monitoring.server.MonitoringConnectionManager;
import ru.naumen.monitoring.server.SDContextUtils;
import ru.naumen.monitoring.server.advimport.datasource.MonitoringDataSourceProviderBean;
import ru.naumen.monitoring.server.RootContextConstant;
import ru.naumen.monitoring.shared.advimport.datasource.MonitoringDataSource;

import ru.naumen.metainfo.shared.ClassFqn;
import ru.naumen.commons.shared.utils.StringUtilities;
import ru.naumen.core.shared.Constants;
import ru.naumen.core.server.script.ScriptService;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;

import com.tibbo.aggregate.common.context.Context;
import com.tibbo.aggregate.common.context.ContextException;
import com.tibbo.aggregate.common.context.ContextManager;
import com.tibbo.aggregate.common.context.ContextUtils;
import com.tibbo.aggregate.common.datatable.DataRecord;
import com.tibbo.aggregate.common.datatable.DataTable;
import com.tibbo.aggregate.common.server.ServerContextConstants;

def rlc = beanFactory.getBean('monitoringConnectionManagerImpl');
def scriptService = beanFactory.getBean(ScriptService.class);
SDContextUtils contextUtils = beanFactory.getBean('SDContextUtils');
//
ContextManager contextManager = rlc.getContextManager();
Context device1 = contextManager.get('users.admin.devices.Cent_OS');
def des = device1.getDescription();
//Context root = contextManager.getRoot();
//def address = 'address';
def queryStr = '';
queryStr = '';
def value = contextUtils.queryData(device1, "$queryStr");
return des.toString();