//Автор: mdemyanov
//Дата создания: 09.10.2012
//Назначение:
/**
 * Создание голосований в согласовании при входе в состояние "В процессе"
 * При настройке технологу необходмо указать код типа голосования
 * в формате "vote$type_code" в атрибуте VOTE_FQN
 */
//Версия: 4.0
//Категория: создание вложенных объектов
// Атрибут "Тип голосования"
def VOTE_FQN = 'vote$ratVote'
// Атрибут "согласование источник""
def PARENT = 'parent'
// Дата голосования
def VOTE_DATE = 'voteDate'
//Контейнер атрибутов
def attrs = [:]
// Назначаем согласование родитель (текущий объект)
attrs[PARENT] = subject
// Заполняем плановю дату голосования
attrs[VOTE_DATE] = subject.deadLine
// Создаем голосования для сотрудников
if (subject.cabEmploers) {
    def voterEmployee = 'voter_em'
    subject.cabEmploers.each({ voter ->
        attrs[voterEmployee] = voter
        utils.create(VOTE_FQN, attrs)
        attrs[voterEmployee] = null
    })
}
// Создаем голосования для отделов
if (subject.cabOU) {
    def voterOU = 'voter_ou'
    subject.cabOU.each({ voter ->
        attrs[voterOU] = voter
        utils.create(VOTE_FQN, attrs)
        attrs[voterOU] = null
    })
}
// Создаем голосования для команд
if (subject.cabTeams) {
    def voterTeam = 'voter_te'
    subject.cabTeams.each({ voter ->
        attrs[voterTeam] = voter
        utils.create(VOTE_FQN, attrs)
        attrs[voterTeam] = null
    })
} else return ''
return ''