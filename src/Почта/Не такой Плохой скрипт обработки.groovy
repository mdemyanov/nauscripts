/*! UTF-8 */
/**
 * Категория: Стандартный скрипт обработки почты для SD4.0.
 * Назначение:
 * Ищет запрос по номеру в теме письма, если находит - прикрепляет письмо к нему.
 * Если в скрипте указан префикс (в параметре SCALL_CHAR), то поиск по номеру запроса из темы будет удачным только в том случае, если число в теме идет после спец. символа, 
 * указанного в скрипте. По умолчанию значение параметра - "" (то есть будет осуществляться поиск без префикса)
 * Если в теме есть число с заданным символом перед ним (например, символом может быть "#"), тогда ищет запрос с таким номером, если находит - прикрепляет письмо к нему.
 * Если не находит - ищет сотрудника по обратному адресу и по адресу отправителя почтового сообщения.
 * Если не находит, то запрос создается с привязкой к сотруднику с фамилией в параметре 'DEFAULT_EMPLOYEE_NAME'.
 * Вычисляет тип запроса по адресу отправителя письма (соответствие задается в параметре 'SENDER_2_TYPE_CODE').
 * Если соответствие не задано, используется код дефолтного типа запроса (задается в параметре 'DEFAULT_TYPE_CODE').
 * Значения 'Уровня влияния' и 'Срочности' запроса задаются кодами элементов
 * (соотв. в параметрах 'IMPACT_CODE' и 'URGENCY_CODE');
 * Соглашение выбирается по номеру (указанному в параметре 'AGREEMENT_INVENTORY_NUMBER') среди соглашений,
 * получателем которых является найденный сотрудник.
 * Услуга выбирается по номеру (указанному в параметре 'SERVICE_INVENTORY_NUMBER') среди услуг,
 * поставляемых в рамках ранее определённого соглашения.
 *
 * Описание запроса - из тела письма. В случае, если тело пустое - определенный текст (в данном случае, "Описание запроса")."
 * Контактная информация - e-mail отправителя + контактные телефоны сотрудника.
 * Регистрирует запрос.
 * Проверяет вложения к запросу и прикрепляет письмо к нему.
 *
 * Автоматически переводит закрытый запрос в указанное состояние (по умолчанию отключено).
 * В случае невозможности обработки письма отправителю уходит уведомление с указанием причины невозможности обработки.
 * Ведется логирование всех действий скрипта (в консоли). Параметр isLoggingEnabled отвечает за логирование (false - отключено, true - включено, по умолчанию отключено)
 *
 * Результат работы скрипта должен устанавливаться в переменной окружения result.
 * Доступны следующие коды завершиения скрипта:
 * ERROR      ошибка обработки
 * NEW_BO     зарегистрирован запрос
 * ATTACH     письмо прикреплено к существующему запросу
 * REJECT     письмо отклонено
 * OUTGOING   другое (в настоящее время не используется)
 *
 * Версия: 4.0
 *
 * В контексте скрипта доступны сл. объекты:
 * <ul>
 * <li><b>message</b> - разобранное почтовое сообщение</li>
 * <li><b>result</b> - объект результатов обработки сообщения, может содержать рез-ты других обработчиков</li>
 * <li><b>api</b> - API со вспомогательными методами, полезными при обработке почтовых сообщений</li>
 * <li><b>utils</b> - утилитарные методы общего назначения - ru.naumen.core.server.script.spi.ScriptUtils</li>
 * <li><b>logger</b> - логгер скрипта</li>
 * </ul>
 */

//ПАРАМЕТРЫ---------------------------------------------------------------------------------------------------
def helper = api.mail.helper;
// Логирование основных событий, true - включено, false - выключено
def LOGGING_IS_ENABLED = true;
def SEND_BACK = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
/*Отправка ответных сообщений*/
def sendBack = { subject, body ->
    if (SEND_BACK) {
        api.mail.sender.send(message.from.address, message.from.name,
                "Re: ${message.subject}; $subject", "$body\n\n${helper.respondBody(message)}");
        log("Отправлено обратное письмо");
    } else {
        log("Отправление обратного письма: флаг SAND_BACK снят - письмо не отправлено.");
    }
}
log("Начата обработка сообщения");
// Символ, после которого идет номер запроса. Может быть не заполнен.
def SCALL_CHAR = "#";

//Письмо в запросе
def POST_FQN = 'post$post'      //код метакласса письма
def TYPE_CODE = 'type'          //код атрибута "Тип"
def FROM_CODE = 'fromC'         //код атрибута "От кого"
def TO_CODE = 'whom'            //код атрибута "Кому"
def CC_CODE = 'copy'            //код атрибута "Копия"
def DESCR_CODE = 'descr'        //код атрибута "Описание"
def FILES_CODE = 'files'        //код атрибута "Файлы"
def COPIES_CODE = 'copy'  //код атрибута запроса "Копия"

def POST_TYPE_FQN = 'postType'  //код справочника "Тип письма"
def POST_IN_CODE = 'in'
// уровень влияния, срочность, типы запроса, указанные ниже, должны быть настроены в системе
// код элемента справочника 'Уровни влияния'
def IMPACT_CODE = "normal";

//код уровня срочности
def URGENCY_CODE = "normal";

//соответствие e-mail адресов отправителей кодам типов запросов
def SENDER_2_TYPE_CODE = [
        "": "",
        "": ""
];

//код дефолтного типа запроса
def DEFAULT_TYPE_CODE = "rerviceReq";

//уникальный номер соглашения
def AGREEMENT_INVENTORY_NUMBER = "00000";

//уникальный номер услуги
def SERVICE_INVENTORY_NUMBER = "00000";

//идентификаторы типов сотрудников, на которых можно регистрировать запрос. Если пусто, то на любых.
EMPLOYEE_TYPES = ['employee', 'technician', 'contactName', 'businessUser'];  //типы записываются в кавычках через запятую.

// фамилия служебного сотрудника
DEFAULT_EMPLOYEE_NAME = "Служебный";

// Если запрос найден - код целевого состояния, в которое будет переведён запрос
def NEW_STATE = "resumed";

//Пареметры для заполнения атрибута "Способ обращения"
def REQUEST_WAY_ATTR = "wayOf"; // Код атрибута "Способ обращения" типа "Элемент справочника"
def REQUEST_WAY_CATALOG = "wayOfAddressing"; // Код справочника, в котором хранятся коды обращения
def REQUEST_WAY_VALUE = "email"; // Код элемента "По почте"

//ОСНОВНОЙ БЛОК----------------------------------------------------------------------------------------------
/**
 * Выбор подходящего по типу сотрудника 
 */
def getApplicableEmployee(def employees) {
    def result = [] as Set;
    employees.each
            {
                if (EMPLOYEE_TYPES.contains(it.getMetainfo().getCase()) || EMPLOYEE_TYPES.isEmpty()) {
                    result << it;
                }
            }
    if (1 < result.size()) {
        utils.throwReadableException("Найдено несколько подходящих сотрудников: " + result*.UUID + ". Работа скрипта остановлена. Запрос не зарегистрирован.");
    }
    return result.isEmpty() ? null : result.iterator().next();
}

/**
 * Получение сотрудника по фамилии (по умолчанию),
 * из допустимых типов EMPLOYEE_TYPES.
 */
def getDefaultEmployee = {
    log("Поиск сотрудника по умолчанию");
    def employee = helper.searchEmployeeByLastName(DEFAULT_EMPLOYEE_NAME);
    if (employee && !EMPLOYEE_TYPES.isEmpty() && !EMPLOYEE_TYPES.contains(employee.getMetainfo().getCase())) {
        log("Cотрудник по фамилии найден, но его тип не соответствует ни одному из списка EMPLOYEE_TYPES.");
        employee = null;
    }
    return employee;
}
/**
 * Поиск сотрудника по аресу отправителя
 */
def searchSenderEmployee =
    {
        log("Поиск сотрудника по адресу отправителя...");
        // Поиск сотрудников по адресу отправителя письма
        def employees = helper.searchEmployeesByEmail(message.from.address);
        // Выбор подходящего по типу сотрудника
        def employee = getApplicableEmployee(employees);
        // Если сотрудник не найден, ищем служебного сотрудника для привязки запроса к нему
        if (employee == null) {
            log("Сотрудник не найден.");
            employee = getDefaultEmployee();
        }
        return employee;
    }

/**
 * создание переписки
 */
def createPost = { serviceCall ->
    log("Создание объекта $POST_FQN")
    def postAttributes = [
            parent: serviceCall,
            (FROM_CODE): message.from.address,
            (TO_CODE): message.toAsString,
            (CC_CODE): message.ccAsString,
            (DESCR_CODE): "${message.subject}<br><br><br>${message.htmlBody}",
            (TYPE_CODE): utils.get(POST_TYPE_FQN, [code: POST_IN_CODE]),
    ]
    def post = utils.create(POST_FQN, postAttributes)
    log("Объект создан. Прикрепление файлов.")
    //Прикрепление файлов вложенных в письмо
    message.attachments.each {
        utils.attachFile(post, FILES_CODE, it.filename, it.contentType, "", it.data)
    }
    log("Файлы прикреплены")

    //Назначение атрибута запроса "Копии письма"
    //utils.edit(serviceCall, [ (COPIES_CODE) : postAttributes[CC_CODE] ])
}
/**
 * Регистрация запроса
 */
def createServiceCall =
    {
        // свойства запроса
        def props = [:];

        def employee = searchSenderEmployee();
        // Если сотрудник не найден, отклоняем письмо
        if (employee == null) {
            log("Сотрудник из списка требуемых типов не найден.");
            result.reject(api.mail.CLIENT_NOT_FOUND_REJECT_REASON);
            return null;
        }
        // Сотрудник найден
        log("Сотрудник найден: " + employee.title + ".");
        props.client = employee;

        // поиск соглашения
        def agreements = employee.recipientAgreements;
        // если соглашение только одно берём его
        def agreement = agreements.size() == 1 ? agreements.iterator().next() : null;
        for (a in agreements) {
            if (AGREEMENT_INVENTORY_NUMBER == a.inventoryNumber) {
                agreement = a;
                break;
            }
        }
        if (agreement == null) {
            log("Не найдено соглашение для сотрудника с uuid = " + employee.UUID + ".");
            result.reject(api.mail.OTHER_REJECT_REASON);
            return null;
        }
        // Соглашение найдено
        props.agreement = agreement;

        // Поиск Услуги
        def services = agreement.services;
        // если услуга только одна берём её
        def service = services.size() == 1 ? services.iterator().next() : null;
        for (s in services) {
            if (SERVICE_INVENTORY_NUMBER == s.inventoryNumber) {
                service = s;
                break;
            }
        }
        if (service == null) {
            log("Не найдена услуга для сотрудника с uuid = " + employee.UUID + " и соглашения с uuid =  " + agreement.UUID + ".");
            result.reject(api.mail.OTHER_REJECT_REASON);
            return null;
        }
        // Соглашение найдено
        props.service = service;

        log("Регистрация запроса...");
        // свойства запроса
        // найти тип запроса
        def typeCode = SENDER_2_TYPE_CODE[message.from.address];
        typeCode = typeCode != null ? typeCode : DEFAULT_TYPE_CODE;
        def fqn = api.types.newClassFqn('serviceCall', typeCode);
        // описание запроса - из тела письма
        props.descriptionRTF = api.string.isEmptyTrim(message.body) ? "Описание запроса" : message.body;
        // краткое описание описание запроса - из темы письма
        props.shortDescr = api.string.isEmptyTrim(message.getSubject()) ? "Без" : message.getSubject();
        // Временная зона для запроса - по умолчанию временная зона сервера
        def tzCode = TimeZone.getDefault().ID;
        props.timeZone = utils.get('timezone', ['code': tzCode]);
        // Уровень влияния
        props.impact = utils.get('impact', ['code': IMPACT_CODE]);
        // Срочность
        props.urgency = utils.get('urgency', ['code': URGENCY_CODE]);
        // Контактная информация из данных сотрудника
        props.clientName = employee.title;
        props.clientPhone = employee.phonesIndex;
        props.clientEmail = employee.email;
        if (api.metainfo.getMetaClass(fqn).hasAttribute(REQUEST_WAY_ATTR)) {
            try {
                props[REQUEST_WAY_ATTR] = utils.get((REQUEST_WAY_CATALOG), ['code': REQUEST_WAY_VALUE]);
            }
            catch (Exception e) {
                //Ничего не делаем, ловим Exception для того,
                //чтобы не появлялось ошибки при регистрации запроса, если справочника с кодом не существует
            }
        }
        // формируем новый запрос
        def serviceCall = utils.create(fqn.toString(), props);
        result.messageState = api.mail.NEW_BO_MSG_STATE;
        return serviceCall;
    }

def process =
    {
        // Ищем запрос по теме письма
        def scall;
        if (SCALL_CHAR.isEmpty()) {
            scall = helper.searchByCallNumber(message)
        } else {
            scall = helper.searchByCallNumberWithPrefix(message, SCALL_CHAR);
        }
        // Если запрос не найден, регистрируем новый
        if (scall == null) {
            log("Запрос не найден.");
            scall = createServiceCall();
            if (scall != null) {
                log("Зарегистрирован запрос №." + scall.number + ".");
                // helper.attachMessage(scall, message);
                helper.notifyMailReceived(scall, message);
                api.mail.sender.send(message.from.address, "", "Re: " + message.subject + "; Запрос № " + scall.number,
                        "\nПо вашему обращению зарегистрирован запрос № " + scall.number + "\n\n" + helper.respondBody(message));
            }
        } else {   // Запрос найден.
            // Добавляем комментарий к запросу
            def employee = searchSenderEmployee();
            if (employee != null) {
                //utils.create('comment', ['source' : scall.UUID, 'text' : message.body, 'author' : employee]);
                createPost(scall);
                // Переводим запрос в новое состояние (возобновляем, если закрыт).СЕЙЧАС ВОЗМОЖНОСТЬ ОТКЛЮЧЕНА! Для включения нужно убрать "//" в начале следующей строки
                // utils.edit(scall, ['state' : NEW_STATE]);
                sendBack('', "\nВаше письмо прикреплено к запросу №${scall.number}");
                //helper.attachMessage(scall, message);
                result.messageState = api.mail.ATTACH_MSG_STATE;
                //helper.notifyMailReceived(scall);
                /*api.mail.sender.send(message.from.address, "", "Re: " + message.subject,
                    "\nВаше письмо прикреплено к запросу № " + scall.number + "\n\n" + helper.respondBody(message));*/
            } else {
                api.mail.sender.send(message.from.address, "", "Re: " + message.subject,
                        "\nНе удалось определить отправителя \n\n" + helper.respondBody(message));
            }
        }
    }

// main
log("start");

/*if (result.rejected)
{
    log("reason is " + result.rejectReason);
    if (!helper.isSystemEmailAddress(message.from.address))
    {
        api.mail.sender.send(message.from.address, "", "Re: " + message.subject,
        helper.formRespondBody(result) + "\n\n" + helper.respondBody(message));
    }
    return;
}*/

try {
    process();
}
catch (Exception e) {
    result.error("Возникли ошибки обработки письма.");
    logger.error(e.message, e);
}
// Если были ошибки обработки или письмо отклонено, уведомляем отправителя
// if(!result.successfull && !helper.isSystemEmailAddress(message.from.address))
// {
//     api.mail.simpleSender.send(message.from.address, '', 'Re: ' + message.subject,
//     helper.formRespondBody(result) + "\n\n" + helper.respondBody(message));
//     return;
// }