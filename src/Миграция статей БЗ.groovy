//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: Скрипт миграции статей БЗ в новый класс-тип
/**
 *
 */
//Версия: 4.0
//Категория: 

//ПАРАМЕТРЫ------------------------------------------------------------
/*Найдем все существующие статьи*/
def articles = utils.find('articleKB', [:]);
/*Создадим счетчик статей. В начале равен нулю.*/
def articlesCount = 0;
/*Для каждой найденой статьи создадим новую (новый класс)*/
articles.each({ article ->
    utils.create('article$article', ['parent': article.parent, 'text': article.text,
            'title': article.title, 'author': article?.author]);
    articlesCount++;
})
return "Создано ${articlesCount} статей";