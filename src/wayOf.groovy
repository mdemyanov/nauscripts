// вычисление способа обращения
def wayOf = 'wayOf'
def attr = [:]
if (subject.author.UUID == subject.clientEmployee.UUID) {
    attr[wayOf] = utils.get('wayOfAddressing', ["code": "privateOffice"])
} else attr[wayOf] = utils.get('wayOfAddressing', ["code": "operator"])
utils.edit(subject, attr)
return ''