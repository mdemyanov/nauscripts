//Автор: ikozlov
//Дата создания: 31.05.2013
//Назначение:
/**
 * Скрипт возвращает список задач в зависимости от команды и от выбранного запроса
 */
//Версия: 4.0
//Категория: Фильтрация выпадающих списков

//ПАРАМЕТРЫ------------------------------------------------------------

def TASKS_CODE = 'task'; // Код класса задачи, элементы которого надо получить
if(null == subject)
{
    def ATTRS_FOR_UPDATE_ON_FORMS = ['serviceCall', 'team','datefrom', 'dateto'];
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
def OBJ = subject;

//ОСНОВНОЙ БЛОК--------------------------------------------------------
def objects = [];
def myQuery = api.db.query("""
	select tasks
	from team team
	join team.tasks tasks
	where team.id = :teamid and tasks.startTime between :datefrom and :dateto""");
myQuery.set('teamid', OBJ.team.UUID);
myQuery.set('datefrom', OBJ.datefrom);
myQuery.set('dateto', OBJ.dateto);
objects = myQuery.list();
return objects;