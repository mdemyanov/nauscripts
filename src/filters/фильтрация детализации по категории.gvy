//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
if(null==subject)
{
    def ATTRS_FOR_UPDATE_ON_FORMS = ['service','catogories'];
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
def categories = subject.catogories;
def objects = [];
try {
	objects = utils.find('categories$specification', ['category' : categories]);
}
catch(Exception e) {
	log(e);
}
return objects;