// package Prod
/*! UTF-8 */
/**
 * Категория: Стандартный скрипт обработки почты для SD4.0.
 * Назначение:
 * Если требуется найти запрос по данным из письма (SEARCH_SC = true), то
 * ищет запрос по номеру в теме письма, если находит - прикрепляет письмо к нему.
 * Если в скрипте указан префикс (в параметре SCALL_CHAR), то поиск по номеру запроса из темы будет удачным только в том случае, 
 * если число в теме идет после спец. символа, указанного в скрипте. 
 * По умолчанию значение параметра - "" (то есть будет осуществляться поиск без префикса)
 * Если в теме есть число с заданным символом перед ним (например, символом может быть "#"), 
 * тогда ищет запрос с таким номером, если находит - прикрепляет письмо к нему.
 * Если не находит - ищет сотрудника по обратному адресу и по адресу отправителя почтового сообщения.
 *
 * Если не находит (и разрешено создавать новые запросы (CREATE_NEW_SC = true),
 *  то запрос создается с привязкой к сотруднику с фамилией в параметре 'DEFAULT_EMPLOYEE_NAME'.
 * Вычисляет тип запроса по адресу отправителя письма (соответствие задается в параметре 'SENDER_2_TYPE_CODE').
 * Если соответствие не задано, используется код дефолтного типа запроса (задается в параметре 'DEFAULT_TYPE_CODE').
 * Значения 'Уровня влияния' и 'Срочности' запроса задаются кодами элементов
 * (соотв. в параметрах 'IMPACT_CODE' и 'URGENCY_CODE');
 * Соглашение выбирается по номеру (указанному в параметре 'AGREEMENT_INVENTORY_NUMBER') среди соглашений,
 * получателем которых является найденный сотрудник.
 * Услуга выбирается по номеру (указанному в параметре 'SERVICE_INVENTORY_NUMBER') среди услуг,
 * поставляемых в рамках ранее определённого соглашения.
 *
 * Описание запроса - из тела письма. В случае, если тело пустое - определенный текст (в данном случае, "Описание запроса")."
 * Контактная информация - e-mail отправителя + контактные телефоны сотрудника.
 * Регистрирует запрос.
 * Проверяет вложения к запросу и прикрепляет письмо к нему.
 *
 * Автоматически переводит запрос в указанный статус (если требуется).
 * Устанавливает ответственного за созданный запрос (если требуется).
 *
 * В случае невозможности обработки письма отправителю уходит уведомление с указанием причины невозможности обработки.
 * Ведется логирование всех действий скрипта (в консоли). Параметр isLoggingEnabled отвечает за логирование 
 * (false - отключено, true - включено, по умолчанию отключено)
 *
 * Результат работы скрипта должен устанавливаться в переменной окружения result.
 * Доступны следующие коды завершиения скрипта:
 * ERROR      ошибка обработки
 * NEW_BO     зарегистрирован запрос
 * ATTACH     письмо прикреплено к существующему запросу
 * REJECT     письмо отклонено
 * OUTGOING   другое (в настоящее время не используется)
 *
 * Версия: 4.0
 *
 * В контексте скрипта доступны сл. объекты:
 * <ul>
 * <li><b>message</b> - разобранное почтовое сообщение</li>
 * <li><b>result</b> - объект результатов обработки сообщения, может содержать рез-ты других обработчиков</li>
 * <li><b>api</b> - API со вспомогательными методами, полезными при обработке почтовых сообщений</li>
 * <li><b>utils</b> - утилитарные методы общего назначения - ru.naumen.core.server.script.spi.ScriptUtils</li>
 * <li><b>logger</b> - логгер скрипта</li>
 * </ul>
 */

//ПАРАМЕТРЫ---------------------------------------------------------------------------------------------------

// Символ, после которого идет номер запроса. Может быть не заполнен.
SCALL_CHAR = "";

// Параметры: уровень влияния, срочность, типы запроса, указанные ниже, должны быть настроены в системе!

// Код элемента справочника 'Уровни влияния'
IMPACT_CODE = "single";

// Код уровня срочности
URGENCY_CODE = "U2";

// Соответствие e-mail адресов отправителей кодам типов запросов
SENDER_2_TYPE_CODE = ["": "",
        "": ""];

// Код дефолтного типа запроса
DEFAULT_TYPE_CODE = "def";

// Уникальный номер соглашения
AGREEMENT_INVENTORY_NUMBER = "33";

// Уникальный номер услуги
SERVICE_INVENTORY_NUMBER = "33";

// Идентификаторы типов сотрудников, на которых можно регистрировать запрос. Если пусто, то на любых.
EMPLOYEE_TYPES = [];  //типы записываются в кавычках через запятую.

// Фамилия служебного сотрудника
DEFAULT_EMPLOYEE_NAME = "Служебный";

// Код целевого состояния, в которое будет переведён запрос
NEW_STATE = "new";

// Ответственный за запрос(сотрудник) (уникальный идентификатор сотрудника в виде 'employee$12345')
RESPONSIBLE_EMPLOYEE = '';

// Ответственный за запрос(команда) (уникальный идентификатор команды в виде 'team$10000') 
RESPONSIBLE_TEAM = '';

// Логические переменные:
SEARCH_SC = true;    // указывающая, требуется ли искать запрос по данным из письма
CREATE_NEW_SC = true;    // указывающая, требуется ли регистрировать новый запрос (если запрос не найден или искать не требуется)
ADD_COMMENT = true;    // указывающая, требуется ли добавить комментарий с текстом письма к найденному запросу

// Статусы запроса в скрипте (их исправлять не надо!).
SEARCHED = 'searched'; // Найден
CREATED = 'created';  // Создан
// В зависимости от статуса запроса в скрипте (Найден или Создан) можно раздельно указать дополнительные действия с запросом.
// Для этого в квадратные скобки нужно вставить соответствующее значение статуса (SEARCHED или CREATED)
CHANGE_STATE = [SEARCHED, CREATED]; // перевод запроса в новый статус.
SET_RESPONSIBLE = [SEARCHED, CREATED]; // установка ответственного
SET_ADDITIONAL = []; // дополнительные действия

// Параметры для заполнения атрибута "Способ обращения"
REQUEST_WAY_ATTR = "wayAddressing"; // Код атрибута "Способ обращения" типа "Элемент справочника"
REQUEST_WAY_CATALOG = "wayAddressing"; // Код справочника, в котором хранятся коды обращения
REQUEST_WAY_VALUE = "sd"; // Код элемента "По почте"

// Логирование основных событий, true - включено, false - выключено
LOGGING_IS_ENABLED = true;
log = { msg -> if (LOGGING_IS_ENABLED) logger.debug(msg); }

//ОСНОВНОЙ БЛОК----------------------------------------------------------------------------------------------

log("Скрипт обработки входящей почты: Начало.");
// Действия перед обработкой письма
actionsBeforeProcess();
if (result.rejected) {
    return;
}
// Обработка входящего письма
processIncomingMail();
// Действия после обработки письма
actionsAfterProcess();
log("Скрипт обработки входящей почты: Работа завершена.");

//ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ------------------------------------------------------------------------------------

/**
 * Действия, выполняемые перед обработкой письма
 */
def actionsBeforeProcess() {
    if (null == message.subject || message.subject.isEmpty()) {
        log("Входящее письмо не имеет темы!")
        SEARCH_SC = false;
    }
    if (null == message.body || message.body.isEmpty()) {
        log("Входящее письмо не имеет письменного содержания!");
    }
    // Если письмо отклонено - вывод причины
    if (result.rejected) {
        log("Причина, по которой письмо отклонено: " + result.rejectReason);
        if (!api.mail.helper.isSystemEmailAddress(message.from.address)) {
            api.mail.sender.send(message.from.address, "", "Re: " + message.subject,
                    api.mail.helper.formRespondBody(result) + "\n\n" + api.mail.helper.respondBody(message));
        }
    }
}

def processIncomingMail() {
    // Поиск запроса
    def serviceCall = searchServiceCall();
    def SCStateInScript = CREATED;

    if (null == serviceCall) {
        // Создание нового запроса
        serviceCall = createServiceCall();
    } else {
        SCStateInScript = SEARCHED;
        // Прикрепление письма к существующему запросу
        serviceCall = attachMail(serviceCall);
    }
    //Дополнительная обработка запроса
    additionalProcess(serviceCall, SCStateInScript);
}

/**
 * Поиск запроса по номеру из темы письма 
 */
def searchServiceCall() {
    def serviceCall;
    if (SEARCH_SC) {
        serviceCall = (SCALL_CHAR.isEmpty()) ?
            api.mail.helper.searchByCallNumber(message) :
            api.mail.helper.searchByCallNumberWithPrefix(message, SCALL_CHAR);
        log((serviceCall) ? "Запрос найден." : "Запрос не найден.");
    } else {
        log("/ Поиск запроса по данным из письма отменён (SEARCH_SC = false) /");
    }
    return serviceCall;
}

/**
 * Регистрация запроса
 */
def createServiceCall() {
    def serviceCall;
    if (CREATE_NEW_SC) {
        try {
            // Получение свойств запроса (заполнение необходимых атрибутов)
            log("Получение параметров для создания нового запроса...");
            def props = getProperties();
            // Создание запроса
            log("Регистрация запроса...");
            api.tx.call {
                serviceCall = utils.create(fqn.toString(), props);
                log("Зарегистрирован запрос №." + serviceCall.number + ".");
            }
        }
        catch (e) {
            log(e.message);
            log("Запрос не зарегистрирован! Возникли ошибки!");
            result.reject(api.mail.OTHER_REJECT_REASON);
            api.mail.sender.send(message.from.address, "", "Re: " + message.subject,
                    "\nЗапрос не зарегистрирован. Возникли ошибки при обработке письма.\n\n" + api.mail.helper.respondBody(message));
            return serviceCall;
        }
        // Запись в результат о том, что создан новый объект
        result.messageState = api.mail.NEW_BO_MSG_STATE;
        // Прикрепление исходного письма
        api.mail.helper.attachMessage(serviceCall, message);
        log("Письмо прикреплено.");
        // Добавление вложенных в письмо файлов
        def attachmentsSize = api.mail.helper.attachMessageAttachments(serviceCall, message).size();
        log("Добавлено вложенных файлов: ${attachmentsSize}");
        // Оповещение о поступлении письма
        api.mail.helper.notifyMailReceived(serviceCall);
        // Оповещение отправителя об успешной регистрации запроса
        api.mail.sender.send(message.from.address, "", "Re: " + message.subject + "; Запрос № " + serviceCall.number,
                "\nПо вашему обращению зарегистрирован запрос № " + serviceCall.number + "\n\n" + api.mail.helper.respondBody(message));
        log("Отправлено оповещение отправителю об успешной обработке письма.");
    } else {
        log("/ Регистрация нового запроса отменена пользователем (CREATE_NEW_SC = false) /")
    }
    return serviceCall;
}

/**
 * Заполнение атрибутов при создании запроса
 */
def getProperties() {
    // Cвойства запроса
    def props = [:];
    // Автор запроса
    def employee = getEmployee();
    props.client = employee;
    // Соглашение
    def agreement = getAgreement(employee);
    props.agreement = agreement;
    // Услуга
    props.service = getService(employee, agreement);
    // Тип запроса
    def fqn = getFqn();
    // Описание запроса (из тела письма)
    props.description = api.string.isEmptyTrim(message.body) ? "Описание запроса" : message.body;
    // Временная зона
    props.timeZone = getTimeZone(fqn);
    // Уровень влияния
    props.impact = utils.get('impact', ['code': IMPACT_CODE]);
    // Срочность
    props.urgency = utils.get('urgency', ['code': URGENCY_CODE]);
    // Контактная информация из данных сотрудника
    props.clientName = employee.title;
    props.clientPhone = employee.phonesIndex;
    props.clientEmail = employee.email;
    // Способ обращения
    props.putAll(getRequestWay(fqn));
    return props;
}

/**
 * Получение сотрудника - автора запроса
 */
def getEmployee() {
    def employee = searchEmployeeByEmail();
    // Если сотрудник не найден, отклоняем письмо
    if (employee == null) {
        result.reject(api.mail.CLIENT_NOT_FOUND_REJECT_REASON);
        utils.throwReadableException("Сотрудник из списка требуемых типов не найден.");
    }
    // Сотрудник найден
    log("Сотрудник найден: " + employee.title + ".");
    return employee;
}

/**
 * Поиск сотрудника по адресу отправителя
 */
def searchEmployeeByEmail() {
    log("Поиск сотрудника по адресу отправителя...");
    // Поиск сотрудников по адресу отправителя письма
    def employees = api.mail.helper.searchEmployeesByEmail(message.from.address);
    // Выбор подходящего по типу сотрудника
    def employee = getApplicableEmployee(employees);
    // Если сотрудник не найден, ищем служебного сотрудника для привязки запроса к нему
    if (employee == null) {
        log("Сотрудник не найден.");
        employee = getDefaultEmployee();
    }
    return employee;
}

/**
 * Выбор подходящего по типу сотрудника
 */
def getApplicableEmployee(def employees) {
    def totalResult = [] as Set;
    employees.each
            {
                if (EMPLOYEE_TYPES.contains(it.getMetainfo().getCase()) || EMPLOYEE_TYPES.isEmpty()) {
                    totalResult << it;
                }
            }
    if (1 < totalResult.size()) {
        utils.throwReadableException("Найдено несколько подходящих сотрудников: " + totalResult*.UUID + ". Работа скрипта остановлена. Запрос не зарегистрирован.");
    }
    return totalResult.isEmpty() ? null : totalResult.iterator().next();
}

/**
 * Получение сотрудника по фамилии (по умолчанию),
 * из допустимых типов EMPLOYEE_TYPES.
 */
def getDefaultEmployee() {
    log("Поиск сотрудника по фамилии " + DEFAULT_EMPLOYEE_NAME + "...");
    def employee = api.mail.helper.searchEmployeeByLastName(DEFAULT_EMPLOYEE_NAME);
    if (employee && !EMPLOYEE_TYPES.isEmpty() && !EMPLOYEE_TYPES.contains(employee.getMetainfo().getCase())) {
        log("Cотрудник по фамилии найден, но его тип не соответствует ни одному из списка EMPLOYEE_TYPES.");
        employee = null;
    }
    return employee;
}

/**
 * Получение соглашения
 */
def getAgreement(def employee) {
    def agreements = employee.recipientAgreements;
    // Если соглашение только одно - берём его
    def requiredAgreement = agreements.size() == 1 ? agreements.iterator().next() : null;
    for (agreement in agreements) {
        if (AGREEMENT_INVENTORY_NUMBER == agreement.inventoryNumber) {
            requiredAgreement = agreement;
            break;
        }
    }
    if (requiredAgreement == null) {
        utils.throwReadableException("Не найдено соглашение для сотрудника '${employee.title}'(uuid = " + employee.UUID + ").");
    }
    return requiredAgreement;
}

/**
 * Получение услуги
 */
def getService(def employee, def agreement) {
    def services = agreement.services;
    // если услуга только одна берём её
    def requiredsService = (1 == services.size()) ? services.iterator().next() : null;
    for (service in services) {
        if (SERVICE_INVENTORY_NUMBER == service.inventoryNumber) {
            requiredsService = service;
            break;
        }
    }
    if (requiredsService == null) {
        utils.throwReadableException("Не найдена услуга для сотрудника '${employee.title}'(uuid = " + employee.UUID + ") и соглашения '${agreement.title}'(uuid =  " + agreement.UUID + ").");
    }
    return requiredsService;
}

/**
 *  Получение типа запроса
 */
def getFqn() {
    def typeCode = SENDER_2_TYPE_CODE[message.from.address];
    typeCode = typeCode != null ? typeCode : DEFAULT_TYPE_CODE;
    return api.types.newClassFqn('serviceCall', typeCode);
}

/**
 * Получение временной зоны
 */
def getTimeZone(def fqn) {
    // Временная зона - берется из значения по умолчанию у типа запроса
    def timeZone = api.metainfo.getMetaClass(fqn)?.getAttribute('timeZone')?.defaultValue;
    def tzCode;
    if (timeZone) {
        tzCode = timeZone.code;
    } else {
        // Если значение по умолчанию в типе не установлено - берется временная зона сервера
        timeZone = TimeZone.getDefault();
        if (!timeZone) {
            //если нет возможности определить Временную зону сервера - выставляем зону с нулевым сдвигом.
            timeZone = TimeZone.getTimeZone("GMT-0");
        }
        tzCode = timeZone.ID;
    }
    return utils.get('timezone', ['code': tzCode]);
}

/**
 * Заполнение атрибута "Способ обращения" (если он существует)
 */
def getRequestWay(def fqn) {
    properties = [:];
    if (api.metainfo.getMetaClass(fqn).hasAttribute(REQUEST_WAY_ATTR)) {
        try {
            properties[REQUEST_WAY_ATTR] = utils.get((REQUEST_WAY_CATALOG), ['code': REQUEST_WAY_VALUE]);
        }
        catch (Exception e) {
            //Ничего не делаем, ловим Exception для того,
            //чтобы не появлялось ошибки при регистрации запроса, если справочника с кодом не существует
        }
    }
    return properties;
}

/**
 * Обработка найденного запроса
 */
def attachMail(def serviceCall) {
    log("Получение необходимых параметров для обработки запроса...");
    // Поиск сотрудника по адресу отправителя
    def employee = searchEmployeeByEmail();
    if (employee != null) {
        log("Сотрудник найден: " + employee.title + ".");
        // Прикрепление письма к запросу
        api.mail.helper.attachMessage(serviceCall, message);
        log("Письмо прикреплено.");
        // Добавление вложенных в письмо файлов
        def attachmentsSize = api.mail.helper.attachMessageAttachments(serviceCall, message).size();
        log("Добавлено вложенных файлов: ${attachmentsSize}");
        // Добавление комментария с текстом письма
        addComment(serviceCall, employee);
        // Запись в результат о том, что письмо было прикреплено к существующему объекту
        result.messageState = api.mail.ATTACH_MSG_STATE;
        // Оповещение о поступлении письма
        api.mail.helper.notifyMailReceived(serviceCall);
        // Оповещение отправителя об успешном прикреплении письма к запросу
        api.mail.sender.send(message.from.address, "", "Re: " + message.subject,
                "\nВаше письмо прикреплено к запросу № " + serviceCall.number + "\n\n" + api.mail.helper.respondBody(message));
        log("Отправлено оповещение отправителю об успешной обработке письма.");
    } else {
        log("Не удалось определить отправителя письма.")
        // Оповещение отправителя о неудаче
        api.mail.sender.send(message.from.address, "", "Re: " + message.subject,
                "\nНе удалось определить отправителя! \n\n" + api.mail.helper.respondBody(message));
        serviceCall = null;
    }
    return serviceCall
}

/**
 * Действия, выполняемые после обнаружения или создания запроса
 */
void additionalProcess(def serviceCall, def SCStateInScript) {
    if (!serviceCall) {
        log("Дополнительные операции с запросом (смена статуса или ответственного) отменены.");
        return;
    }
    // Перевод запроса в новый статус, если требуется
    changeStateIfRequired(serviceCall, SCStateInScript);
    // Установка ответственного за запрос (в обход стратегии ответственного)
    setResponsible(serviceCall, SCStateInScript);
    // Установка дополнительных свойств, определяемых в клиентских скриптах
    setAdditionalUserProperties(serviceCall, SCStateInScript);
}

/**
 * Добавление комментария к запросу
 */
void addComment(def serviceCall, def author) {
    if (!ADD_COMMENT) return;
    try {
        log("Добавление комментария...");
        api.tx.call {
            utils.create('comment', ['source': serviceCall.UUID, 'text': message.body, 'author': author]);
            log("Добавлен комментарий с текстом письма.");
        }
    }
    catch (e) {
        log("Возникли ошибки. Комментарий к запросу не добавлен.");
    }
}

/**
 * Перевод запроса в новый статус, если требуется (в отдельной транзакции)
 */
void changeStateIfRequired(def serviceCall, def SCStateInScript) {
    if (!CHANGE_STATE.contains(SCStateInScript)) return;
    log("Изменение статуса запроса...");
    try {
        api.tx.call {
            utils.edit(serviceCall, ['state': NEW_STATE]);
            log("Запрос переведён в статус '" + NEW_STATE + "'.");
        }
    }
    catch (e) {
        log("Возникли ошибки. Запрос не переведен в статус '${NEW_STATE}'.");
    }
}

/**
 * Назначение ответственного за запрос (в обход стратегии ответсвенного)
 */
void setResponsible(def serviceCall, def SCStateInScript) {
    if ((RESPONSIBLE_EMPLOYEE.isEmpty() && RESPONSIBLE_TEAM.isEmpty()) || !SET_RESPONSIBLE.contains(SCStateInScript)) return;
    log("Назначение ответственного...");
    def responsibleEmployee;
    def responsibleTeam;
    try {
        responsibleEmployee = getAndCheckObjectToSetResponsible(RESPONSIBLE_EMPLOYEE, 'employee', 'Ответственный(сотрудник)');
        responsibleTeam = getAndCheckObjectToSetResponsible(RESPONSIBLE_TEAM, 'team', 'Ответственный(команда)');
        api.tx.call {
            serviceCall = utils.edit(serviceCall, ['responsibleTeam': responsibleTeam, 'responsibleEmployee': responsibleEmployee]);
            log("Назначен ответственный за запрос: Сотрудник '" + (serviceCall.responsibleEmployee?.title ?: "") + "'/ Команда '" + (serviceCall.responsibleTeam?.title ?: "") + "'.");
        }
    }
    catch (e) {
        log(e.message);
        log("Возникли ошибки. Операция по назначению ответственного прервана.");
    }
}

/**
 * Получение и проверка на корректность объекта для установки его ответсвенным за запрос
 */
def getAndCheckObjectToSetResponsible(def uuid, def classId, def responsibleText) {
    if (uuid.isEmpty()) return null;
    def object = utils.load(uuid);
    if (null == object) {
        utils.throwReadableException("Назначение ответственного: Объект с идентификатором '" + uuid + "' не найден.");
    }
    def objectClass = object.metaClass.id;
    if (!utils.equal(objectClass, classId)) {
        utils.throwReadableException("Объект класса '" + objectClass + "' не может быть установлен в качестве '" + responsibleText + "'!");
    }
    return object;
}

/**
 * Установка дополнительных свойств, определяемых в клиентских скриптах
 */
void setAdditionalUserProperties(def serviceCall, def SCStateInScript) {
    if (!SET_ADDITIONAL.contains(SCStateInScript)) return;
    try {
        properties = getAdditionalProperties(serviceCall);
        api.tx.call {
            utils.edit(serviceCall, properties);
            log("Дополнительные свойства запроса успешно установлены.");
        }
    }
    catch (e) {
        log(e.message);
        log("Возникли ошибки. Операция по установке дополнительных свойств прервана.");
    }
}

/**
 * Конкретно установка свойств, определяемых в клиентских скриптах
 */
def getAdditionalProperties(def serviceCall) {

}

/**
 * Действия, выполняемые по результату обработки письма
 */
def actionsAfterProcess() {
    // Если были ошибки обработки или письмо отклонено - уведомление отправителя
    if (!result.successfull && !api.mail.helper.isSystemEmailAddress(message.from.address)) {
        api.mail.simpleSender.send(message.from.address, '', 'Re: ' + message.subject,
                api.mail.helper.formRespondBody(result) + "\n\n" + api.mail.helper.respondBody(message));
        log("Отправлено обратное оповещение об ошибках обработки письма.");
        return;
    }
}