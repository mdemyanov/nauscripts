//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def scall = utils.find('team', [:]);
def HTML = """<style type="text/css">
    #chartContainer {
        width: 300px;
        max-width: 700px;
        height: 300px;
    }
</style>
<div id="chartContainer" style="max-width:700px; width: 500px; height: 400px;">
    <iframe srcdoc="<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset=&quot;UTF-8&quot;>
    <script type=&quot;text/javascript&quot; src=&quot;http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.9.1.min.js&quot;></script>
    <script type=&quot;text/javascript&quot; src=&quot;http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js&quot;></script>
    <script type=&quot;text/javascript&quot; src=&quot;http://ajax.aspnetcdn.com/ajax/globalize/0.1.1/globalize.min.js&quot;></script>
    <script type=&quot;text/javascript&quot; src=&quot;http://cdn3.devexpress.com/jslib/13.1.5/js/dx.chartjs.js&quot;></script>
</head>
<body>
<div id=&quot;chartContainer&quot; style=&quot;max-width:700px;height: 300px;&quot;></div>
<script type=&quot;text/javascript&quot;>
    \$(function () {
        var model = {
            settings: {
                preset: &quot;preset1&quot;,

                geometry: {
                    startAngle: 180,
                    endAngle: 90,
                    radius: 250
                },

                scale: {
                    majorTick: {
                        showCalculatedTicks: false,
                        customTickValues: [0,25,50,100]
                    }
                },

                rangeContainer: {
                    backgroundColor: &quot;none&quot;,
                    ranges: [
                        {
                            startValue: 0,
                            endValue: 25,
                            color: &quot;red&quot;
                        },
                        {
                            startValue: 25,
                            endValue: 50,
                            color: &quot;yellow&quot;
                        },
                        {
                            startValue: 50,
                            endValue: 100,
                            color: &quot;green&quot;
                        }
                    ]
                },

                needles: [{value: 81}]
            },

            useDevice: function(){
                var value = this.gauge.needleValue(0);
                if(value > 0){
                    value -= 10;
                }
                this.gauge.needleValue(0, value);
            },

            recharge: function(){
                this.gauge.needleValue(0, 100);
            }
        };

        var html = [
            '<div data-bind=&quot;dxCircularGauge: settings&quot;></div>',
            &quot;<div>&quot;,
            '<label data-bind=&quot;click: useDevice&quot; style=&quot;cursor:pointer;font-size:18px&quot;>',
            &quot;&quot;,
            &quot;</label>&quot;,
            &quot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&quot;,
            '<label data-bind=&quot;click: recharge&quot; style=&quot;cursor:pointer;font-size:18px&quot;>',
            &quot;&quot;,
            &quot;</label>&quot;,
            &quot;</div>&quot;
        ].join(&quot;&quot;);

        \$(&quot;#chartContainer&quot;).append(html);
        ko.applyBindings(model, \$(&quot;#chartContainer&quot;)[0]);
        model.gauge =  \$(&quot;#chartContainer div&quot;).dxCircularGauge(&quot;instance&quot;);
})
</script>
</body>
</html>" frameborder="0"  style="width:700px;height: 350px;"></iframe>
</div>
""";
scall.each {
    utils.edit(it, ['karma': HTML]);
}
return '';