def icona = 'icon'
//def metainfoService = beanFactory.getBean("metainfoService")
def attr = [:]
def objects = utils.find('KE$printer', [:])
for (def obj1 in objects) {
    //attr[icona] = beanFactory.getBean("metainfoService").getMetaClass(obj1.metaclass.getCase()).getAttribute('icona').getDefaultValue()
    attr[icona] = utils.get('icons', ["code": "printer"])
    //attr[icona] = obj1.icon.getDefaultValue()
    utils.edit(obj1, attr)
}