//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
def sates = ['firstLine', 'secondLine', 'resumed'];
def scalls = utils.find('serviceCall', ['state' : sates]);
def team = utils.find('team', ['title' : 'Первая линия']);
def employee = utils.find('employee', ['title' : 'Гришина Галина Сергеевна']);
scalls.each{
	try {
		utils.edit(it, ['responsibleEmployee' : it.responsibleTeam.leader]);
		
	}
	catch(Exception e) {
		log(e.toString())
	}	
}

return 'root';