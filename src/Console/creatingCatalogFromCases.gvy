//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Создание каталога типов
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
def CALALOG_CODE = 'classNtypes';
def CLASS_LIST = ['task', 'negotiation', 'serviceCall'];
//Тело--
CLASS_LIST.each{ classic->
	def CLASS = api.metainfo.getMetaClass(classic);
	def folder = utils.create(CALALOG_CODE,['folder':true,
		'code':CLASS.code,
		'title':CLASS.title]);
	def CLASS_TYPES = api.metainfo.getTypes(CLASS.code);
	CLASS_TYPES.each{type->
		utils.create(CALALOG_CODE,['folder':false,
		//'code':api.string.replace(CLASS_TYPES.code,"${CLASS.code}\$", ''),
		'code':type.code,
		'parent':folder,
		'title':type.title]);
	};
};
return '!';