//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
def CATALOG = 'operation';
def MAP = [
"Технические помещения" : "Электрика",
"Торговый зал" : "Электрика",
"Офисная зона" : "Электрика",
"Уличное освещение/реклама" : "Электрика",
];
def counter = 60;
def parentFinder = { title ->
	log('!!!!!!! look for' + title)
	return utils.get(CATALOG, ['title' : title])
};
def eleCreator = {element, parent->
 utils.create(CATALOG, ['code' : counter,
 	'title' : element.key,
 	'parent' : parent])
 	return 1;
};
def parentCreator = {element->
 utils.create(CATALOG, ['code' : counter,
 	'title' : element.value])
 	return 1;
};
try {
	MAP.each{
		if(parentFinder(it.value))
		{
			log('!!!!!!!!!! Создание')
			counter+=eleCreator(it, parentFinder(it.value))
		}
		else {
			log('!!!!!!!!!! Создание + родитель' + parentFinder(it.value))
			counter+=(parentCreator(it) + eleCreator(it, parentFinder(it.value)))
		}
}
}
catch(Exception e) {
	log(e.toString())
}

return '';