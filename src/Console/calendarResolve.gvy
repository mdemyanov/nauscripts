//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def METACLASS = 'employee$technician';
def EMPLOYEES = [];
def SEARCH_PARAMS = [:];
//ФУНКЦИИ--------------------------------------------------------------
def employeeGetCalenar = {employee->
	def calId = api.string.replace(employee.calId, '@', '%40');
	def calTemplate = """<style type=text/css>
.calendar {
height: 600px;
width: 800px;
}
</style>
<div class=calendar>
<iframe src=https://www.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=${calId}&amp;color=%236B3304&amp;ctz=Europe%2FMoscow style= border-width:0  width=800 height=600 frameborder=0 scrolling=no></iframe>
</div>""";
		return calTemplate;
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
EMPLOYEES = utils.find(METACLASS, SEARCH_PARAMS);
EMPLOYEES.each{employee->
	try {
		utils.edit(employee, ['calendar' : employeeGetCalenar(employee)]);
	}
	catch(Exception e) {
		log("Не удалось добавить календарь: ${e.toString()}");
	}	
};
return EMPLOYEES.size();
