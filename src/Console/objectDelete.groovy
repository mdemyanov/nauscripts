/*! UTF8 */
/*& 3600 cache */
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Код: PartNumber из naupp
//Назначение:
/**
 * Удаляет представителей классов/типов указанных в атрибуте FQNS
 */
//Версия: 4.0.1.x
//Категория: консольный скрипт

// Логгирование:
/*По умолчанию логгирование выключено*/
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def FQNS = ['serviceLevel'];
def COUNTS = [:];
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК--------------------------------------------------------
FQNS.each { fqn ->
    def objects = utils.find(fqn, [:]);
    COUNTS.put(fqn, 0);
    objects.each {
        try {
            log("Удаляем объект ${it.title}");
            utils.delete(it);
            COUNTS[(fqn)] += 1;
        }
        catch (Exception e) {
            log("Объект ${it.title} не удален, \n Причина: ${e.toString()}");
        }

    };
};
return COUNTS.toString();
