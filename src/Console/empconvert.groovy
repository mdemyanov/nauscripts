// package Console
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def empls = utils.find('employee$corpUser', [:]);
try {
    empls.each({
        utils.edit(it, ['metaClass': 'employee$businessUser']);
    });
}
catch (Exception e) {
    logger.info(e);
}
return '';

