//Автор: md
//Дата создания: 01.07.2013
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def units = utils.find('units', [:]);
def attrsToDef = ['icon'];
units.each{unit->
	//def metaClass = unit.metaClass;
	def attrs = [:];
	attrsToDef.each{
		attrs.put(it, api.metainfo.getMetaClass(unit).getAttribute(it).defaultValue);
	}
	utils.edit(unit,attrs);
}
return '';