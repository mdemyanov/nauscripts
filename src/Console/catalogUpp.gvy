//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
def products = [
	'Водоснабжение' : 'sun',
	'Канализация' : 'sun' ,
	'Сантехническое оборудование' : 'sun' ,
	'Система вентиляции' : 'ven',
	'Тепловая завеса' : 'ven',
	'Кондиционирование' : 'ven',
	'Технические помещения' : 'elect',
	'Торговый зал' : 'elect',
	'Офисная зона' : 'elect',
	'Уличное освещение/реклама' : 'elect'
	];
def i = 720;
try {
	products.each{
		
		utils.create('category', ['parent' : utils.find('category', ['code' : it.value]), 
			'title' : it.key, 
			'code' : i]);
		i++;
	}
}
catch(Exception e) {
	log(e);
}
