//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def servUUID = 'slmService$9001';
def service = utils.get(servUUID);
//ФУНКЦИИ--------------------------------------------------------------
return service.units.components.components.size();
//ОСНОВНОЙ БЛОК------------------------------------------------------------
