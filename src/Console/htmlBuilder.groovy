import groovy.xml.StreamingMarkupBuilder;
def HTML = new StreamingMarkupBuilder().bind {
    div('height': '800px',
            'width': '1000px') {
        iframe('height': '800px',
                'width': '1000px',
                'marginheight': '0',
                'src': 'http://habrahabr.ru/')
    }
}
return HTML;