//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def UUID = 'serviceCall$377702';
//ФУНКЦИИ--------------------------------------------------------------
def changeToArhive = {obj->
	def attrs = [:];
	attrs.put('removed', true);
	def flag = '';
	try {
		utils.edit(obj, attrs);
		flag = '1';
	}
	catch(Exception e) {
		log("Объект не переведен в архив по причине: ${e.toString()}");
		flag = 'o';
	}
	return flag;	
}
//ОСНОВНОЙ БЛОК------------------------------------------------------------
def scall = utils.get(UUID);
return changeToArhive(scall);

