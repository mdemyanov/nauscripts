//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
import ru.naumen.metainfo.shared.ClassFqn;
import ru.naumen.metainfo.shared.dispatch2.AddAttributeAction;
def METACLASS_CODES = ['task'];
def ATTRS = [
	'extDescr' : [title : 'Описание',
	type : 'richtext',
	editable : true,
	computable : false,
	required : false,
	unique : false	
	],
	'shortDescr' : [title : 'Краткое описание',
	type : 'string',
	editable : true,
	computable : false,
	required : false,
	unique : false	
	]
];

//ФУНКЦИИ--------------------------------------------------------------
def addAttribute = {fqn, title, code, type, computable, editable, required, unique->
	def metaClass = api.metainfo.getMetaClass(fqn);
    def attribute = null;
    AddAttributeAction action = new AddAttributeAction(ClassFqn.parse(fqn), code, type, title, "", 
            computable, editable, required, unique, '', '', '', '', '');
       return true; 
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
METACLASS_CODES.each {fqn->
	ATTRS.each{attribute->
		try {
			addAttribute(fqn,
			attribute.value.title, 
			attribute.key, 
			attribute.value.type, 
			attribute.value.editable,
			attribute.value.computable,
			attribute.value.required,
			attribute.value.unique);
		}
		catch(Exception e) {
			log(e.toString());
		}
		
	}
}
return '';