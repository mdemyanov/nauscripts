//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
//ФУНКЦИИ--------------------------------------------------------------
def spec = 'employee$technician';
def businessUser = 'employee$businessUser';
//ОСНОВНОЙ БЛОК------------------------------------------------------------
def specEmpls = utils.find(spec, [:]);
def count = 0;
specEmpls.each {
	log("Сотрудник: ${it.title}");
	def toDelete = utils.get(businessUser, ['email' : it.email ?: 'notMail', 'license' : 'notLicensed', 'parent' : 'ou$344101']);
	if (null != toDelete) {
		try {
		log(" Сотрудник ${toDelete.title} удален");
		utils.delete(toDelete);
		// toDelete.each{
		// 	log("Сотрудник не лиц: ${it.title}")
		// };		
		count++;
	}
	catch(Exception e) {
		log(" Сотрудник ${toDelete.title} не удален по причине ${e.toString()}")
	}
	}
	
}
return "Удалено ${count} дубликатов"