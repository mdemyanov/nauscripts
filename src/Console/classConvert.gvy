//Автор: mdemyanov
//Дата создания: 02.07.2012
//Код: PartNumber из naupp
//Назначение:
/**
 * Открывает для пользователя возможность просматривать новости
 */
//Версия: 4.0.1.x
//Категория: Вычислимая роль
// Логгирование:
/*По умолчанию логгирование выключено*/
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def FQN = 'slmService';
def SEARCH_PARAMS = [:];
def RESULT_FQN = 'slmService$cfService';
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК--------------------------------------------------------
utils.find(FQN, SEARCH_PARAMS).each{
	utils.edit(it, ['metaClass' : RESULT_FQN]);
};
return 'Hello';
