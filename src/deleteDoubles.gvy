//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def CUR_CLASS = 'employee$technician';
def TAR_CLASS = 'employee$businessUser';
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК--------------------------------------------------------
def employees = utils.find(CUR_CLASS,[:]);
def count = 0;
employees.each{employee->
	log("Специалист: ${employee.title}");
	def tarEmployee = utils.find(TAR_CLASS, ['email' : employee?.email, 'license' : 'notLicensed', 'parent' : 'uuid:ou$344101']);
	//tarEmployee ? utils.delete(tarEmployee) : count--;
	tarEmployee.each{
		it?.email ? log("Сотрудник: ${it.title}") : count--;
		//it?.email ? utils.delete(it) : log('не Удален');
	count++;
	};
};
return count.toString();