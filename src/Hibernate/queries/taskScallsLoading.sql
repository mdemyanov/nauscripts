select employee.title, (
	select 
	case
	when count(callResponsibl.id) is null then '0' 
	else count(callResponsibl.id) 
	end
	from employee.callResponsibl callResponsibl
	) as scalls, 
	(select 
	case
	when count(activeTasks.id) is null then '0' 
	else count(activeTasks.id) 
	end
	from employee.activeTasks activeTasks
	where activeTasks.state not in ('closed', 'resolved', 'registered')) as tasks
from team as team
inner join team.members employee
where team.id = '2603'
order by scalls, tasks