//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
def OBJECT_UUID = OBJECT['uuid'];
def OBJECT_ID = api.string.replace(OBJECT_UUID, 'serviceCall$', '');

def RESPONSIBLE_TEAM_CODE = 'responsibleTeam';
def RESPONSIBLE_TEAM = OBJECT[RESPONSIBLE_TEAM_CODE];
def RESPONSIBLE_TEAM_UUID = RESPONSIBLE_TEAM['RESPONSIBLE_TEAM'];
def RESPONSIBLE_TEAM_ID = api.string.replace(RESPONSIBLE_TEAM_UUID, 'team$', '');

def QUERY = api.db.query("""
select employee.title, (
	select 
	case
	when count(callResponsibl.id) is null then '0' 
	else count(callResponsibl.id) 
	end
	from employee.callResponsibl callResponsibl
	) as scalls, 
	(select 
	case
	when count(activeTasks.id) is null then '0' 
	else count(activeTasks.id) 
	end
	from employee.activeTasks activeTasks
	where activeTasks.state not in ('closed', 'resolved', 'registered')) as tasks
from team as team
inner join team.members employee
where team.id = '2603'
order by scalls, tasks
	        """)

def CHANGE_RESPONSIPLE_LINK = """
	<a href="${api.rest.edit(subject, ['title' : 'new title'],
'username')}">'</a>
""";