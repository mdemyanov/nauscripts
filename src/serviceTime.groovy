def serviceTime = 'serviceTime'
def atribute = [:]
atribute[serviceTime] = utils.get('servicetime', ["code": "8x5"])
def serviceCalls = utils.find('serviceCall$incidentCall', [:])
for (def sc in serviceCalls) {
    utils.edit(sc, atribute)
}