def html = """<table rules="rows" border="1" style="border-color: #395382; border-radius: 5px; border-width: 2px; border-style: double; border-spacing: 2px; background-color: #87ceeb;">
    <tbody>
    <tr style="background-color: rgba(82,152,231,0.83); color: #f5f5f5; border-bottom-color: blue; border-bottom-width: 2px;">
        <th>
            Сервис
        </th>
        <th>
            Название
        </th>
        <th>
            Статус
        </th>
    </tr>
    """
def myQuery = api.db.query("""select client.agreement.title as agreement,
                                        client.service.title as service,
                                        count(distinct client.parent.id)
        from association as client
        group by client.agreement.title, client.service.title
        """)
def qq = myQuery.list()
qq.each({ html += "<tr><td>" + it[0] + "</td><td>" + it[1] + "</td><td>" + it[2].toString() + "</td></tr>" })
html = api.string.concat("", [html, "</tbody></table>"])
return html


if (subject) {
    return utils.find('KE', ['ouOuner': subject.clientOU])
}
objects