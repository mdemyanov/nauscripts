select srvtitle,
srvid,
sctype,
count(1) cnt
from (
  select srv.title srvtitle,
  srv.id srvid,
  case 
    when sc.case_id = 'incidentCall' then 'Инцидент' else 
    case 
      when sc.case_id = 'accessRequest' then 'Запрос доступа' else
        case 
          when sc.case_id = 'rerviceReq' then 'Запрос на обслуживание' else
            case 
              when sc.case_id = 'advice' then 'Консультация' else 'Другое' end end end end sctype
  from tbl_slmservice srv
  join tbl_servicecall sc on sc.service_id = srv.id
) sourse
group by srvtitle, srvid, sctype
order by srvtitle, srvid, sctype