select age,
sum(case when state not in ('resolved', 'closed') then 1 else 0 end) notclosed,
count(*) allsc
from (
  select sc.id id,
  sc.state state,
  case 
    when sc.registration_date > current_date then '1. Сегодня' else 
    case 
      when sc.registration_date > (current_date - 2) and sc.registration_date < current_date then '2. Заявки, которым от суток до двух' else
        case 
          when sc.registration_date > (current_date - 3) and sc.registration_date < (current_date - 2) then '3. Заявки, которым от двух до 3-х суток' else
            case 
              when sc.registration_date > (current_date - 5) and sc.registration_date < (current_date - 3) then '4. Заявки, которым от 3 до 5 суток' else 
              case
                when sc.registration_date > (current_date - 7) and sc.registration_date < (current_date - 5) then '5. Заявки, которым от 5 до 7 суток' else
                case
                  when sc.registration_date < (current_date - 7) then '6. Заявки, старше 7 суток' end end end end end end age
  from tbl_servicecall sc
) sourse
group by age
order by age