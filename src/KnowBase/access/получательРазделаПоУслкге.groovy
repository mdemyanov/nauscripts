// package KnowBase.access
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: вычисление роли
/**
 * Выччисляет, имеет ли пользователь доступ к разделу БЗ:
 * если пользователь является получателм услуги по одному из соглашений,
 * то возвращаем тру.
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
if (user) {
    /*Сервис по разделу*/
    def SERVICE = subject.service;
    //Основной блок----------------------------------------------------
    def myQuery = api.db.query("""
		select agreements
		from slmService slmService
		join slmService.agreements agreements
		join agreements.recipients employee
		where employee.id = :employee
		and slmService.id = :service
	        """)
    myQuery.set('employee', user.UUID);/*Текущий пользователь*/
    myQuery.set('service', SERVICE.UUID);/*Сервис по разделу*/
    def result = myQuery.list().size();
    return result ? true : false;
} else {
    return true;
}