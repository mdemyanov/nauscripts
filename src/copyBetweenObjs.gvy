//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def TO_OBJECT = 'call'; /*Куда копировать: если пусто, то subject*/
def FROM_OBJECT = ''; /*Откуда копировать: если пусто, то subject*/
/**Атрибуты для копирования:
* Здесь указывается соответсвие копируемых атрибутов:
* в key (первый атрибут) - значение из FROM_OBJECT,
* в value (второй) - атрибут в TO_OBJECT
*/
def ATTRS_TO_COPY = [
		'whoSolved_em' : 'solvedByEmployee', /*Класс обслуживания*/
		'whoSolved_te' : 'solvedByTeam', /*Название*/
		'resultDescript' : 'report'
	];
//ФУНКЦИИ--------------------------------------------------------------
def validator = {value->
	return (value == '') ? subject : subject[value];	
}
def coping = {from, to, attrsToCopy->
	def attrs = [:];
	/*Запись значений атрибутов контейнер*/
	log("Произведем запись значений атрибутов в контейнер.");
	attrsToCopy.each{ attr->
		attrs.put(attr.value, from[attr.key]);
	}
	try {
		utils.edit(to, attrs);
		log("Объект ${to.title} успешно изменен")
	}
	catch(Exception e) {
		log("Ошибка при копировании: ${e.toString()}")
	}	
}
//ОСНОВНОЙ БЛОК------------------------------------------------------------
/*Произведем валидацию ссылок и запишем значения*/
log("Запускаем валидацию.")
def from = validator(FROM_OBJECT);
def to = validator(TO_OBJECT);
/*Запустим операцию копирования*/
log("Запускаем копирование");
coping(from,to, ATTRS_TO_COPY);
return '';