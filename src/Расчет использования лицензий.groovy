//Количество используемых лицензий или количество пользователей на 1 лицензию
//Возвращает вещественное число
//Количество пользователей на 1 лицензию вычисляется только для лицензий типа "Конкурентная"
if (subject) {
    def recipientsCount = subject.recipients.size() //количество получателей Лицензии
    def licCount = subject.licCount //общее количество лицензий
    def result = ''
    switch (subject.licenseType.code) {
        case 'competitive':
            result = api.string.concat("", [(recipientsCount / licCount).toString(), " пользователя(ей) на одну лицензию"])
            break
        case 'named':
            result = api.string.concat("", ["Осталось ", (recipientsCount / licCount).toString(), " сободных лицензий"])
            break
        default:
            result = api.string.concat("", ["Используется ", (recipientsCount / licCount).toString(), " лицензий"])
            break
    }
    return result
} else return "Ошибка вычисления"