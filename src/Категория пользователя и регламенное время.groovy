//Скрипт на заполнение категории пользователя и регламентноо времени решения запроса
// Определяем категорию пользователей
def userCategory = 'categoriesUsers'
// Класс обслуживания
def SERVICE_TIME = 'serviceTime'
// Атрибут плановой даты решения инцидента
def deadLineTime = 'deadLineTime'
// Запас времени обслуживания
def stockTimer = 'stockTimer'
// Массив атрибутов
def attrs = [:]
attrs[userCategory] = subject.clientEmployee.categoriesUsers
attrs[deadLineTime] = api.timing.serviceEndTime(subject[SERVICE_TIME], subject.timeZone, subject.registrationDate, subject.resolutionTime.toMiliseconds())
attrs[stockTimer] = subject.resolutionTime
utils.edit(subject, attrs)