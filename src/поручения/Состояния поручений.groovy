// Смены состояний
// в работе: смена атрибута иконки для отображения в списке
utils.edit(subject, ['icom': utils.get('icons', ['code': 'time'])])
// на паузе: смена атрибута иконки для отображения в списке
utils.edit(subject, ['icom': utils.get('icons', ['code': 'pause'])])
// завершено: смена атрибута иконки для отображения в списке
utils.edit(subject, ['icom': utils.get('icons', ['code': 'check'])])