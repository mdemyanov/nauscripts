def SRV = 'services'
def RECIP = 'recipients'
def RECIPOU = 'recipientsOU'
def agreements = utils.find('agreement', [:])
for (def agr in agreements) {
    for (def c in agr[SRV]) {
        def attrs = [:]
        //def listEm = agr.recipients
        //listEm.add(agr.recipients)
        //attrs[RECIP] = listEm
        attrs[RECIP] = agr.recipients
        //attrs[RECIP].add(agr.recipients)
        //attrs[RECIP].add(agr.recipientsOU)
        attrs[RECIPOU] = agr.recipientsOU
        utils.edit(c, attrs)
    }
}

=== === === === === === =
def services = utils.find('slmService', [:])
for (def s in services) {
    def recipients = api.db.query("select r  from slmService s join s.agreements a join a.recipients r where s = :s").set('s', s).list();
    utils.edit(s, ['recipients', recipients]);
}