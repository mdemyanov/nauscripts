//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
/*Указать через запятую класс$тип запросов с атрибутом descriptionRTF*/
def OBJ_CLASSES = ['employee'];
def PARAMS = ['removed' : true];
def count = 0;
//ФУНКЦИИ--------------------------------------------------------------
def changer = {obj->
	try {
		utils.edit(obj, ['removed' : false]);
		log("Изменен запрос: ${obj.title}");
	}
	catch(Exception e) {
		log("Объект не изменен по причине: ${e.toString()}");
	}
	return 1;
}
//ОСНОВНОЙ БЛОК------------------------------------------------------------
OBJ_CLASSES.each{objClass->
	utils.find(objClass, PARAMS).each{obj->
		api.tx.call{
			count+=changer(obj);
		}
	}
}
return count;