var dataSource = [
    { state: "Интернет", plan: 423, fact: 480 },
    { state: "Телефония", plan: 178, fact: 170 },
  { state: "ИС: SAP ERP", plan: 308, fact: 372 },
    { state: "Корп. почта", plan: 348, fact: 300 },
    { state: "Рабочее место", plan: 160, fact: 211 }
];

$("#chartContainer").dxChart({
    dataSource: dataSource,
    commonSeriesSettings: {
        argumentField: "state",
        type: "bar",
        hoverMode: "allArgumentPoints",
        selectionMode: "allArgumentPoints",
        label: {
            visible: true,
            format: "fixedPoint",
            precision: 0
        }
    },
    series: [
        { valueField: "plan", name: "План" },
        { valueField: "fact", name: "Факт" }
    ],
    title: "Плановый и фактические трудозатраты за август",
    legend: {
        verticalAlignment: "bottom",
        horizontalAlignment: "center"
    },
    pointClick: function (point) {
        this.select();
    }
});