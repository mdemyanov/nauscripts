var dataSource = [
    { week: 19, in: 550, out: 540 },
    { week: 20, in: 600, out: 595 },
    { week: 21, in: 650, out: 513 },
    { week: 22, in: 700, out: 650 },
    { week: 23, in: 750, out: 700 },
    { week: 24, in: 800, out: 800 },
    { week: 25, in: 850, out: 840 },
    { week: 26, in: 1000, out: 900 },
    { week: 27, in: 1150, out: 1140 },
    { week: 28, in: 1300, out: 1300 },
    { week: 29, in: 1450, out: 1400 }
];

$("#chartContainer").dxChart({
    dataSource: dataSource,
    commonSeriesSettings: {
        argumentField: "week"
    },
    series: [
        { valueField: "in", name: "Поступило обращений" },
        { valueField: "out", name: "Разрешено обращений" }
    ],
    argumentAxis:{
        grid:{
            visible: true
        }
    },
    tooltip:{
        enabled: true
    },
    title: "Динамика обработки входящих обращений",
    legend: {
        verticalAlignment: "bottom",
        horizontalAlignment: "center"
    },
    commonPaneSettings: {
        border:{
            visible: true,
            right: false
        }       
    }
});