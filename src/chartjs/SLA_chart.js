var dataSource = [
    { state: "Интернет", rfs: 22, consult: 0, incident: 12 },
    { state: "Телефония", rfs: 12, consult: 1, incident: 27 },
    { state: "Рабочая станция", rfs: 12, consult: 2, incident: 45 },
    { state: "Корп. почта", rfs: 0, consult: 0, incident: 2 },
    { state: "ИС: SAP ERP", rfs: 41, consult: 1, incident: 45 }
]
;
$("#chartContainer").dxChart({
    dataSource: dataSource,
    commonSeriesSettings: {
        argumentField: "state",
        type: "bar",
        hoverMode: "allArgumentPoints",
        selectionMode: "allArgumentPoints",
        label: {
            visible: true,
            format: "fixedPoint",
            precision: 0
        }
    },
    series: [
        { valueField: "incident", name: "Инцидент" },
        { valueField: "consult", name: "Консультация" },
        { valueField: "rfs", name: "Запрос на обслуживание" }
    ],
    title: "Отчет по нарушениям SLA",
    legend: {
        verticalAlignment: "bottom",
        horizontalAlignment: "center"
    },
    pointClick: function (point) {
        this.select();
    }
});