//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def CLASSES = ['serviceCall$userCalls'];
def UUIDS = ['serviceCall$51410',
        'serviceCall$51413',
        'serviceCall$51412',
        'serviceCall$51411',
        'serviceCall$51409',
        'serviceCall$51408',
        'serviceCall$51407',
        'serviceCall$51405',
        'serviceCall$51401',
        'serviceCall$51402',
        'serviceCall$51403',
        'serviceCall$51404'];
def PARAMS = ['UUID': UUIDS];
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК--------------------------------------------------------
CLASSES.each {
    def units = utils.find(it, PARAMS);
    units.each { unit ->
        log(unit.metaClass.getCase());
    }
}
return '';
