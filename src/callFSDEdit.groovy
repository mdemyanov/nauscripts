def calls = utils.find('serviceCall', [:])
def attr = [:]
def factSolvedDate = 'factSolvedDate'
for (def call in calls) {
    attr[factSolvedDate] = call.stateStartTime
    utils.edit(call, attr)
}
