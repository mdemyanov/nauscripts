//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
import groovy.xml.StreamingMarkupBuilder;
// Контейнер для соглашений
def EMPLOYEES = [];
// Параметры для поиска соглашений
def EMPLOYEES_PARAMS = [:];
//основной блок-----------------------------------------------------------
// Произведем поиск всех соглашений по параметрам
EMPLOYEES = utils.find('employee$businessUser', EMPLOYEES_PARAMS);
log("Найдено ${EMPLOYEES.size().toString()} сотрудников");
// Для каждого соглашения выполним операции
api.tx.call {
	EMPLOYEES.each{recipient ->	
	// Определим получателей (только сотрудники)
	def AGREEMENTS = recipient.recipientAgreements;    
	log("Найдено ${AGREEMENTS.size().toString()} соглашений у ${recipient.title}");
	// Теперь для каждого сотрудника создадим панель
    def HTML = new StreamingMarkupBuilder().bind{
            // Контейнер со стилями
            link(
            'rel' : 'stylesheet',
            'href' : 'https://dl.dropboxusercontent.com/s/ezvgnk9uv0mbaxn/style.css?token_hash=AAGUFbFVJE6riYgbtXX1WH9QNUmtgqqmX4sISGuzmFQr0g&dl=1',
            )
            // Навигационная панель
            ul('id' : 'nav') {
	AGREEMENTS.each {agreement ->
        //Определим услуги, распространяемые по соглашению
        def SERVICES = agreement.services;
        log("Найдено ${SERVICES.size().toString()} услуг");
		
        		SERVICES.each {service ->
        			li() {
        				a('href' : api.web.open(service), service.title)
        				ul() {
        					log("Для регистрации доступно ${SERVICES.size().toString()} типа запроса");
        					// Для каждого типа запроса, доступного по услуге
        					service?.callCases.each { callCase ->
        						// Контейнер с параметрами
        						def ATTRIBUTES = [
        							'service' : service,
									'agreement' : agreement
        							];
        						// Тип запроса
        						def FQN = callCase.id + '$' + callCase.getCase();\
                                log("Создаем ссылку на запрос с параметрами: Соглашение: ${agreement.title}; Услуга: ${service.title};");
        						li() {
        							a('href' : api.web.add(FQN, recipient, ATTRIBUTES), api.metainfo.getMetaClass(FQN).title);
        						}
        					}
        				}
        			}
        		}
    		}
    		
		}		
	}
    try {
            utils.edit(recipient, ['panel' : HTML.toString()]);
        }
        catch(Exception e) {
            log(e.toString());
        }
}
}

return 'true';