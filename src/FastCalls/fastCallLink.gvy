//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Скрипт создает ссылку на быструю регистрацию запроса для кокретного контрагента
 * и заполняет по умолчанию некоторые параметры
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
def TEMPLATE = subject;
def FQN = 'serviceCall$' + TEMPLATE.metaClass.getCase();
def responsible = utils.get('team', ['title' : 'Первая линия'])
def clientEmployee = utils.get('employee', ['lastName': "Караваева"]);
def ATTRIBUTES = [
	'service' : TEMPLATE.service,
	'agreement' : TEMPLATE.agreement,
	'template' : TEMPLATE,
];
try {
	def url = api.web.add(FQN,clientEmployee, ATTRIBUTES);
	def fastLink = api.types.newHyperlink('text', url);
	utils.edit(TEMPLATE, ['fastUrl' : fastLink]);
}
catch(Exception e) {
	log(e.toString());
}
