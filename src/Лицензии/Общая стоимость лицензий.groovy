// package Лицензии

def licenses = utils.find('license', ['licAgreement': subject])
def cost = 0.0
licenses.each({
    cost += it.allCost
})
return cost