// package Лицензии

def licenses = utils.find('license', ['licAgreement': subject])
def free = 0
licenses.each({
    (it.metaClass.getCase() == 'named' ? free += it.freeLic : 0)
})
return free