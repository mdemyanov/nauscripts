// package Лицензии

def licenses = utils.find('license', ['licAgreement': subject])
def used = 0.0
def i = 0
licenses.each({
    (it.metaClass.getCase() == 'concurent' ? used += (it.recipients.size() / it.licCount) : 0)
    (it.metaClass.getCase() == 'concurent' ? i += 1 : 0)
})
return used / i