//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
if(subject.newEmployee){
def CANDIDATE = subject;
def params = [
	'candidate' : CANDIDATE,
	'agreement' : CANDIDATE.serviceCall.agreement,
	'service' : CANDIDATE.serviceCall.service,
	'clientOU' : CANDIDATE.serviceCall.clientOU,
	'startTime' : CANDIDATE.comingPlan
];
try {
	utils.create('serviceCall$newEmployee', params);
}
catch(Exception e) {
	log(e);
}
}
return '';