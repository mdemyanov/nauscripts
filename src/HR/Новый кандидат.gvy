//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: создание сотрудника при регистрации задания
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
log("Запуск сценария создания нового сотрудника.");
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
def EMPLOYEE_CLASS = 'employee$candidate';
def PARAMS = [
		'sername' : 'lastName', /**/
		'name' : 'firstName', /**/
		'lastname' : 'middleName', /**/
		'telephone' : 'mobilePhoneNumber', /**/
		'email' : 'email',
		'UUID' : 'interviews'
	];
def CANDIDATE = OBJECT['candidate'];
def DEFAULT_CATALOG = utils.get('ou$base',['title' : 'База кандидатов']);
//ФУНКЦИИ--------------------------------------------------------------
def candidateCreator = {
	def attrs = [:]; /*Контейнер, в который записываются значения атрибутов*/
	/*Запись значений атрибутов контейнер*/
	log("Произведем запись значений атрибутов в контейнер.");
	PARAMS.each{ attr->
		attrs.put(attr.value, OBJECT[attr.key]);
	};
	attrs.put('parent', DEFAULT_CATALOG);
	def object;
	log("Создание объекта типа: ${EMPLOYEE_CLASS}");
	try {
		object = utils.create(EMPLOYEE_CLASS, attrs);
	}
	catch(Exception e) {
		log("Объект не создан по причине: ${e.toString()}");
	}
	return object;
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
if (CANDIDATE) {
	log("Кандидат не создан: уже существует.");
	utils.edit(OBJECT, ['title' : CANDIDATE.title]);
} else {
	CANDIDATE = candidateCreator();
	log("Создан кандидат: ${CANDIDATE?.title}.");
	utils.edit(OBJECT, ['title' : OBJECT['sername'] + ' ' + OBJECT['name']]);
}
return '';