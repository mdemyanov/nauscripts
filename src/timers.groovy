// Переводим время в милисекунды
def ms = subject.resolutionTime.toMiliseconds()
//Создаем переменную, которая хранит необходимый временной интервал умноженный на коэффициент
def resolTime07 = (ms * 0.7 / 1000) / 60
// Создаем новый временной промежуток
def newVal = new ru.naumen.common.shared.utils.DateTimeInterval(resolTime07.longValue(), 'MINUTE')
// Редактируем новый временной промежуток
utils.edit(subject, ['resolTime07': newVal])
// serviceTime элемент или код элемента справочника "Классы обслуживания"
// timeZone элемент или код элемента справочника "Часовые пояса"
// startTime время от которого необходимо начать отсчитывать период обслуживания (Date)
// interval период обслуживания (кол-во милисекунд, целое число)
def endTime = api.timing.serviceEndTime(serviceTime, timeZone, startTime, interval)