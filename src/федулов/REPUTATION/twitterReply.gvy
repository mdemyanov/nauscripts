//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
/*Делаем импорты*/
import groovyx.net.http.RESTClient;
import groovy.util.slurpersupport.GPathResult;
import static groovyx.net.http.ContentType.URLENC;
/*Прописываем параметры и глобальные переменные*/
/*Компания, где прописанны параметры подключения*/
def ROOT = utils.get('root', [:]);
/*Обращение к АПИ сервиса*/
def TWITTER = new RESTClient('https://api.twitter.com/1.1/');
/*Параметры авторизации*/
TWITTER.auth.oauth(
	ROOT.consKey,
	ROOT.consSecret,
	ROOT.accToken,
	ROOT.accTokenSecret);
/*Объект, в рамках которого создаем твит*/
def SOURCE = subject?.serviceCall;
/*Сам твит*/
def OBJECT = subject;
//ФУНКЦИИ--------------------------------------------------------------
def postTweet = {statusText,reTweet->
	try {
	log("Публикуем твит с текстом: ${statusText} в ответ на ${reTweet}")
	return TWITTER.post(path : 'statuses/update.json',
    requestContentType: URLENC,
    body: [status: statusText, 
    in_reply_to_status_id: reTweet]);
	}
	catch(Exception e) {
	log("Не удалось опубликовать твит, причина: ${e.toString()}");
	return 'Репост неудался';
	}
};
def getParams = {tweetURL->
	def paramsList = api.string.replace(tweetURL, 'http://twitter.com/', '').split('/');
	return paramsList;
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
/*Ссылка на сообщение*/
def tweetURL = SOURCE?.respLink.URL;
/*Собираем параметры ссылки в лист*/
def params = getParams(tweetURL);
/*Записываем текст сообщения*/
def statusText = "@${params[0]}:" + api.string.htmlToText(OBJECT?.massage);
def reTweet = params[2];
def tweet = postTweet(statusText,reTweet);
return '';