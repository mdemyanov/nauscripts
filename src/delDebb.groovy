//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def classToDelete = ['news', 'employee', 'serviceCall', 'serviceCall$rfc'];
def deletedObjets = [:];
def unDeletedObjets = [:];
classToDelete.each {
    objToDelete = utils.find(it, [:]);
    deletedObjets.put(it, 0);
    unDeletedObjets.put(it, 0);
    for (obj in objToDelete) {
        try {
            utils.delete(obj);
            deletedObjets.put(it, deletedObjets.get(it) + 1);
        }
        catch (Exception e) {
            logger.info("""
				Не удалось удалить объект ${obj.title}
				Причина ${e}
		""");
            unDeletedObjets.put(it, unDeletedObjets.get(it) + 1);
        }
        finally {
            logger.info("""
		Удаление ${obj.title}, объект класса ${it}
		""");
        }
    }
    objToDelete = null;
    logger.info("""
		Удалено ${deletedObjets.get(it)} объектов класса ${it}
		Не удалено ${unDeletedObjets.get(it)} объектов класса ${it}
		""");
}
return deletedObjets;