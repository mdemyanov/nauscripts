// package Tasks
//Автор: mdemyanov
//Дата создания: 13.11.2012
//Назначение: заполнение поля "Кем решен"
/**
 * Автоматически заполняет поле "Кем решен"
 * Описание логики:
 * Сценарий проверяет аггрегирующий атрибут ответственного на предмет заполнености. Варианты:
 * 1. Если назначен ответственный в рамках конкретной команды (заполнены атрибуты responsibleTeam и 
 * responsibleEmployee), то сотрудником, решившим запрос, считается текущий ответсвенный.
 * 2. Если в роли ответственного выступает только команда, но сотрудник, который перевел запрос в 
 * состояние "Разрешен" входит в нее, то он назначается решившим запрос.
 * 3. Если ответственный не назначен, то сценарий возвращает строку с описание причины, почему 
 * объект нельзя перевести в конкретное состояние.
 */
//Версия: 4.0.0.17
//Категория:
//ПАРАМЕТРЫ------------------------------------------------------------
/*не используется, так как не работает*/
//def responsible 		= subject.responsible;
/*Ответственная команда*/
def responsibleTeam = subject.responsibleTeam;
/*Ответственный сотрудник*/
def responsibleEmployee = subject.responsibleEmployee;
//Основной блок------------------------------------------------------------
/*Если назначен отв сотрудник в команде*/
if (responsibleTeam && responsibleEmployee) {
    utils.edit(subject, ["solvedBy_te": responsibleTeam, "solvedBy_em": responsibleEmployee]);
    return '';
} else if (responsibleTeam && !responsibleEmployee) /*Если сотрудник не назначен, но есть команда*/ {
    (responsibleTeam.members.contains(user)) ? (utils.edit(subject, ["solvedBy_te": responsibleTeam,
            "solvedBy_em": user, "responsibleEmployee": user])) :
        utils.edit(subject, ["solvedBy_te": responsibleTeam, "solvedBy_em": null])
    return '';
} else /*Если нет ответственного*/ {
    return 'Ошибка: нельзя разрешить задачу без ответственного. /n Сначала назначьте ответственного или возьмите себе';
}