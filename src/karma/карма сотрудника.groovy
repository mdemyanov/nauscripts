// package karma
//Автор: mdemyanov
//Дата создания: 13.10.2012
//Назначение: Учет рейтинга сотрудников, при закрытии пользовательских запросов
/**
 * Действие на вход статуса "Закрыт": находит все запросы, разрешенные сотрудником,
 * и заполняет таблицу рейтинга. 
 */
//Версия: 4.0
//Категория: пользовательский сценарий учета рейтинга
//ПАРАМЕТРЫ------------------------------------------------------------
//Основной блок------------------------------------------------------------
// Проверяем заполненность атрибута "Разрешен сотрудником"
if (subject.solvedByEmployee) {
    def employee = subject.solvedByEmployee;
// Создаем шапку таблицы
    def htmlToEmployee = """<table width=\"100\">
<col align=\"center\" span=\"2\">
<tr>
    <th>Оценка</th>
    <th>Голосов</th>
</tr>""";
// Пишем запрос к базе данных:
// Запрос к БД находит все запросы, разрешенные данным сотрудником и имеюие атрибут "raiting"
    def myEmployeeQuery = api.db.query("""select calls.raiting.id, count(*)
from serviceCall as calls
where calls.solvedByEmployee.title = :title
group by calls.raiting.id
""");
// Задаем значение атрибуту ":title" в запросе к БД
    myEmployeeQuery.set('title', employee.title);
// Вычисляем запрос
    def result = myEmployeeQuery.list();
    if (result.size() >= 1) /*Если количество более 1, то заполняем табличку*/ {    /*По каждому элементу запроса добавляем строку в таблицу*/
        result.each({
            htmlToEmployee += "<tr><td>" + "<img src='./download?uuid=file\$" +
                    (it[0] + 100).toString() + "' alt=''>" +
                    "</td><td align=\"center\">" + it[1].toString() + "</td></tr>"
        });
        /*Добавляем "подвал" таблицы*/
        htmlToEmployee += "</table>";
        /*Редактируем значение "Кармы (Рейтинга)" сотрудника*/
        utils.edit(employee, ['karma': htmlToEmployee])
    } else /*Если количество менее 1*/ {    /*Если сотрудник еще не разрешал запросы, то мы это тоже укажем*/
        htmlToEmployee = """Пустая карма(Этот сотрудник еще не разрешил ни один
	запрос или система подсчета кармы дала ошибку)""";
        /*Редактируем значение "Кармы (Рейтинга)" сотрудника*/
        utils.edit(employee, ['karma': htmlToEmployee]);
    }
}
return ''