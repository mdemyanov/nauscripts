//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
def CLASS = 'userKarma$userKarma'; // Код справочника, элементы которого надо получить
if(null==subject)
{
    def ATTRS_FOR_UPDATE_ON_FORMS = ['clientEmployee'];//список кодов атрибутов объекта, от которых зависит фильтрация, необходимо для обновления фильтруемого селекта на форме при изменении этих атрибутов.
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
try {
	return subject?.clientEmployee?.userKarma;
}
catch(Exception e) {
	log(e.toString());
	return [];
}
