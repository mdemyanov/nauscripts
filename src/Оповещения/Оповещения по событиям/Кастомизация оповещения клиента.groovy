//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория: Кастомизация оповещения
//ПАРАМЕТРЫ----------------------------------------------------------
def CLIENT_EMAIL = 'clientEmail'; // Код атрибута "Контактный е-мэйл"
def IS_SERVICE_USER = (subject?.clientEmployee?.lastName == 'Служебный') ? true : false;
//ОСНОВНОЙ БЛОК------------------------------------------------------
if (IS_SERVICE_USER) {
    def clientEmail = subject[CLIENT_EMAIL];
    notification.to[clientEmail] = 'Неизвестный пользователь';
    notification.to.remove(subject?.clientEmployee?.email);
}
