Уважаемый , $ { subject.resposibleTeam.leader.title } !
В ответственность Вашей команды "${subject.resposibleTeam.title}" поступил < a href = " $ { api.rest.get(subject, subject.resposibleTeam.leader.login) } " > обращение № $ { subject.number } < /a>.
Краткое описание:
"${subject.shortDescr}".
Для назначения ответственного или получения детальной информации перейдите в <a href="${api.rest.get(subject, subject.resposibleTeam.leader.login)}">карточку запроса</ a >.