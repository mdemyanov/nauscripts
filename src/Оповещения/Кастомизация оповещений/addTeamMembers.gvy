//Автор: mdemyanov
//Дата создания: 02.07.2012
//Категория: Кастомизация оповещения
//Назначение: Скрипт добавляет в список получателей членов ответсвенной команды

//оповещения.
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def CLIENT_EMAIL = 'responsibleEmail'; // Код атрибута "Контактный е-мэйл"
def CLIENT_NAME = 'responsibleName'; // Код атрибута "Контактное лицо"
def SCALL = subject.parent;
def RESPONSIBLE_TEAM;
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК------------------------------------------------------------
RESPONSIBLE_TEAM = SCALL?.responsibleTeam;

if(RESPONSIBLE_TEAM) {
	//Проставляем получателей: все члены команды
	RESPONSIBLE_TEAM.memebers?.each{ employee->
		//Проставляем получателей: все члены команды
		try {
			notification.to[employee.email] = employee.title;
		}
		catch(Exception e) {
			log("Не удалось добавить получателя, по причине: ${e.toString()}");
		}		
	}
} else {
	log("У запроса отсутствует ответственный");
}