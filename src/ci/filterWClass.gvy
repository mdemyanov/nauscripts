//Автор: md
//Дата создания: 01.07.2013
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
if(null==subject)
{
    def ATTRS_FOR_UPDATE_ON_FORMS = ['unitsClass'];
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
def OBJECT = subject;
def CLASS = 'units';
def PARAMS = [
				'usedIn' : null,
				'classification' : OBJECT['unitsClass']
			];
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК------------------------------------------------------------
def UNITS = utils.find(CLASS, PARAMS);
log("Для фильтра найдено ${UNITS.size().toString()} ообъектов")
return UNITS;