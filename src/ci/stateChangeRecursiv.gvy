//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * смена статуса у зависимых компонентов
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
/*Логгирование сообщений в консоли*/
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg)
}
log("Начало работы сценария: смена статуса у зависимых компонентов.");
/*Основные переменные*/
def ACCAUNT_OBJECT = subject;
def UNITS_CODE = 'components'; /*Обеспечение - составляющие компоненты*/
def COMPONENTS_CODE = 'components'; 
def FIRST_LEVEL_UNITS = ACCAUNT_OBJECT[UNITS_CODE]; /*Колеекция компонентов первого уровня*/
def SECOND_LEVEL_UNITS = []; /*Колеекция компонентов второго уровня*/
def ALL_UNITS = []; /*Все компоненты*/
def CURRENT_STATE = ACCAUNT_OBJECT['state'];
log("Новый статус: ${CURRENT_STATE}");
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК------------------------------------------------------------
/*Заполнение коллекции компонентов второго уровня*/
log('Начинаем добавление компонентов');
FIRST_LEVEL_UNITS.each{unit->
	try {
		log('Добавление компонентов');
		SECOND_LEVEL_UNITS.addAll(unit[COMPONENTS_CODE]);
	}
	catch(Exception e) {
		log(e.toString());
	}
	
}
ALL_UNITS = FIRST_LEVEL_UNITS + SECOND_LEVEL_UNITS;
ALL_UNITS.each{unit->
	api.tx.call{
		log("Смена статуса объекта ${unit.title} на ${CURRENT_STATE}");
		try {
			utils.edit(unit, ['state' : CURRENT_STATE]);
		}
		catch(Exception e) {
			log(e.toString());
		}
	};	
}
return '';
