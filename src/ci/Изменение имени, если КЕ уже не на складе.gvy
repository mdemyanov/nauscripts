//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Изменяет отрибут "Название", исключая значение "На складе"
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК------------------------------------------------------------
def newTitle = api.string.replace(OBJECT['title'], ' [на складе]', '');
utils.edit(OBJECT, ['title' : newTitle]);


return ((null == oldSubject.usedIn) && (oldSubject.usedIn != subject.usedIn)) ?  "Нет изменений" : ''