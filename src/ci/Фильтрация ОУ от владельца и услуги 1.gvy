//Автор: md
//Дата создания: 01.07.2013
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
if(null == subject)
{
    def ATTRS_FOR_UPDATE_ON_FORMS = ['units', 'clientTeam','clientOU','clientEmployee', 'service'];
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
def CLASS = 'units$account';
def PARAMS = [:];
def EL_LIST = [];
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК------------------------------------------------------------
PARAMS = [
				'service' : OBJECT['service'],
				'owner_te' : OBJECT['clientTeam'],
				'owner_ou' : OBJECT['clientOU'],
				'owner_em' : OBJECT['clientEmployee']
			];
def COMPONENTS = [];
def UNITS = utils.find(CLASS, PARAMS);
if (user ? user.license == 'concurrent' : true) {
	UNITS.each{unit->
		COMPONENTS.addAll(unit.components);
		unit?.components?.each{unitComponent->
			COMPONENTS.addAll(unitComponent.components);
		}
	}
}
COMPONENTS.addAll(UNITS);
return COMPONENTS;