//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def CLASSES = ['units$hardware', 'units$components', 'units$software'];
def PARAMS = [
				'usedIn' : null
			];
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК--------------------------------------------------------
CLASSES.each{
	def units = utils.find(it, PARAMS);
	units.each{unit->
		def newTitle = unit.title + ' [на складе]';
		utils.edit(unit, ['title' : newTitle]);
	}
}
return '';
