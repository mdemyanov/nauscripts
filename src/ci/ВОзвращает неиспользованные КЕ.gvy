//Автор: mshalaeva
//Дата создания: 01.07.2013
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
if(null==subject)
{
    def ATTRS_FOR_UPDATE_ON_FORMS = [];
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
def CLASS = 'units';
def PARAMS = [
				'usedIn' : null
			];
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК------------------------------------------------------------
return utils.find(CLASS, PARAMS);
