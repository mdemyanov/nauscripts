// package works
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: Добавляет в запрос информацию по трудозатратам
/**
 * Если трудозатраты удалены, то удаляет из из атрибутов запроса
 */
//Версия: 4.0
//Категория: Пользовательский

//ПАРАМЕТРЫ------------------------------------------------------------
// определим запрос-источник
logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
def call = subject.serviceCall;
// Если работы уже списывались, то запомним их количество
def work = ((null == call.work) ? 0 : call.work);
// Количество новог списания
def oldWorkWork = ((null == oldSubject.works) ? 0 : oldSubject.works);
def newWorkWork = ((null == subject.works) ? 0 : subject.works);
if (0 != newWorkWork) {
    utils.edit(call, ['work': work + newWorkWork - oldWorkWork]);
}