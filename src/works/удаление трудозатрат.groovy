// package works
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: Добавляет в запрос информацию по трудозатратам
/**
 * Если трудозатраты удалены, то удаляет из из атрибутов запроса
 */
//Версия: 4.0
//Категория: Пользовательский
//ПАРАМЕТРЫ------------------------------------------------------------
def OLD_WORK = oldSubject;
workDelete(OLD_WORK);

def workDelete(works) {
    // определим запрос-источник
    def scall = works.serviceCall;
    // Если работы уже списывались, то запомним их количество
    def work = ((null == scall.work) ? 0 : scall.work);
    // Количество новог списания
    def workWork = ((null == works.works) ? 0 : works.works);
    if (0 != workWork) {
        utils.edit(scall, ['work': work - workWork,
                'countWorks': scall.countWorks - 1]);
    }
}