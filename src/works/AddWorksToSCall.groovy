// package works
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: Добавляет в запрос информацию по трудозатратам
/**
 *
 */
//Версия: 4.0
//Категория: Пользовательский
//ПАРАМЕТРЫ------------------------------------------------------------
// определим запрос-источник
def call = subject.serviceCall;
// Если работы уже списывались, то запомним их количество
def work = ((null == call.work) ? 0 : call.work);
// Количество новог списания
def workWork = ((null == subject.works) ? 0 : subject.works);
if (0 != workWork) {
    utils.edit(call, ['work': work + workWork,
            'countWorks': call.countWorks + 1]);
}
def scallDebit = { scall ->
    // Если работы уже списывались, то запомним их количество
    def work = ((null == scall.work) ? 0 : scall.work);
    // Количество новог списания
    def workWork = ((null == subject.works) ? 0 : subject.works);
    if (0 != workWork) {
        utils.edit(scall, ['work': work + workWork,
                'countWorks': scall.countWorks + 1]);
    }
    return;
}