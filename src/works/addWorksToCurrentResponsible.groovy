// package works
//Автор: mdemyanov
//Дата создания: 21.11.2012
//Назначение: Списание трудозатрат для текущего ответсвенного
/**
 * В этом сценарии настраивается спесание трудозатрат специалисту,
 * который является текущим ответственным в объекте-источнике.
 * Объектом источником может быть:
 * 1. Запрос: в этом случае трудозатраты списываются на ответст
 * венного за запрос
 * 2. Во всех остальных случаях - на ответсвенного за задание. Если задание
 * связано с запросом, то трудозатраты тоже к нему привязываются.
 */
//Версия: 4.0.0.17
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def WORK = subject;/*Сами работы = текущий объект*/
/*Определение метакласса работ*/
def WORK_METACLASS = api.metainfo.getMetaClass(WORK.getMetainfo());
def TASK = WORK?.task;/*Задача, в которой списываются трудозатраты*/
/*Метакласс задачи, если задача есть*/
def TASK_METACLASS = TASK ? api.metainfo.getMetaClass(TASK.getMetainfo()) : null;
/*Запрос, в раках которого списываются работы*/
def SCALL = WORK?.serviceCall;
def RESPONSIBLE_TEAM;/*Ответственная команда*/
def RESPONSIBLE_EMPLOYEE;/*Ответственный сотрудник*/
/*Определим функцию, которая будет списывать трудозатраты в запрос*/

def scallDebit(scall, works) {
    /*Если работы уже списывались, то запомним их количество*/
    def work = ((null == scall.work) ? 0 : scall.work);
    /*Количество новог списания*/
    def workWork = ((null == works.works) ? 0 : works.works);
    if (0 != workWork) {
        utils.edit(scall, ['work': work + workWork,
                'countWorks': scall.countWorks + 1]);
    }
}
//Основной блок--------------------------------------------------------
switch (WORK_METACLASS.code) {
    case 'work$workInScall':
        RESPONSIBLE_TEAM = SCALL.responsibleTeam;
        RESPONSIBLE_EMPLOYEE = SCALL.responsibleEmployee;
        break
    default:
        RESPONSIBLE_TEAM = TASK.responsibleTeam;
        RESPONSIBLE_EMPLOYEE = TASK.responsibleEmployee;
        SCALL = TASK_METACLASS.hasAttribute('serviceCall') ? TASK.serviceCall : null;
}
def ATRIBUTES = [
        'task': TASK,
        'serviceCall': SCALL,
        'specialist_em': RESPONSIBLE_EMPLOYEE,
        'specialist_te': RESPONSIBLE_TEAM
];
utils.edit(WORK, ATRIBUTES);
if (null != SCALL) {
    scallDebit(SCALL, WORK);
}
return '';