// package PostCFG
//Автор: skucher
//Дата создания: 10.08.2012
//Назначение:
/**
 * Регистрация нового запроса по полученному электронному письму или привязка письма к существующему запросу.
 *
 * Алгоритм работы:
 * Ищет запрос по номеру в теме письма, если находит - создает объект-письмо типа POST_FQN вложенный в запрос.
 * Если не находит - ищет контрагента запроса среди сотрудников по адресу отправителя.
 * Если не находит - ищет среди отделов тот, в атрибуте DOMAIN_CODE которого встречается домен отправителя.
 * Если не находит - использует отдел по умолчанию (DEFAULT_OU_TITLE).
 * Вычисляет тип запроса по адресу отправителя письма (соответствие задается в параметре 'SENDER_2_TYPE_CODE').
 * Если соответствие не задано, используется код дефолтного типа запроса (задается в параметре 'DEFAULT_TYPE_CODE').
 * Значения 'Уровня влияния' и 'Срочности' запроса задаются кодами элементов
 * (соотв. в параметрах 'IMPACT_CODE' и 'URGENCY_CODE');
 * Соглашение выбирается по номеру (указанному в параметре 'AGREEMENT_INVENTORY_NUMBER') среди соглашений,
 * получателем которых является найденный контрагент.
 * Услуга выбирается по номеру (указанному в параметре 'SERVICE_INVENTORY_NUMBER') среди услуг,
 * поставляемых в рамках ранее определённого соглашения.
 *
 * Описание запроса составляется из темы и тела письма и помещается в атрибут типа richtext.
 * Контактная информация - e-mail отправителя + контактные телефоны сотрудника.
 * Регистрирует запрос.
 * Создает объект-письмо типа POST_FQN вложенный в запрос.
 *
 * Автоматически переводит закрытый запрос в указанное состояние (по умолчанию отключено).
 * В случае невозможности обработки письма, отправителю уходит уведомление с указанием причины.
 * Ведется логирование всех действий скрипта (в консоли). Параметр isLoggingEnabled отвечает за логирование (false - отключено, true - включено, по умолчанию отключено)
 *
 * Результат работы скрипта должен устанавливаться в переменной окружения result.
 * Доступны следующие коды завершиения скрипта:
 * ERROR      ошибка обработки
 * NEW_BO     зарегистрирован запрос
 * ATTACH     письмо прикреплено к существующему запросу
 * REJECT     письмо отклонено
 * OUTGOING   другое (в настоящее время не используется)
 */
//Версия: 4.0
//Категория: Обработка почты

//ПАРАМЕТРЫ---------------------------------------------------------------------------------------------------
def SCALL_PREFIX = '#'          //символ, после которого в письме должен быть номер запроса

def POST_FQN = 'post$post'      //код метакласса письма
def TYPE_CODE = 'type'          //код атрибута "Тип"
def FROM_CODE = 'fromC'         //код атрибута "От кого"
def TO_CODE = 'whom'            //код атрибута "Кому"
def CC_CODE = 'copy'            //код атрибута "Копия"
def DESCR_CODE = 'descr'        //код атрибута "Описание"
def FILES_CODE = 'files'        //код атрибута "Файлы"

def POST_TYPE_FQN = 'postType'  //код справочника "Тип письма"
def POST_IN_CODE = 'in'         //код элемента справочника "Входящее"

def COPIES_CODE = 'copiesFromLett'  //код атрибута запроса "Копия"
def DESCRIPTION_CODE = 'descriptionRTF' //код атрибута запроса "Описание"
def DESCRIPTION_THEME = 'shortDescr' //код атрибута запроса "Тема" Максим: в моем случае - "Краткое описание"

def DEFAULT_OU_TITLE = '01 Unsorted' //название отдела по умолчанию

//Код атрибута "Домен" класса "Отдел". Атрибут может содержать список почтовых доменов разделенных запятой.
//Если доменная часть адреса (user@domain) отправителя письма содержиться в списке, запрос будет создан в привязке к этому отделу.
//Пример значения атрибута: "support.company.ru, it.company.ru, admins.company.ru"
def DOMAINS_CODE = 'domain'

// уровень влияёния, срочность, типы запроса, указанные ниже, должны быть настроены в системе
// код элемента справочника 'Уровни влияния'
def IMPACT_CODE = 'single'
def URGENCY_CODE = '2'      //код уровня срочности
def SENDER_2_TYPE_CODE = [  //соответствие e-mail адресов отправителей кодам типов запросов
        '': '',
        '': ''
]
def DEFAULT_TYPE_CODE = 'incidentCall'        //код дефолтного типа запроса
def AGREEMENT_INVENTORY_NUMBER = 'S99087'   //уникальный номер соглашения
def SERVICE_INVENTORY_NUMBER = 'S00054'     //уникальный номер услуги
def NEW_STATE = 'resumed'               // Если запрос найден - код целевого состояния, в которое будет переведён запрос
def LOGGING_IS_ENABLED = true          // Логирование основных событий, true - включено, false - выключено

//КЛАССЫ--------------------------------------------------------------
class ServiceCallRegistrationData {
    def client
    def agreement
    def service
}
//ОСНОВНОЙ БЛОК----------------------------------------------------------------------------------------------
def DOMAIN_SPLIT_REGEXP = '\\s*,\\s*'
def helper = api.mail.helper
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.debug(msg)
}
def sendBack = { subject, body ->
    api.mail.sender.send(message.from.address, message.from.name,
            "Re: ${message.subject}; $subject", "$body\n\n${helper.respondBody(message)}")
    log("Отправлено обратное письмо")
}
def respondByRejectOrError = {
    if (!helper.isSystemEmailAddress(message.from.address)) {
        api.mail.simpleSender.send(message.from.address, message.from.name,
                "Re: ${message.subject}", "${helper.formRespondBody(result)}\n\n${helper.respondBody(message)}")
        log("Отправлено обратное письмо с сообщением об ошибке")
    }
}
def createPost = { serviceCall ->
    log("Создание объекта $POST_FQN")
    def postAttributes = [
            parent: serviceCall,
            (FROM_CODE): message.from.address,
            (TO_CODE): message.toAsString,
            (CC_CODE): message.ccAsString,
            (DESCR_CODE): "${message.subject}${message.getHtmlBody()}",
            (TYPE_CODE): utils.get(POST_TYPE_FQN, [code: POST_IN_CODE]),
    ]
    def post = utils.create(POST_FQN, postAttributes)
    log("Объект создан. Прикрепление файлов.")
    //Прикрепление файлов вложенных в письмо
    message.attachments.each {
        utils.attachFile(post, FILES_CODE, it.filename, it.contentType, "", it.data)
    }
    log("Файлы прикреплены")

    //Назначение атрибута запроса "Копии письма"
    utils.edit(serviceCall, [(COPIES_CODE): postAttributes[CC_CODE]])
}
def determineClientOu = { Closure select ->
    log("Поиск среди сотрудников по адресу отправителя")
    def employee = helper.searchEmployeeByEmail(message.from.address)
    def result = select(employee?.parent)
    if (result)
        return result

    log("Поиск среди отделов по домену отправителя")
    def OUs = utils.find('ou', [:])
    String domain = message.from.domain
    OUs = OUs.findAll {
        it[DOMAINS_CODE]?.split(DOMAIN_SPLIT_REGEXP)?.find { it.equals(domain) }
    }
    result = select(OUs)
    if (result)
        return result
    log("Проверяем отдел по умолчанию: '$DEFAULT_OU_TITLE'")
    select(utils.get('ou', [title: DEFAULT_OU_TITLE]))
}
def selectOuWhatCanBeRegistered = { OUs ->
    def logResult = { ServiceCallRegistrationData result ->
        log("Найден отдел c подходящим соглашением и услугой: ${result.client.UUID}, ${result.agreement.UUID}, ${result.service.UUID}")
    }
    if (!OUs)
        return null
    OUs = OUs instanceof Collection ? OUs : [OUs]

    log("Проверка отделов (${OUs.size()}) на соответствие уникальным номерам соглашения и услуги")
    def result = new ServiceCallRegistrationData()
    result.client = OUs.find { ou ->
        log("Проверка отдела: ${ou.UUID}")
        def agreement = ou.recipientAgreements.find { it.inventoryNumber == AGREEMENT_INVENTORY_NUMBER }
        if (!agreement)
            return null
        result.agreement = agreement
        result.service = agreement.services.find { it.inventoryNumber == SERVICE_INVENTORY_NUMBER }
    }
    if (result.client) {
        logResult(result)
        return result
    }
    log("Проверка отделов (${OUs.size()}) на наличие только одного соглашения и/или услуги")
    result.client = OUs.find { ou ->
        log("Проверка отдела: ${ou.UUID}")
        def agreements = ou.recipientAgreements
        def agreement = agreements.find { it.inventoryNumber == AGREEMENT_INVENTORY_NUMBER }
        agreement = agreement ?: (agreements.size() == 1 ? agreements.iterator().next() : null)
        if (agreement)
            result.agreement = agreement
        else
            return null
        def services = agreement.services
        def service = services.find { it.inventoryNumber == SERVICE_INVENTORY_NUMBER }
        result.service = service ?: (services.size() == 1 ? services.iterator().next() : null)
    }
    if (result.client) {
        logResult(result)
        return result
    }
    log("Отдел с подходящим соглашением и услугой не найден")
    return null
}
/**
 * Регистрация запроса
 */
def createServiceCall = { ServiceCallRegistrationData registrationData ->
    // свойства запроса
    def props = [:]

    props.client = registrationData.client
    props.agreement = registrationData.agreement
    props.service = registrationData.service

    props.clientName = message.from.name
    props.clientEmail = message.from.address

    // свойства запроса
    // найти тип запроса
    def typeCode = SENDER_2_TYPE_CODE[message.from.address]
    typeCode = typeCode ?: DEFAULT_TYPE_CODE
    def fqn = 'serviceCall' + '$' + typeCode

    // описание запроса - из темы и тела письма
    props[DESCRIPTION_CODE] = "${message.htmlBody}"
    props[DESCRIPTION_THEME] = "${message.subject}"
    // Временная зона для запроса - по умолчанию временная зона сервера
    //def tzCode = TimeZone.getDefault().ID
    //props.timeZone = utils.get('timezone', ['code' : tzCode])
    // Уровень влияния
    props.impact = utils.get('impact', ['code': IMPACT_CODE])
    // Срочность
    props.urgency = utils.get('urgency', ['code': URGENCY_CODE])
    // формируем новый запрос
    def serviceCall = utils.create(fqn, props)
    result.messageState = api.mail.NEW_BO_MSG_STATE
    return serviceCall
}

def process = {
    // Ищем запрос по теме письма
    def serviceCall = helper.searchByCallNumberWithPrefix(message, SCALL_PREFIX)
    // Если запрос не найден, регистрируем новый
    if (!serviceCall) {
        log("Запрос не найден. Регистрируем новый.")
        ServiceCallRegistrationData registrationData = determineClientOu(selectOuWhatCanBeRegistered)
        if (!registrationData) {
            result.reject(api.mail.OTHER_REJECT_REASON)
            log("Не удалось получить данные, необходимые для регистрации запроса")
            return
        }
        serviceCall = createServiceCall(registrationData)
        if (serviceCall) {
            log("Зарегистрирован '${serviceCall.title}'")
            createPost(serviceCall)
            helper.notifyMailReceived(serviceCall)
            //sendBack("Запрос #${serviceCall.number}","\nПо вашему обращению зарегистрирован запрос #${serviceCall.number}\n_ _С уважением,\nПлатежная система \"Рапида\"\nТел.: +7 (495) 380-1544\nwww.rapida.ru")
        }
    } else {   // Запрос найден.
        def client = determineClientOu { it instanceof Collection ? it.iterator().next() : it }
        if (client) {
            log("Запрос найден. Добавляем к нему комментарий и объект $POST_FQN.")
            //utils.create('comment', [ source : serviceCall.UUID, text : message.htmlBody, ])
            // Переводим запрос в новое состояние (возобновляем, если закрыт).СЕЙЧАС ВОЗМОЖНОСТЬ ОТКЛЮЧЕНА! Для включения нужно убрать "//" в начале следующей строки
            // Включено, добавлен просчет

            createPost(serviceCall)
            result.messageState = api.mail.ATTACH_MSG_STATE
            //helper.notifyMailReceived(serviceCall)
            //sendBack('', "\nВаше письмо прикреплено к запросу #${serviceCall.number}")
        } else
            sendBack('', "\nНе удалось определить отправителя")
    }
}

// main
log("Скрипт обработки почты")
if (result.rejected) {
    log("reason is " + result.rejectReason)
    respondByRejectOrError()
    return
}
try {
    process()
}
catch (e) {
    result.error("Возникли ошибки обработки письма")
    logger.error(e.message, e)
}
// Если были ошибки обработки или письмо отклонено, уведомляем отправителя
if (!result.successfull)
    respondByRejectOrError()