// package PostCFG
//Автор: skucher
//Дата создания: 29.08.2012
//Назначение:
/**
 * Действие на создание объекта класса post$post.
 * Посылает электронное сообщение составленное на основе объекта Post.
 */
//Версия: 4.0
//Категория: Действия по событию типа скрипт
logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
def post = subject
def scall = post.parent
//ПАРАМЕТРЫ------------------------------------------------------------
def SUBJECT = "" //шаблон темы письма
def SIGNATURE = "" //шаблон подписи письма

def POST_FQN = 'post$post'  //код метакласса письма
def TYPE = 'type'           //код атрибута "Тип"
def FROM = 'fromC'          //код атрибута "От кого"
def TO = 'whom'             //код атрибута "Кому"
def CC = 'copy'             //код атрибута "Копия"
def DESCRIPTION = 'descr'   //код атрибута "Описание"
def FILES = 'files'         //код атрибута "Файлы"

def POST_TYPE_FQN = 'postType' //код справочника "Тип письма"
def POST_OUT_CODE = 'out'      //код элемента справочника "Исходящее"
def POST_IN_CODE = 'in'        //код элемента справочника "Входящее"

def SCALL_CC = 'copiesFromLett' //код атрибута запроса "Копия"

// Шаблон темы письма. Возможно использование переменных post (объект письма) и scall (запрос к которому привязано письмо)
def SUBJECT_TEMPLATE = "Обращение #${scall.number}. Тема: ${scall.title}"

// Шаблон подписи к письму. Возможно использование переменных post (объект письма) и scall (запрос к которому привязано письмо)
def SIGNATURE_TEMPLATE = """<font color=\"gray\" face=\"verdana\" size=\"3\">
        <br>
        С уважением,<br>
        Служба поддержки пользователей,<br>
        NAUMEN. Информационные системы управления растущим бизнесом.<br>
        Телефон: +7(495)783-02-87 доб. 3015<br>
        Мобильный: +7-963-699-92-95<br>
        Email: <a href=\"salessd4@sd-dev2.naumen.ru\">salessd4@sd-dev2.naumen.ru</a>
        <br><br>Naumen Service Desk в сети интернет:<br><br>
<a href=\"http://www.naumensd.ru/\" title=\"Сайт Naumen Service Desk 4.0\"><img src=\"http://download.naumen.ru/upload/SALES/logo_sign.png\" height=\"28\" alt=\"Сайт Naumen Service Desk 4.0     \"></a>
        <a href=\"http://www.linkedin.com/groups/Naumen-Service-Desk-4359014?gid=4359014\"><img src=\"http://download.naumen.ru/upload/SALES/linkedin.png\" height=\"28\" alt=\"Мы в LinkedIN  \"></a>
        <a href=\"http://www.facebook.com/NaumenServiceDesk"><img src="http://download.naumen.ru/upload/SALES/facebook.png\" height=\"28\" alt=\"Мы в Facebook   \"></a>
        <a href=\"https://twitter.com/naumensd\"><img src=\"http://download.naumen.ru/upload/SALES/twitter.png\" height=\"28\" alt=\"Наш Twitter   \"></a>
    </font>
    </font></div>"""

//def SUPPORT_ADDRESS = "salessd4@sd-dev2.naumen.ru" //адрес, который используется когда привязкой запроса является "Физическое лицо"
def SUPPORT_ADDRESS = "salessd4@sd-dev2.naumen.ru"

//сообщение об ошибке вызванной пустым атрибутом запроса 'Контактный e-mail'
def errorEmptyClientEmail = "Письмо в рамках запроса #${scall.number} не было отправлено из-за отсутствия адреса 'Кому'"

LOGGING_IS_ENABLED = true //true - логирование включено, false - логирование выключено
//ФУНКЦИИ--------------------------------------------------------------
def isValidEmail(String email) {
    def parts = email.split('@')
    try {
        return parts[0] && parts[1] && parts.size() == 2
    }
    catch (e) {
        return false
    }
}
//ОСНОВНОЙ БЛОК--------------------------------------------------------
final def splitTemplate = "\\s*;\\s*"
def log = { message ->
    if (LOGGING_IS_ENABLED)
        logger.debug(message)
}
def eachEmail = { String list, String attribute, Closure closure ->
    list?.split(splitTemplate)?.each { email ->
        log "Обрабатывается адрес эл. почты: $email"
        if (isValidEmail(email))
            closure(email)
        else
            log "Неправильно указан e-mail адрес '$email' в поле '$attribute'"
    }
}

def message = api.mail.sender.createMail()
log "Определение параметров сообщения"
//def to = scall.clientEmail
def to = post.whom
if (!to) {
    log(errorEmptyClientEmail)
    return
}
//def cc = scall[SCALL_CC]
def cc = post.copy
def from = SUPPORT_ADDRESS //TODO реализовать корректное определение адреса отправителя

if (post[TYPE]?.code != POST_OUT_CODE) {
    log "Объект $POST_FQN не является исходящим сообщением. Завершаем работу скрипта."
    return
}

log "Заполнение полей сообщения"
eachEmail(to, 'Кому') { message.addTo('', it) }
eachEmail(cc, 'Копия') { message.addCc('', it) }
message.setSubject(SUBJECT_TEMPLATE)
message.setText("${post[DESCRIPTION]}<br>$SIGNATURE_TEMPLATE", message.TEXT_HTML)
//post[FILES]?.each { message.attachFile(it) } //TODO реализовать прикрепление файлов
post[FILES].each({
    def fileName = it.title //Получаем имя файла
    def contentType = it.mimeType //Получаем тип файла
    def description = '' //Заполняем описание файла
    //Получаем источник данных
    def ds = utils.getFileDataSource(it)
    //получаем сами даные
    def data = org.apache.commons.io.IOUtils.toByteArray(ds.getInputStream())
    // Добавляем новый файл в объект subject
    message.attachFile(ds)
})

message.setFrom('', from)

log "Отправка сообщения"
api.mail.sender.sendMail(message)

log "Автозаполнение полей объекта $POST_FQN"
utils.edit(post, [(TO): to, (CC): cc, (FROM): from, (TYPE): utils.get(POST_TYPE_FQN, [code: POST_OUT_CODE])])