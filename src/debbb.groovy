//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def CLASSES = ['ou', 'employee'];
def AGREEMENT_ATTR = 'recipientAgreements';
def COUNT = 0;
//ФУНКЦИИ--------------------------------------------------------------
def changeToDefault = { obj ->
    def metaClass = api.metainfo.getMetaClass(obj);
    def agreement = metaClass.getAttribute(AGREEMENT_ATTR);
    def defaultValue = agreement.getDefaultValue();
    utils.edit(obj, ['recipientAgreements': defaultValue]);
    return 1;
}
//ОСНОВНОЙ БЛОК------------------------------------------------------------
CLASSES.each { objClass ->
    def objs = utils.find(objClass, [:]);
    objs.each { obj ->
        try {
            COUNT += changeToDefault(obj);
        }
        catch (Exception e) {
            log("Опреация не удалась по причине ${e.toString()}");
        }
    }
}
return 'Hello, World!'