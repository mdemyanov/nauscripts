// package kb
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: СОздает статью БЗ из запроса
/**
 *
 */
//Версия: 4.0
//Категория: 

//Основной блок------------------------------------------------------------
if (subject.addToKB) {
    def section = utils.get('sectionKB$section', ['service': subject.service])
    def name = "Из " + subject.title
    utils.create('article$article', ['title': name,
            'text': subject.solutionRTF,
            'author': subject?.solvedByEmployee,
            'parent': section])
}
return ''