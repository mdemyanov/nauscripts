//Автор: mdemyanov
//Дата создания: 09.10.2012
//Назначение: создание голосований в согласовании по запросам
/**
 * Создание голосований в согласовании при входе в состояние "В процессе"
 * При настройке технологу необходмо указать код типа голосования
 * в формате "vote$type_code" в атрибуте VOTE_FQN
*/
//Версия: 4.0
//Категория: создание вложенных объектов
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
// таблица соответсвия кодов атрибутов
def rulesSettings = [
'cabOU' : 'voter_ou',
'cabEmploers' : 'voter_em',
'cabTeams' : 'voter_te',
];
// Атрибут "Тип голосования"
def VOTE_FQN = 'vote$vote';
// Атрибут "согласование источник""
def PARENT = 'parent';
// Дата голосования
def VOTE_DATE = 'voteDate';
// Код атрибута название
def TITLE = 'title';
// Код группы атрибутов
def CAB_GROUP_CODE = 'd846b316-d696-4060-b550-d6376d0a52c6';
//Контейнер атрибутов
def attrs = [:];
//Основной блок---------------------------------------------------------
// Назначаем согласование родитель (текущий объект)
attrs[PARENT] = OBJECT;
// Копируем названия из согласования
attrs[TITLE] = OBJECT[TITLE];
// Заполняем плановю дату голосования
attrs[VOTE_DATE] = OBJECT.deadLine.deadLineTime;
/*Получим метакласс согласования*/
def metaClass = api.metainfo.getMetaClass(OBJECT.metaClass);
// получим группу атрибутов согласующего комитета
def CABGroup = metaClass.getAttributeGroup(CAB_GROUP_CODE);
try {
	/*Для каждого атрибута в группе*/
	CABGroup.attributeCodes.each{cabinet->
			/*Для каждого участника согласующего комитета*/
			OBJECT[cabinet].each{voter->
				/*Заполним атрибут "Голосующий" в зависимости от типа голосующего*/
				attrs[rulesSettings.get(cabinet)] = voter;
				/*Создадим объект "Голосование"*/
				utils.create(VOTE_FQN, attrs);
				/*Очистим переменную с согласующим*/
				attrs[rulesSettings.get(cabinet)] = null;
			}
	}
}
/*Если произвошла ошибка - викинем в лог*/
catch(Exception e) {
	log(e);
}
return '';