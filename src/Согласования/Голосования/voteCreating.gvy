//Автор: mdemyanov
//Дата создания: 09.10.2012
//Назначение: создание голосований в согласовании по запросам
/**
 * Создание голосований в согласовании при входе в состояние "В процессе"
 * При настройке технологу необходмо указать код типа голосования
 * в формате "vote$type_code" в атрибуте VOTE_FQN
*/
//Версия: 4.0
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
    return '';
};
//ПАРАМЕТРЫ------------------------------------------------------------
//Категория: создание вложенных объектов
// Атрибут "Тип голосования"
def VOTE_FQN = 'vote$interview';
// Атрибут "согласование источник""
def PARENT = subject;
// Дата голосования
def VOTE_DATE = 'voteDate';
// Соответсвие согласующего кабинета и  атрибута в голосовании
def CABINET = [
	'cabEmploers' : 'voter_em',
	'cabOU' : 'voter_ou',
	'cabTeams' : 'voter_te',
];
def VOTERS_COUNTER = 0;
//Контейнер атрибутов
def attrs = [:];
//ФУНКЦИИ--------------------------------------------------------------
def voteCreator = {voter, voterAttr->
	def vote;
	try {
		attrs[voterAttr] = voter;
		vote = utils.create(VOTE_FQN, attrs);
		attrs[voterAttr] = null;
	}
	catch(Exception e) {
		log("Объект не создан по причине: ${e.toString()}");
	}
	return vote ? 1 : 0;	
};
//Основной блок---------------------------------------------------------
// Назначаем согласование родитель (текущий объект)
attrs['parent'] = PARENT;
// Заполняем плановю дату голосования
attrs[VOTE_DATE] = PARENT?.deadLine?.deadLineTime;
// Создаем голосования
CABINET.each{cab->
	PARENT[cab.key].each{voter->
		VOTERS_COUNTER+=voteCreator(voter, cab.value);
	};
};
log("Создание голосований: создано ${VOTERS_COUNTER.toString()} объекта(ов).");
return '';