//Автор: mdemyanov
//Дата создания: 08.02.2013
//Назначение:
/**
 * Сценарий на вход в статус голосования "Согласовано"
 * Если все проголосовали "За"
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
/*Код класса глосований*/
def VOTE_CODE = 'vote';
/*Код отказа*/
def CLOSED = 'closed';
/*Сод согласия*/
def ACCEPTED = 'accepted';
/*Начальный статус*/
def REGISTERD = 'registered';
/*Ссылка на согласование*/
def PARENT = subject.parent;
/*Если еще не голосовал, то 1*/
def VOTED = (oldSubject.state == REGISTERD) ? 1 : 0;
/*Результирующий вердикт*/
def RESULT = '';
//MAIN------------------------------------------------------------
/*Количество неотвеченных голосваний - VOTED (текущее)*/
def NO_VOTES = utils.find(VOTE_CODE,
        ['parent': PARENT, 'state': REGISTERD]).size() - VOTED;
/*Количество  "Против" (если преходв в статус "Отклонено", то +1)*/
def VOTES_CLOSED = utils.find(VOTE_CODE,
        ['parent': PARENT, 'state': CLOSED]).size();
/*Количество  "За" (если преходв в статус "Согаласовано", то +1)*/
def VOTES_ACCEPTED = 1 + utils.find(VOTE_CODE,
        ['parent': PARENT, 'state': ACCEPTED]).size();
/*Если все проголосовали, то выполняем*/
if (!NO_VOTES) {
    RESULT = (VOTES_CLOSED < 1) ? ACCEPTED : CLOSED;
    utils.edit(PARENT, ['state': RESULT])
}
if (comment) {
    try {
        utils.create('comment', ['source': PARENT, 'text': comment, 'author': user]);
    }
    catch (Exception e) {

    }
}
return '';