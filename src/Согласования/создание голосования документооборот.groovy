// Атрибут "согласование источник""
def parent = 'parent'
// Дата голосования
def voteDate = 'voteDate'
//Контейнер атрибутов
def attrs = [:]
// Назначаем согласование родитель (текущий объект)
attrs[parent] = subject
// Заполняем плановю дату голосования
attrs[voteDate] = subject.deadLine
// Создаем голосования для сотрудников
if (subject.cabEmploers) {
    def voterEmployee = 'voter_em'
    def employers = subject.cabEmploers
    for (def emloyee in employers) {
        attrs[voterEmployee] = emloyee
        utils.create('vote$docFlowVote', attrs)
        attrs[voterEmployee] = null
        logger.info('!!!!!!!!!!!!!! работаем сотрудников')
    }
}
// Создаем голосования для отделов
if (subject.cabOU) {
    def voterOU = 'voter_ou'
    def oues = subject.cabOU
    for (def ou in oues) {
        attrs[voterOU] = ou
        utils.create('vote$docFlowVote', attrs)
        attrs[voterOU] = null
        logger.info('!!!!!!!!!!!!!! работаем отделов')
    }
}
// Создаем голосования для команд
if (subject.cabTeams) {
    def voterTeam = 'voter_te'
    def teams = subject.cabTeams
    for (def ou in teams) {
        attrs[voterTeam] = ou
        utils.create('vote$docFlowVote', attrs)
        attrs[voterTeam] = null
        logger.info('!!!!!!!!!!!!!! работаем команд')
    }
} else return ''
return ''