//Автор: mdemyanov
//Дата создания: 21.11.2012
//Назначение: Создание объекта "Согласование"
/**
 * Создает согласование в запросе на изменение при переходе в соответсвующий статус
 */
//Версия: 4.0.0.17
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
/*Создание согласования в запросе на изменение*/
/*Контейнер для хранения изменяемых атрибутов*/
def NEGOTIATIONS_ATTRS = [:];
/*код атрибута "согласующий комитет: сотрудники" согласования*/
def CAB_EMPLOERS = 'cabEmploers';
def CAB_OU = 'cabOU';
def CAB_TEAM = 'cabTeams';
/*Ответственный за согласование*/
def RESPONSIBLE = 'responsible';
/*Описание согласования*/
def DESCRIPTION = 'description';
/*Атрибут "В рамках запроса"*/
def SCALL = 'serviceCall';
/*код атрибута "Класс обслуживания"*/
def SERVICE_TIME = 'serviceTime';
/*код атрибута "период согласования"*/
def NEGOTIATIONS_TM = 'negotiationTM';
/*код атрибута "дата завершения"*/
def DEADLINE = 'deadLine';
/*Заполняем описание согласования описанием запроса*/
NEGOTIATIONS_ATTRS[DESCRIPTION] = subject.descriptionRTF;
/*Добавляем в согласующий комитет владельцев КЕ*/
NEGOTIATIONS_ATTRS[CAB_EMPLOERS] = subject.relToKE?.ownedBy_em;/*Сотрудник*/
NEGOTIATIONS_ATTRS[CAB_OU] = subject.relToKE?.ownedBy_ou;/*Отдел*/
NEGOTIATIONS_ATTRS[CAB_TEAM] = subject.relToKE?.ownedBy_te;/*Команда*/
/*Ответственный за согласование*/
NEGOTIATIONS_ATTRS[RESPONSIBLE] = subject.responsible;/*Ответственый за запрос*/
/*Заполняем источник согласования*/
NEGOTIATIONS_ATTRS[SCALL] = subject;
/*Заполняем плановую дату завершения согласования*/
NEGOTIATIONS_ATTRS[DEADLINE] = api.timing.serviceEndTime(subject.serviceTime, /*Время обслуживания*/
        subject.timeZone, /*Часовой пояс*/
        new Date(), /*Текущая дата*/
        subject['resolutionTime'].toMiliseconds());/*Время на решение запроса*/
/*Создаем согласование с набором параметров*/
utils.create('negotiation$callNegot', NEGOTIATIONS_ATTRS);