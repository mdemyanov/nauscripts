//Автор: mdemyanov && skucher
//Дата создания: 07.07.2012
//Назначение:
/**
 * ВНИМАНИЕ! СКРИПТ НЕ ТЕСТИРОВАЛСЯ!
 * Документооборот #1
 * Создает согласование договора с руководителем отдела.
 * Копирует атрибуты договора в описание согласования.
 * Копирует файлы из договора в согласование.
 */
//Версия: 4.0
//Категория: Сервисы пользователей

//ПАРАМЕТРЫ------------------------------------------------------------
def NEGOTIATION = 'negotiation$docFlowNegot' // тип согласования: в рамках документооборота
// Коды атрибутов
def CAB = 'cabEmploers'            // согласующий коммитет
def CONTRACT = 'docFlow'       // договор
def CONTRACT_TYPE = 'contrType'
//def CLIENT          = 'client'         // когтрагент
def CONTRACT_CAT = 'contrCatergory'    // категория договора
def DESCRIPTION = 'description'          // краткое описание
def FILE = 'file'           // файл

//ОСНОВНОЙ БЛОК--------------------------------------------------------
def contract = subject // объект договора
def head = contract.client_em.immediateSupervisor // руководитель

// Подготовка атрибутов согласования
def attrs = [(CAB): head, (CONTRACT): contract]
//еделим таблицу соответсвий атрибутов
def appropriate = ['contrType': 'contrType',
        'contrCatergory': 'contrCatergory',
        'description': 'description']
appropriate.each({ key, value ->
    attrs[key] = contract[value]
})
// Создание объекта согласования
utils.create(NEGOTIATION, attrs)
