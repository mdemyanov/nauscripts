//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Создание согласования в запросе на предоставление доступа
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
/*Контейнер для хранения изменяемых атрибутов*/
def NEGOTIATIONS_ATTRS = [:];
/*код атрибута "согласующий комитет: сотрудники" согласования*/
def CAB_EMPLOERS = 'cabEmploers';
def CAB_OU = 'cabOU';
def CAB_TEAM = 'cabTeams';
/*Ответственный за согласование*/
def RESPONSIBLE = 'responsible';
/*Название согласование */
def TITLE = 'title';
/*Описание согласования*/
def DESCRIPTION = 'description';
/*Атрибут "В рамках запроса"*/
def SCALL = 'serviceCall';
/*код атрибута "Часовой пояс"*/
def TIME_ZONE = 'timeZone';
/*код атрибута "Класс обслуживания"*/
def SERVICE_TIME = 'serviceTime';
/*код атрибута "период согласования"*/
def NEGOTIATIONS_TM = 'negotiationTM';
// Основной блок-----------------------------------------------------------
NEGOTIATIONS_ATTRS[TITLE] = 'Запрос ' + subject.title + ': ' + subject?.clientEmployee?.title;
/*Заполняем описание согласования описанием запроса*/
/*Заполняем строковую переменную с перечнем систем*/
def systems = '';
subject.system.each {
    systems += it.title;
    systems += '<br>';
}
/*Заполняем строковую переменную с перечнем прав*/
def rules = '';
subject.rules.each {
    rules += it.title;
    rules += '<br>';
}
NEGOTIATIONS_ATTRS[DESCRIPTION] = """
Доступ в системы: ${systems}
C правами: ${rules}
""";
/*Заполняем согласующий комитет*/
/*руководитель сотрудника*/
NEGOTIATIONS_ATTRS[CAB_EMPLOERS] = [subject?.clientEmployee?.immediateSupervisor];
/*Куратор услуги*/
NEGOTIATIONS_ATTRS[CAB_EMPLOERS].add(subject?.service?.responsibleTeam?.leader);
/*Ответственный за согласование*/
NEGOTIATIONS_ATTRS[RESPONSIBLE] = subject.responsible;/*Ответственый за запрос*/
/*Заполняем источник согласования*/
NEGOTIATIONS_ATTRS[SCALL] = subject;
/*копирование временных параметров запроса*/
NEGOTIATIONS_ATTRS[TIME_ZONE] = subject[TIME_ZONE];
NEGOTIATIONS_ATTRS[SERVICE_TIME] = subject[SERVICE_TIME];
/*Создаем согласование с набором параметров*/
utils.create('negotiation$callNegot', NEGOTIATIONS_ATTRS);
