// если голосование против
//находим все голосования по общему родителю
def votes = utils.find('vote', ["parent": subject.parent])
//начальное значение голосов, при условии что данное голосование "за"
def yes = 0
def no = 1
// переменная для проверки завершенности голосований
def isFinished = true
for (def vote in votes) {
    //Подсчет количества голосов и проверка завершенности голосований, если хотя бы одно из голосований не завершено, то isFinished = false
    ((vote.state == 'accepted') ? (yes++) : ((vote.state == 'closed') ? (no++) : (isFinished = false)))
}
//если все проголосовали, то переводим родителя в следующее состояние: согласовано или отклонено
(((isFinished == true) && (subject.parent.state != 'closed')) ? ((subject.parent.state != 'closed') ? (utils.edit(subject.parent, ['state': "closed"])) : (utils.edit(subject.parent, ['state': "accepted"]))) : (isFinished = false))
return ''