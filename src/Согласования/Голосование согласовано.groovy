//находим все голосования по общему родителю
def votes = utils.find('vote', ["parent": subject.parent])
//начальное значение голосов, при условии что данное голосование "за"
def yes = 1
def no = 0
def isFinished = true
for (def vote in votes) {
    ((vote.state == 'accepted') ? (yes++) : ((vote.state == 'closed') ? (no++) : (isFinished = false)))
}
(((isFinished == true) && (subject.parent.state != 'accepted')) ?
    ((no == 0) ? (utils.edit(subject.parent, ['state': "accepted"])) :
        ((subject.parent.state != "closed") ? (utils.edit(subject.parent, ['state': "closed"])) : (isFinished = false))) : (isFinished = false))
return ''