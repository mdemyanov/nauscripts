//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
def SCALL = subject;
def startTime = new Date();
def map = [
	'description' : 'extDescr',
	'shortDeskr' : 'title',
	'timeToDo' : 'timeToDo',
	'responsibleTeam' : 'responsibleTeam',
	'responsibleEmployee' : 'responsibleEmployee'
];
try {
	SCALL.workTypes.each{workType->
		def params = ['serviceCall' : SCALL];
		params.put('startTime', startTime);
		map.each{
			params.put(it.key, workType[it.value]);
		}
		
		utils.create('task$tasksInSCalls', params);
	}
}
catch(Exception e) {
	log(e);
}
return '';