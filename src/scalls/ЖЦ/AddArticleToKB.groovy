// package scalls.ЖЦ
//Автор: mdemyanov
//Дата создания: 28.11.2012
//Назначение: действие на вход в статус
/**
 * При входе в статус "Разрешен", если стоит флаг "Добавить в базу знаний", 
 * то создает статью в соответствующем разделе
 */
//Версия: 4.0.1
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def ATRIBUTES = [:];            /*Контейнер для хранения атрибутов*/
def TITLE = 'title';            /*Код атрибута "Название статьи"*/
def TEXT = 'text';                /*Код атрибута "Текст статьи"*/
def AUTHOR = 'author';            /*Код атрибута "Автор"*/
def PARENT = 'parent';            /*Код атрибута "Раздел Базы знаний"*/
def CATEGORIES = 'categories';    /*Код атрибута "Категории"*/
//Основной блок------------------------------------------------------------
if (subject.addToKB)/*Если стоит галочка "Добавить в БЗ"*/ {
    /*Найдем раздел, соответствующий услуге по запросу*/
    ATRIBUTES[PARENT] = utils.get('sectionKB$section', ['service': subject?.service]);
    ATRIBUTES[TITLE] = "Из " + subject.title;        /*Заполняем "Название"*/
    ATRIBUTES[TEXT] = subject.solutionRTF;        /*Заполняем "Текст статьи"*/
    ATRIBUTES[AUTHOR] = subject?.solvedByEmployee;    /*Заполняем "Автор статьи"*/
    ATRIBUTES[CATEGORIES] = subject.categories;    /*Заполняем "Категории"*/
    utils.create('article$article', ATRIBUTES);
}
return ''