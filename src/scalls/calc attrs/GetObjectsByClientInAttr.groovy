//Автор: mdemyanov
//Дата создания: 13.11.2012
//Назначение: 
/**
 * Скрипт возвращает список объектов указанного метакласса в зависимости от того, содержится ли клиент запроса в указанном атрибуте объекта.
 * Код метакласса объекта и код атрибута этого метакласса типа "Ссылка на БО"(класс "Отдел", "Сотрудник" или "Команда") указывается в параметрах скрипта.
 */
//Версия: 4.0
//Категория: Фильтрация выпадающих списков
//ПАРАМЕТРЫ------------------------------------------------------------
def CLS_CODE = ''; // Код класса объектов, список которых необходимо получить
def CLIENT_EMPLOYEE_ATTR_CODE = ''; // Код атрибута типа "Ссылка на БО", в котором лежит ссылка на контрагента запроса. Атрибут определен в классе с кодом, заданном в CLS_CODE
def CLIENT_OU_ATTR_CODE = ''; /*Код атрибута ссылки на отдел*/
def CLIENT_TEAM_ATTR_CODE = ''; /*Код атрибута ссылки на Команду*/

if (null == subject) {
    def ATTRS_FOR_UPDATE_ON_FORMS = ['client', 'clientEmployee', 'clientOU', 'clientTeam'];
    // список кодов атрибутов объекта, от которых зависит фильтрация, необходимо для обновления 
    // фильтруемого селекта на форме при изменении этих атрибутов.
    // По умолчанию, все атрибуты-ссылки на клиента
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
def objects = []; /*Список объектов*/
def clientEmployee = subject.clientEmployee; /*Клиент сотрудник*/
def clientOU = subject.clientOU; /*Клиент отдел*/
def clientTeam = subject.clientTeam; /*Клиент команда*/
//Основной блок------------------------------------------------------------
objects.addAll(clientEmployee ? utils.find(CLS_CODE, [(CLIENT_EMPLOYEE_ATTR_CODE): clientEmployee.UUID]) : /*Если запрос на сотрудника*/
    (clientOU ? utils.find(CLS_CODE, [(CLIENT_OU_ATTR_CODE): clientOU.UUID]) : /*Если запрос на отдел*/
        (clientTeam ? utils.find(CLS_CODE, [(CLIENT_TEAM_ATTR_CODE): clientTeam.UUID]) : []))); /*Если запрос на команду*/
//Возвращаемое значение------------------------------------------------------------
return objects;