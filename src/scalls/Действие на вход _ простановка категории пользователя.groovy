//Автор: mdemyanov
//Дата создания: 09.10.2012
//Назначение: заполняет атрибут "Категория пользователя", определяет способ обращения
/**
 * Проставляет категорию пользователя при регистрации нового запроса
 */
//Версия: 4.0
//Категория: изменение значений атрибутов
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def WAY_OF = 'wayOf';
def USER_CATEGORY = 'userCategory';
def attrs = [:];
//Основной блок--------------------------------------------------------
// Заполняем категорию пользователя
attrs[USER_CATEGORY] = subject?.clientEmployee?.categoriesUser;
/*Если ватор запроса - null, то заполняем "Обращение по почте"*/
switch (subject?.author) {
    case null:
        attrs[WAY_OF] = utils.get('wayOfAddressing', ['code': 'email']);
        break
/*Если автор запроса - контрагент, то заполняем "Обращение в ЛК"*/
    case subject?.clientEmployee:
        attrs[WAY_OF] = utils.get('wayOfAddressing', ['code': 'privateOffice']);
        break
/*Во всех остальных случая - ставится обращение через оператора*/
    default:
        attrs[WAY_OF] = utils.get('wayOfAddressing', ['code': 'operator']);
        break
}
// редактируем запрос, проставляем вычисленные атрибуты
utils.edit(subject, attrs);