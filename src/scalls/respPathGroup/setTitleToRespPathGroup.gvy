//Автор: mdemyanov
//Дата создания: 02.07.2012
//Код: PartNumber из naupp
//Назначение:
/**
 * Открывает для пользователя возможность просматривать новости
 */
//Версия: 4.0.1.x
//Категория: Вычислимая роль
// Логгирование:
/*По умолчанию логгирование выключено*/
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
def SERVICE = OBJECT?.service;
def RESPONSIBLE_TEAM = OBJECT?.responsibleTeam;
def RESPONSIBLE_EMPLOYEE = OBJECT?.responsibleEmployee;
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК--------------------------------------------------------
def title;
if (RESPONSIBLE_EMPLOYEE) {
	title = """
		Поддержка '${SERVICE.title}' сотрудником 
		${RESPONSIBLE_EMPLOYEE.title} из команды '${RESPONSIBLE_TEAM.title}'.
	""";
} else {
	title = """
		Поддержка '${SERVICE.title}' командой '${RESPONSIBLE_TEAM.title}'.
	""";
}
try {
	utils.edit(OBJECT, ['title' : title]);
}
catch(Exception e) {
	log(e.toString());
}
return '';