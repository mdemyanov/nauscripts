//Автор: mdemyanov
//Дата создания: 02.07.2012
//Код: PartNumber из naupp
//Назначение:
/**
 * Открывает для пользователя возможность просматривать новости
 */
//Версия: 4.0.1.x
//Категория: Вычислимая роль
// Логгирование:
/*По умолчанию логгирование выключено*/
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
def SERVICE = OBJECT?.service;
def CURATOR_FQN = 'RespPartnGroup';
//ФУНКЦИИ--------------------------------------------------------------
def getCurrentLocation = {
	def location = (OBJECT?.clientEmployee?.location) ?:
		((OBJECT?.clientOU?.location) ?: null);
		return location;
};
def getServiceResponsible = {location->
	def responsible = [:];
	log("Ответственный: ${responsible.size()}")
	while((responsible.size() == 0) && null != location) {
		def RPG = utils.find(CURATOR_FQN, ['service' : SERVICE,
		'location' : location, 'removed' : false]);
		if (RPG.size() != 0) {
			responsible = ['team' : RPG[0]?.responsibleTeam,
				'employee' : RPG[0]?.responsibleEmployee];
		}
		location = location.parent;
	}
	log("""Ответственная команда: ${responsible['team']?.title}; 
		ответственный сотрудник: ${responsible['employee']?.title}.""")
	return responsible;
};
//ОСНОВНОЙ БЛОК--------------------------------------------------------
def location = getCurrentLocation();
if(location) {
	def responsible = getServiceResponsible(location);
	if (responsible.size()) {
		utils.edit(OBJECT, ['responsibleTeam' : responsible['team'],
			'responsibleEmployee' : responsible['employee']]);
	}
}
return '';