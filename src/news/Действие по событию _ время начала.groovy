// package news
// действие по событию "наступление времени атрибута"
// Перевод в статус осуществляется в одно время с наступлением атрибута "Время начала", 
// при условии, что новость находится в статусе "План", 
// иначе статус будет изменен на "Закрыта"
switch (subject.state) {
    case 'plan': utils.edit(subject, ['state': 'active'])
        break
    case 'registered': utils.edit(subject, ['state': 'closed'])
        break
}
return ''