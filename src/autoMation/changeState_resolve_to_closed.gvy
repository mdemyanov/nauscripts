//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Скрипт предназначен для автоматической смены статусов запросов:
 * из статуса "Разрешен" в статус "Закрыто"
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def RANDOM = new Random(); /*Используется для получения рандомного числа*/
def SCALLS = [];/*Текущие запросы в системе*/
def SCALLS_FQN = 'serviceCall$userCalls';
def CURRENT_STATES = ['resolved'];/*Текущие сстатусы*/
def NEW_STATE = 'closed'
def SCALL_PARAMS = ['state' : CURRENT_STATES]; /*Параметры для поиска текущих запросов*/
//ФУНКЦИИ--------------------------------------------------------------
/*Ищет запросы по типу и параметрам */
def scallFinder = {scallFqn, params->
	def scalls = utils.find(scallFqn, params);
	return scalls;
};
/*Получение комментариев*/
def getComments = {scall->
	def comments = utils.comments(scall);
	def cCount = comments.size();
	def resultComments = [];
	// оставляем только комментарии первой линии
	log("Поиск комментариев пользователя");
	if (cCount > 2) {
		comments[cCount - 1]?.author?.license == 'unlicensed' ? resultComments.add(comments[cCount - 1]) : log('NoComments');
		comments[cCount - 2]?.author?.license == 'unlicensed' ? resultComments.add(comments[cCount - 2]) : log('NoComments');
	}
	return resultComments;
};
/*Постинг комментариев*/
def postComments = {scall, parentCall->
	def comments = getComments(parentCall);
	/*Просмотр списка*/
	comments.each{comment->
		utils.create('comment', ['source' : scall.UUID, 
			'text' : comment.text, 
			'author' : scall?.clientEmployee]);/*Создание комментариев*/
	};/*Просмотр списка завершен*/
	return '';
};
/*Меняет статус на следующий, редактирет ответсвенного и добавляет комментарии.*/
def stateChanger = {scall->
	def parentCall = utils.get(SCALLS_FQN, [
						'reference' : true,
						'service' : scall['service'],
						'shortDescr' : scall['shortDescr']
						]); /*Поиск запроса источника по параметрам*/
	def params = [:]; /*Параметры для изменения*/
	params.put('state', NEW_STATE);/*Установка целевого статуса*/
	/*Оценка*/
	params.put('firstLRaiting', parentCall?.firstLRaiting);
	/*Оценка*/
	params.put('secondLRaiting', parentCall?.secondLRaiting);
	/*Оценка*/
	params.put('userRaiting', parentCall?.userRaiting);
	/*Запрос закрывается клиентом*/
	params.put('closedByEmployee', scall?.clientEmployee);
	log("Устанавливаем КЕМ РЕШЕН сотрудник: ${scall?.responsibleEmployee?.title}");
	def flag = 0;
	/*Попытка изменить запрос*/
	try {
			utils.edit(scall, params);/*Редактируем запрос*/
			if(scall?.clientEmployee && parentCall) {				
				postComments(scall, parentCall);/*Добавляем комментарии пользователя*/
			}
			flag = 1;
		}
	catch(Exception e) {
		/*Если запрос не изменен, то описываем причину в логе.*/
		log("Запрос не изменен, причина: ${e.toString()}")
		}
	return flag;
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
/*Найдем все запросы с целевыми параметрами*/
SCALLS = scallFinder(SCALLS_FQN, SCALL_PARAMS);
log("Найдено ${SCALLS.size()} запросов");
def count = 0;
/*Произведем смену статуса у четверти запросов*/
for(def i = 0; i < SCALLS.size()/4; i++) {
	def scall = SCALLS[RANDOM.nextInt(SCALLS.size())];
	count += stateChanger(scall);
}
log("Статус изменен у ${count} запросов.")
return '';