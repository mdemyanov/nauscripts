//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def RANDOM = new Random(); /*Используется для получения рандомного числа*/
def SCALLS = [];/*Текущие эталонные запросы запросы в системе*/
def SCALLS_FQN = 'serviceCall';
def SCALL_PARAMS = ['reference' : true]; /*Параметры для поиска текущих запросов*/
def SCALL_ATTRS = [	'userCategories', 
					'shortDescr', 
					'extDescr', 
					'agreement',
					'service',
					'timeZone']; /*Атрибуты для копирования в новые запросы*/
def INTERVAL = api.types.newDateTimeInterval(5, 'DAY'); /*Временной интервал*/
//ФУНКЦИИ--------------------------------------------------------------
/*Ищет запросы по типу и параметрам */
def scallFinder = {scallFqn, params->
	def scalls = utils.find(scallFqn, params);
	return scalls;
};
/*Поиск нового клиента для запроса*/
def clientFinder = {scall->
	def employees = scall.agreement.recipients;/*Соглашение, по которому зарегистрирован запрос*/
	return employees[RANDOM.nextInt(employees.size())];/*Случайный получатель соглашения*/
};
/*Регистрация новых запросов*/
def scallReqister = {scall, s_attrs ->
	log("Начинаем регистрацию запроса");
	def params = [:];/*Параметры новго запроса*/
	def client = clientFinder(scall);
	log("Найден клиент: ${client.title}");
	SCALL_ATTRS.each{attr->
		params.put(attr, scall[attr]);
		log("установлен параметр ${attr} ")
	};
	//log("Проставляем отдел: ${client?.parent.title}");
	/*Контрагент - сотрудник*/
	params.put('client', client);
	log("Проставляем сотрудника: ${client.title}");
	def flag = 0;
	try {
		log("Регистрируем запрос класса: ${scall.metaClass}");
		utils.create(scall.metaClass, params);
		flag =  1;
	}
	catch(Exception e) {
		log("Регистрация не удалась: ${e.toString()}");
	}
	return flag;
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
SCALLS = scallFinder(SCALLS_FQN, SCALL_PARAMS);
def count = 0;
SCALLS.each{scall->
	count+=scallReqister(scall,SCALL_ATTRS);
};
log("Отработал скрипт автосоздания запросов. Созданно: ${count.toString()} объектов");
return '';