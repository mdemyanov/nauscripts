//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Скрипт предназначен для автоматической смены статусов запросов:
 * из статусов "На первой" и "На второй линии" в статус "Разрешен"
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def RANDOM = new Random(); /*Используется для получения рандомного числа*/
def SCALLS = [];/*Текущие запросы в системе*/
def SCALLS_FQN = 'serviceCall$userCalls';
def CURRENT_STATES = ['firstLine', 'secondLine'];/*Текущие сстатусы*/
def NEW_STATE = 'resolved'
def SCALL_PARAMS = ['state' : CURRENT_STATES]; /*Параметры для поиска текущих запросов*/
//ФУНКЦИИ--------------------------------------------------------------
/*Ищет запросы по типу и параметрам */
def scallFinder = {scallFqn, params->
	def scalls = utils.find(scallFqn, params);
	return scalls;
};
/*Меняет статус на следующий, редактирет ответсвенного и добавляет комментарии.*/
def stateChanger = {scall->
	def parentCall = utils.get(SCALLS_FQN, [
						'reference' : true,
						'service' : scall['service'],
						'shortDescr' : scall['shortDescr']
						]); /*Поиск запроса источника по параметрам*/
	def params = [:]; /*Параметры для изменения*/
	/*Установка ответственной команды*/
	params.put('solution', parentCall?.solution ?: 'Решение из базы знаний');
	/*Заполняем новый статус запроса*/
	params.put('state', NEW_STATE);
	/*Заполнем поле КЕМ РЕШЕН текущим ответсвенным*/
	params.put('solvedByTeam', scall?.responsibleTeam);
	log("Устанавливаем КЕМ РЕШЕН КОМАНДА: ${scall?.responsibleTeam.title}");
	params.put('solvedByEmployee', scall?.responsibleEmployee);
	log("Устанавливаем КЕМ РЕШЕН сотрудник: ${scall?.responsibleEmployee.title}");
	/*Описание решения*/
	def flag = 0;
	/*Попытка изменить запрос*/
	try {
			utils.edit(scall, params);/*Редактируем запрос*/
			flag = 1;
		}
	catch(Exception e) {
		/*Если запрос не изменен, то описываем причину в логе.*/
		log("Запрос не изменен, причина: ${e.toString()}")
		}
	return flag;
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
/*Найдем все запросы с целевыми параметрами*/
SCALLS = scallFinder(SCALLS_FQN, SCALL_PARAMS);
log("Найдено ${SCALLS.size()} запросов");
def count = 0;
/*Произведем смену статуса у четверти запросов*/
for(def i = 0; i < SCALLS.size()/4; i++) {
	def scall = SCALLS[RANDOM.nextInt(SCALLS.size())];
	if ((scall.metaClass.getCase() == 'consultation') && (scall.state == 'firstLine') ||
		(scall.metaClass.getCase() != 'consultation') && (scall.state != 'firstLine')) {
		log("Изменяем статус запроса ${scall.title}");
		count += stateChanger(scall);
	}	
}
log("Статус изменен у ${count} запросов.")
return '';