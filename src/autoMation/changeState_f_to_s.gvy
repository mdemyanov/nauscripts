//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Скрипт предназначен для автоматической смены статусов запросов:
 * из статуса "Зарегистрирован" в статус "На первой линии"
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def RANDOM = new Random(); /*Используется для получения рандомного числа*/
def SCALLS = [];/*Текущие запросы в системе*/
def SCALLS_FQN = 'serviceCall$userCalls';
def CURRENT_STATES = ['firstLine'];/*Текущие сстатусы*/
def NEW_STATE = 'secondLine'
def SCALL_PARAMS = ['state' : CURRENT_STATES]; /*Параметры для поиска текущих запросов*/
//ФУНКЦИИ--------------------------------------------------------------
/*Ищет запросы по типу и параметрам */
def scallFinder = {scallFqn, params->
	def scalls = utils.find(scallFqn, params);
	return scalls;
};
/*Получение комментариев*/
def getComments = {scall, team->
	def comments = utils.comments(scall);
	def resultComments = [];
	// оставляем только комментарии первой линии
	log("Поиск комментариев ${team.title}");
	comments.each{comment->
		team.members.contains(comment.author) ? 
		resultComments.add(comment) : log("Пропускаем комментарий");
	};
	return resultComments;
};
/*Постинг комментариев*/
def postComments = {responsibleEmployee, scall, parentCall, team->
	def comments = getComments(parentCall, team);
	/*Просмотр списка*/
	comments.each{comment->
		utils.create('comment', ['source' : scall.UUID, 
			'text' : comment.text, 
			'author' : responsibleEmployee]);/*Создание комментариев*/
	};/*Просмотр списка завершен*/
	return '';
};
/*Меняет статус на следующий, редактирет ответсвенного и добавляет комментарии.*/
def stateChanger = {scall, team->
	def parentCall = utils.get(SCALLS_FQN, [
						'reference' : true,
						'service' : scall['service'],
						'shortDescr' : scall['shortDescr']
						]); /*Поиск запроса источника по параметрам*/
	def params = [:]; /*Параметры для изменения*/
	def members = team.members;/*Сотрудники команды*/
	def responsibleEmployee = members[RANDOM.nextInt(members.size())];/*Случайный сотрудник*/
	params.put('state', NEW_STATE);/*Установка целевого статуса*/
	/*Установка ответственного сотрудника*/
	params.put('responsibleEmployee', responsibleEmployee);
	/*Установка ответственной команды*/
	params.put('responsibleTeam', team);
	/*Описание решения*/
	def flag = 0;
	/*Попытка изменить запрос*/
	try {
			utils.edit(scall, params);/*Редактируем запрос*/
			postComments(responsibleEmployee, scall, parentCall, team);/*Добавляем комментарии специалистов*/
			flag = 1;
		}
	catch(Exception e) {
		/*Если запрос не изменен, то описываем причину в логе.*/
		log("Запрос не изменен, причина: ${e.toString()}")
		}
	return flag;
};
//ОСНОВНОЙ БЛОК------------------------------------------------------------
/*Найдем все запросы с целевыми параметрами*/
SCALLS = scallFinder(SCALLS_FQN, SCALL_PARAMS);
log("Найдено ${SCALLS.size()} запросов");
def count = 0;
/*Произведем смену статуса у четверти запросов*/
for(def i = 0; i < SCALLS.size()/4; i++) {
	def scall = SCALLS[RANDOM.nextInt(SCALLS.size())];
	if (scall.metaClass.getCase() != 'consultation') {
		count += stateChanger(scall, 
			scall.service.responsibleTeam);
	}	
}
log("Статус изменен у ${count} запросов.")
return '';