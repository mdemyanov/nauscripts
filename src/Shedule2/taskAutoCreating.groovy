// package Shedule2
//Автор: mdemyanov
//Дата создания: 21.11.2012
//Назначение: автоматическое создание работ при переходе в статус
/**
 * При переходе в статус "Работы запланированны" создает задачи на ресурсы по
 * конкретным датам (регламентную) на каждую КЕ
 * Если КЕ нет - прекращает работу.
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
/*сценарий выполняется,
Если расписание прикреплено к ресурсам*/
def resourses = subject?.resourses;
/*Используем только справочник 24х7*/
/*def SERVICE_TIME = utils.get('servicetime', ['code' : '24x7']);*/
/*Часовой пояс, берется из распивания*/
def TIME_ZONE = subject.timeZone;
/*Разница в объеме трех часов*/
def GRINVICH = 3 * 60 * 60 * 1000;
if (resourses) {
    /*Планировать начиная с даты*/
    def PLANFROM = subject.planFrom;
    /*Планировать не далее даты*/
    def PLANTO = subject.planTo;
    /*Интервал (период) планирования работ*/
    def INTERVAL = subject.schedulePeriod;
    def START = subject.startTime.code.toLong();
    def END = subject.endTime.code.toLong();
    def WORK_ATTRIBUTES = [ /*Определяем атрибуты для регистрируемой задачи*/
            'schedule': subject,/*Источник работы (расписание)*/
            'shortDeskr': subject.shortDescr,/*Описание работы (Краткое)*/
            'description': subject.workDescr,/*Описание работы*/
            'responsibleEmployee': subject.responsibleEmployee,/*Ответственный? копируется только
		сотрудник*/
            'responsibleTeam': subject.responsibleTeam/*Ответственный? копируется только
		сотрудник*/
    ];
//Основной блок------------------------------------------------------------
    /*Планирование рабор в рамках заданныхсроком с учетом
    заданного периода*/
    for (/*Начальня дата в начальную точку времени*/
            def currentDate = getCurrentDate(PLANFROM, TIME_ZONE);
            currentDate <= PLANTO; /*Пока не больше крайнего срока планирования*/
            currentDate = new Date(currentDate.getTime() + INTERVAL.toMiliseconds().toLong())) {
        /*Время начала работ: дата проведения + время проведения*/
        WORK_ATTRIBUTES['startTime'] = new Date(currentDate.getTime() + START);
        /*Время на выполнение работ: разница начального и конечного значений*/
        WORK_ATTRIBUTES['timeToDo'] = api.types.newDateTimeInterval(getInterval(START, END).toLong(), 'SECOND');
        /*Для каждого ресурса, прикрепленного к
        расписанию создаем задачу типа "Регламентная работа*/
        resourses.each({ resourse ->
            WORK_ATTRIBUTES['resourse'] = resourse;/*Заполняем атрибут "Ресурс"*/
            utils.create('task$workBySchedule', WORK_ATTRIBUTES);/*Создаем задачу
				типа "Регламентная работа*/
        });
    }
    return '';
}
return '';
/*Возвращает разниу между датой начала проведения работ */

def getInterval(def start, def end) {
    def value = end - start;
    return (value > 0) ? value * 0.001 : (24 * 60 * 60 * 1000 - value) / 1000;
}
/*Функция для вычисления даты относительно конкретного часового пояса*/
/*Функция принимает на вход: дату, ссылку на элемент справочника "Часовой пояс",
возвращает дату(дату+время)*/

def getCurrentDate(date, timeZone) {
    /*Часовой пояс сервера: разница с GMT в ms*/
    def defaultTimeZone = TimeZone.'default';
    /*Часовой пояс : разница с GMT в ms*/
    def currentTimeZone = TimeZone.getTimeZone(timeZone.code);
    /*Разница между часовыми поясами*/
    Long diffTimeZones = defaultTimeZone.rawOffset - currentTimeZone.rawOffset;
    /*Возвращаем дату/время для часового посяса*/
    return new Date(date.getTime() + diffTimeZones);
}