// package Shedule2
//Автор: mdemyanov
//Дата создания: 01.02.2013
//Назначение:
/**
 * Перевод задачи в  состояние ожидания
 * при условии ее нахождения в состоянии планирования
 */
//Версия: 4.0
//Категория: Действия по событию типа скрипт (../groovy/prod/eventactions/script)
//ПАРАМЕТРЫ------------------------------------------------------------
def TASK = subject;
def CURRENT_STATE = subject.state;
def NEW_STATE = 'wait';
//Основной блок--------------------------------------------------------
if (CURRENT_STATE == 'registered') {
    utils.edit(TASK, ['state': NEW_STATE]);
}
return '';