//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: вычисление роли
/**
 * Выччисляет, имеет ли пользователь доступ к разделу БЗ:
 * если пользователь является получателм услуги по одному из соглашений,
 * то возвращаем тру.
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
//Основной блок----------------------------------------------------
	def myQuery = api.db.query("""
select employee.title, (
	select 
	case
	when count(callResponsibl.id) is null then '0' 
	else count(callResponsibl.id) 
	end
	from employee.callResponsibl callResponsibl
	) as scalls, 
	(select 
	case
	when count(activeTasks.id) is null then '0' 
	else count(activeTasks.id) 
	end
	from employee.activeTasks activeTasks
	where activeTasks.state not in ('closed', 'resolved', 'registered')) as tasks
from team as team
inner join team.members employee
where team.id = '2603'
order by scalls, tasks
	        """)
	def result = myQuery.list()[7].toString();
	return result;