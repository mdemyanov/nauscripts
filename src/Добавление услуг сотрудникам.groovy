def agreements = utils.find('agreement', [:])
for (def agreement in agreements) {
    for (def user in agreement.recipients) {
        def newServises = []
        newServises.addAll(user.services)
        newServises.addAll(agreement.services)
        utils.edit(user, ['services': newServises])
    }
}
