//Автор: mdemyanov
//Дата создания: 05.09.2013
//Код: PartNumber из naupp
//Назначение:
/**
 * Копирует значения текущийх настроек SLA в таблицу соответствий
 */
//Версия: 4.0.1.x
//Категория: Скрипт на вход в статус
// Логгирование:
/*По умолчанию логгирование выключено*/
def LOGGING_IS_ENABLED = false;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
/*Обределяем текущий объект*/
def OBJECT = subject;
/*Определяем родительское соглашение*/
def AGREEMENT = OBJECT['parent'];
/*Определяем изменяемую таблицу соответствий*/
def SYSTEMTABLE = utils.get('rulesSettings', ['code': 'resolutionTime_new']);
/*Соответствие атрибутов, копируемых из правила (key) в таблицу соответствий (value)*/
def ATTRS = ['resolutionTime': 'resolutionTime',
        'categories': 'categories',
        'impact': 'impact',
        'urgency': 'speed',
        'service': 'services'];
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК--------------------------------------------------------
def oldRules = utils.find('agreementRule$agreementRule', ['parent': AGREEMENT]);
//определим строки (элементы) правила соответствий
def rows = [].withDefault { [:] };
// Найдем все правила по текущему соглашению
def agreementRules = oldRules + OBJECT;
//Произведем запись всех текущих настроек в таблицу соответствий
agreementRules.eachWithIndex { rule, i ->
    // Произведем копирование атрибутов во временный контейнер строк
    ATTRS.each { attr ->
        /*Действие по копированию одного атрибута*/
        rows[i].put(attr.key, rule[attr.value]);
    };
    /*Запишем занчение метакласса объекта (запроса)*/
    rows[i].put('metaClass', api.types.newClassFqn('serviceCall$' + rule.typesSC.code));
}
/*Попытка записать измененные значения в таблицу*/
try {
    utils.edit(SYSTEMTABLE, ['rowSet': rows]);
}
/*Сообщение об ошибке. Выводится пользователю в случае неудачи*/
catch (Exception e) {
    utils.throwReadableException("Сообщение об ошибке: ${e.toString()}.\n");
}
/*Конец сценария*/
return '';