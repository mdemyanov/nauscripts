select srv.title,
srv.id,
case 
	when sc.case_id = 'incidentCall' then 'Инцидент' else 
	case 
		when sc.case_id = 'accessRequest' then 'Запрос доступа' else
			case 
				when sc.case_id = 'rerviceReq' then 'Запрос на обслуживание' else
					case 
						when sc.case_id = 'advice' then 'Консультация' else 'Другое' end sctype,
from tbl_slmservice srv
join tbl_servicecall sc on sc.service_id = srv.id
where srv.state not in ['closed','resolved']