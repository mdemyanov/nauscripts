select 
sender_uuid,
count(distinct date_trunc('day',event_date))
from tbl_event
where event_category = 'loginSuccessful' and sender_uuid like 'superUser$%'
and date_trunc('day',event_date)>=to_date('01.03.2013','dd.mm.yyyy')
group by sender_uuid
order by sender_uuid