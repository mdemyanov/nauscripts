//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def laptops = utils.find('KE$laptop', [:]);
def empls = utils.find('employee', [:]);
def i = 0;
def employee = empls.get(0);
try {
    laptops.each({
        utils.edit(it, ['ownedBy_em': employee, 'ownedBy_ou': employee.parent]);
        i++;
        employee = empls.get(i);
    });
}
catch (Exception e) {
    logger.info(e);
}
return '$i';