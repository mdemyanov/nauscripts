//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Расчет баллансовой стоимости (остаточной)
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
/*Логгирование сообщений в консоли*/
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg)
}
log("Начинаем подсчет остаточной стоимости");
/*Основные переменные*/
def ACCAUNT_OBJECT = subject;
def BALLANCE_POST = ACCAUNT_OBJECT['ballancePost']; /*Атрибут дата введения в эксплуотацию*/
def START_COST = ACCAUNT_OBJECT['startCost']; /*Атрибут начальная стоимость*/
double DEPRETIATION_PERIOD = 30*24*60*60; /*Период списания в 30 календарных дней*/
def DEPRETIATION_MONTH = ACCAUNT_OBJECT['deprecaitMonth'];
def CURRENT_DATE = new Date(); /*Текущая дата*/
double TOTAL_DEPRETIATION_COST = 0.0;
//Основной блок------------------------------------------------------------
log("Переходим к вычислениям");

try {
	double totalTime = (CURRENT_DATE.getTime() - BALLANCE_POST.getTime())/1000;
	log('totalTime = ' + totalTime.toString());
	double totalDepretiationCount = totalTime/DEPRETIATION_PERIOD;
	log('totalDepretiationCount = ' + totalDepretiationCount.toString());
	TOTAL_DEPRETIATION_COST = START_COST - START_COST*DEPRETIATION_MONTH*totalDepretiationCount*0.01;	
}
catch(Exception e) {
	log(e.toString());
}
log('TOTAL_DEPRETIATION_COST = ' + TOTAL_DEPRETIATION_COST.toString() + 'round' + 
		TOTAL_DEPRETIATION_COST.round(3).toString());
return (TOTAL_DEPRETIATION_COST > 0 ? TOTAL_DEPRETIATION_COST.round(3) : 0.00);