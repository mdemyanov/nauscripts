//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Расчет баллансовой стоимости (начальной)
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
/*Логгирование сообщений в консоли*/
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg)
}
log("Начинаем подсчет");
/*Основные переменные*/
def ACCAUNT_OBJECT = subject;
def UNITS_CODE = 'units'; /*Обеспечение - составляющие компоненты*/
def COMPONENTS_CODE = 'components'; 
def FIRST_LEVEL_UNITS = ACCAUNT_OBJECT[UNITS_CODE]; /*Колеекция компонентов первого уровня*/
def SECOND_LEVEL_UNITS = []; /*Колеекция компонентов первого уровня*/
def COST_CODE = 'cost'; /*Код атрибута стоимости компонента*/
double TOTAL_COST = 0.0;
//ПАРАМЕТРЫ------------------------------------------------------------
FIRST_LEVEL_UNITS.each{unit->
	try {
		SECOND_LEVEL_UNITS.addAll(unit[COMPONENTS_CODE]);
		log('Добавление компонентов');
	}
	catch(Exception e) {
		log(e.toString());
	}
	
}
def totalCost = {units->
	double cost = 0.0;
	units.each{unit->
			cost+=unit[COST_CODE];
			log('Подсчет стоимости');
		}
	return cost;
	log('Стоимость рассчитанна');
}
try {
	TOTAL_COST+=totalCost(FIRST_LEVEL_UNITS);
	TOTAL_COST+=totalCost(SECOND_LEVEL_UNITS);
}
catch(Exception e) {
	log(e.toString());
}
return TOTAL_COST;