//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * Расчет суммы ежемесячного списания
 */
//Версия: 4.0
//Категория: 
//ПАРАМЕТРЫ------------------------------------------------------------
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
log("Отработка назначения ответственного");
def OBJECT = subject;
def RESONSIBLE_TEAM = OBJECT.responsibleTeam;
def CLIENT_HEAD = OBJECT.clientOU?.parent?.head;
def RESONSIBLE_EMPLOYEE = CLIENT_HEAD;
//Основной блок------------------------------------------------------------
try {
	utils.edit(OBJECT, ['responsibleEmployee' : RESONSIBLE_EMPLOYEE]);
}
catch(Exception e) {
	log(e);
}
return '';