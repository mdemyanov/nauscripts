// Сборник скриптов для создания этапов работ
// для запроса "Запрос на изменение" процесса
// "Управление изменениями"

// Скрипт на автоматическое создание объекта типа работ "Планирование изменения"
// Текущая дата
def currentDate = new Date()
// Атрибут "Ответственная команда"
def resposibleTeam = 'resposibleTeam'
// Атрибут "Название этапа работ"
def taskTitle = 'title'
//Атрибут "Плановая дата начала работ"
def startTime = 'startTime'
// Атрибут "Плановая дата завершения работ"
def deadLine = 'deadLineTime'
// Автор этапа работ
def taskAutor = 'autor'
// Запрос - родитель
def serviceCall = 'serviceCall'
// Конфигурационная единица
def ci = 'relToKe'
//Описание задачи
def taskDescription = 'taskDescription'
// Переводим время в милисекунды
def ms = subject.resolutionTime.toMiliseconds()
//Создаем переменную, которая хранит необходимый временной интервал умноженный на коэффициент
def resolTime25 = (ms * 0.25) / 1000 / 60
// Создаем новый временной промежуток
def interval = new ru.naumen.common.shared.utils.DateTimeInterval(resolTime25.longValue(), 'MINUTE')
// Редактируем новый временной промежуток
// serviceTime элемент или код элемента справочника "Классы обслуживания" 
def SERVICE_TIME = 'serviceTime';
// timeZone элемент или код элемента справочника "Часовые пояса"
// startTime время от которого необходимо начать отсчитывать период обслуживания (Date)
// interval период обслуживания (кол-во милисекунд, целое число)
def endTime = api.timing.serviceEndTime(subject[SERVICE_TIME], subject.timeZone, currentDate, interval.toMiliseconds())
// Массив атрибутов
def attrs = [:]
attrs[resposibleTeam] = subject.service.responsibleTeam
attrs[taskTitle] = "Планирование"
attrs[taskDescription] = "Задача подготовки плана изменения и отката"
attrs[serviceCall] = subject
attrs[ci] = subject.relToKE
attrs[taskAutor] = user
attrs[startTime] = currentDate
attrs[deadLine] = endTime
utils.create('task$stage', attrs)
//=========================================================================
// Заполнение реальной даты закрытия задачи (для действий по событиям)
// Текущая дата
def currentDate = new Date();
// Редактируем фактическую дату завершения задачи
utils.edit(subject, ['factClosedTime', currentDate])
return 'ok'
//=========================================================================
if (subject.state = 'complete')
{
    def currentDate = new Date();
    // Редактируем фактическую дату завершения задачи
    utils.edit(subject, ['factClosedTime', currentDate])
    return 'ok'
}
else return 'not complete'

//=========================================================================
//Создание этапа работ по сборке изменения
// Скрипт на автоматическое создание объекта типа работ "Сборка изменения"
// Текущая дата
def currentDate = new Date();
// Атрибут "Ответственная команда"
def resposibleTeam = 'resposibleTeam'
// Атрибут "Название этапа работ"
def taskTitle = 'title'
//Атрибут "Плановая дата начала работ"
def startTime = 'startTime'
// Атрибут "Плановая дата завершения работ"
def deadLine = 'deadLineTime'
// Автор этапа работ
def taskAutor = 'autor'
// Запрос - родитель
def serviceCall = 'serviceCall'
// Конфигурационная единица
def ci = 'relToKe'
//Описание задачи
def taskDescription = 'taskDescription'
// Переводим время в милисекунды
def ms = subject.resolutionTime.toMiliseconds()
//Создаем переменную, которая хранит необходимый временной интервал умноженный на коэффициент
def resolTime20 = (ms * 0.20) / 1000 / 60
// Создаем новый временной промежуток
def interval = new ru.naumen.common.shared.utils.DateTimeInterval(resolTime20.longValue(), 'MINUTE')
// Редактируем новый временной промежуток
// serviceTime элемент или код элемента справочника "Классы обслуживания" 
def SERVICE_TIME = 'serviceTime';
// timeZone элемент или код элемента справочника "Часовые пояса"
// startTime время от которого необходимо начать отсчитывать период обслуживания (Date)
// interval период обслуживания (кол-во милисекунд, целое число)
def endTime = api.timing.serviceEndTime(subject[SERVICE_TIME], subject.timeZone, currentDate, interval.toMiliseconds())
// Массив атрибутов
def attrs = [:]
attrs[resposibleTeam] = subject.service.responsibleTeam
attrs[taskTitle] = "Сборка"
attrs[taskDescription] = "Задача подготовки и сборки новой версии"
attrs[serviceCall] = subject
attrs[ci] = subject.relToKE
attrs[taskAutor] = user
attrs[startTime] = currentDate
attrs[deadLine] = endTime
utils.create('task$stage', attrs)
//=========================================================================
//Создание этапа работ по тестированию изменения
// Скрипт на автоматическое создание объекта типа работ "Тестированию изменения"
// Текущая дата
def currentDate = new Date();
// Атрибут "Ответственная команда"
def resposibleTeam = 'resposibleTeam'
// Атрибут "Название этапа работ"
def taskTitle = 'title'
//Атрибут "Плановая дата начала работ"
def startTime = 'startTime'
// Атрибут "Плановая дата завершения работ"
def deadLine = 'deadLineTime'
// Автор этапа работ
def taskAutor = 'autor'
// Запрос - родитель
def serviceCall = 'serviceCall'
// Конфигурационная единица
def ci = 'relToKe'
//Описание задачи
def taskDescription = 'taskDescription'
// Переводим время в милисекунды
def ms = subject.resolutionTime.toMiliseconds()
//Создаем переменную, которая хранит необходимый временной интервал умноженный на коэффициент
def resolTime20 = (ms * 0.20) / 1000 / 60
// Создаем новый временной промежуток
def interval = new ru.naumen.common.shared.utils.DateTimeInterval(resolTime20.longValue(), 'MINUTE')
// Редактируем новый временной промежуток
// serviceTime элемент или код элемента справочника "Классы обслуживания" 
def SERVICE_TIME = 'serviceTime';
// timeZone элемент или код элемента справочника "Часовые пояса"
// startTime время от которого необходимо начать отсчитывать период обслуживания (Date)
// interval период обслуживания (кол-во милисекунд, целое число)
def endTime = api.timing.serviceEndTime(subject[SERVICE_TIME], subject.timeZone, currentDate, interval.toMiliseconds())
// Массив атрибутов
def attrs = [:]
attrs[resposibleTeam] = subject.service.responsibleTeam
attrs[taskTitle] = "Тестирование"
attrs[taskDescription] = "Задача тестирования новой версии"
attrs[serviceCall] = subject
attrs[ci] = subject.relToKE
attrs[taskAutor] = user
attrs[startTime] = currentDate
attrs[deadLine] = endTime
utils.create('task$stage', attrs)
//=========================================================================
//Создание этапа работ по внедрению изменения
// Скрипт на автоматическое создание объекта типа работ "Тестированию изменения"
// Текущая дата
def currentDate = new Date();
// Атрибут "Ответственная команда"
def resposibleTeam = 'resposibleTeam'
// Атрибут "Название этапа работ"
def taskTitle = 'title'
//Атрибут "Плановая дата начала работ"
def startTime = 'startTime'
// Атрибут "Плановая дата завершения работ"
def deadLine = 'deadLineTime'
// Автор этапа работ
def taskAutor = 'autor'
// Запрос - родитель
def serviceCall = 'serviceCall'
// Конфигурационная единица
def ci = 'relToKe'
//Описание задачи
def taskDescription = 'taskDescription'
// Переводим время в милисекунды
def ms = subject.resolutionTime.toMiliseconds()
//Создаем переменную, которая хранит необходимый временной интервал умноженный на коэффициент
def resolTime20 = (ms * 0.20) / 1000 / 60
// Создаем новый временной промежуток
def interval = new ru.naumen.common.shared.utils.DateTimeInterval(resolTime20.longValue(), 'MINUTE')
// Редактируем новый временной промежуток
// serviceTime элемент или код элемента справочника "Классы обслуживания" 
def SERVICE_TIME = 'serviceTime'
// timeZone элемент или код элемента справочника "Часовые пояса"
// startTime время от которого необходимо начать отсчитывать период обслуживания (Date)
// interval период обслуживания (кол-во милисекунд, целое число)
def endTime = api.timing.serviceEndTime(subject[SERVICE_TIME], subject.timeZone, currentDate, interval.toMiliseconds())
// Массив атрибутов
def attrs = [:]
attrs[resposibleTeam] = subject.service.responsibleTeam
attrs[taskTitle] = "Применение изменения"
attrs[taskDescription] = "Задача внедрения новой версии"
attrs[serviceCall] = subject
attrs[ci] = subject.relToKE
attrs[taskAutor] = user
attrs[startTime] = currentDate
attrs[deadLine] = endTime
utils.create('task$stage', attrs)
//=========================================================================
// Условие: задача планирования должна быть выполнена
def serviceCall = 'serviceCall'
def title = 'title'
def state = 'state'
def attrs = [:]
attrs[serviceCall] = subject
attrs[title] = "Сборка"
attrs[state] = 'closed'
//def stage = utils.find('task$stage', attrs)
return utils.find('task$stage', attrs) ? '' : "Задача планирования еще не закрыта"
// Условие: задача сборки должна быть выполнена
def serviceCall = 'serviceCall'
def title = 'title'
def state = 'state'
def attrs = [:]
attrs[serviceCall] = subject
attrs[title] = "Сборка"
attrs[state] = 'closed'
//def stage = utils.find('task$stage', attrs)
return utils.find('task$stage', attrs) ? '' : "Задача сборки еще не закрыта"
// Условие: задача тестирования должна быть выполнена
def serviceCall = 'serviceCall'
def title = 'title'
def state = 'state'
def attrs = [:]
attrs[serviceCall] = subject
attrs[title] = "Тестирование"
attrs[state] = 'closed'
//def stage = utils.find('task$stage', attrs)
return utils.find('task$stage', attrs) ? '' : "Задача тестирования еще не закрыта"
// Условие: задача применения должна быть выполнена
def serviceCall = 'serviceCall'
def title = 'title'
def state = 'state'
def attrs = [:]
attrs[serviceCall] = subject
attrs[title] = "Применение изменения"
attrs[state] = 'closed'
//def stage = utils.find('task$stage', attrs)
return utils.find('task$stage', attrs) ? '' : "Задача внедрения еще не закрыта"
