// package beauty
/*
Настройка ролей:
Определение соответсвия сотрудника конкретной роли здадается условным выражением:
return (проверяем находится ли сотрудник в объекте) ? (проверяем соответсвие сотрудника роли) : (возвращаем логический ноль)
1. Проверка нахождения сотрудника в объекте обязательна, иначе система будет выдавать ошибку. Скорее всего это баг,
потому что скрипт выполняется, когда сотрудник находится в личном кабинете. Кроме того, скрипт будет выполняться внутри 
создаваемого экземпляра объекта (тогда subject = null).
2. Не могу объяснить, поэтому приведу пример: проверяем является ли текущий пользователь ответсвенным за запрос и возвращаем результат.
3. Возвращаем false, так как не выполнилось условие 1.
Ниже приведены примеры для УБОев:
*/
// Доступ к разделу БЗ:Разрешаем доступ для членов команды куратора сервиса, по которому предоставляется доступ в БЗ
return (subject) ? user.teams.contains(subject.service.responsibleTeam) : false
//----------------------------------------------------------------------------------------------------------------
// Просмотр раздела БЗ:Разрешаем доступ для получателей сервиса, по которому предоставляется доступ в БЗ
return (subject) ? subject.service.recipients.contains(user) : false
//=====================================================================
// Доступ к статье БЗ:Разрешаем доступ для членов команды куратора сервиса, по которому предоставляется доступ в БЗ
return (subject) ? user.teams.contains(subject.parent.service.responsibleTeam) : false
// Просмотр статьи БЗ:Разрешаем доступ для получателей сервиса
return (subject) ? subject.parent.service.recipients.contains(user) : false
//============Роли для задачи========================================
// Доступ к задаче: ответственный за задачу
// Здесь сравнивал по UUID, потому что сравнение самих объектов не давало результата
// либо приводило к ошибке. Возможно баг.
return (subject) ? subject.responsible_em.UUID == user.UUID : false
// Доступ к задаче: команда ответственная за задачу
return (subject) ? user.teams.contains(subject.responsible_te) : false
// Доступ к задаче: лидер команды, ответственной за задачу
// Здесь сравнивал по UUID, потому что сравнение самих объектов не давало результата
// либо приводило к ошибке. Возможно баг.
return (subject) ? user.UUID == subject.responsible_te.leader.UUID : false
// Доступ к задаче: пользователь, ответственный за запрос-источник
// Здесь сравнивал по UUID, потому что сравнение самих объектов не давало результата
// либо приводило к ошибке. Возможно баг.
return (subject) ? subject.serviceCall.responsibleEmployee.UUID == user.UUID : false
//============Роли для Согласований================================
// Доступ к согласованию: пользователь, ответственный за запрос-источник
// Здесь сравнивал по UUID, потому что сравнение самих объектов не давало результата
// либо приводило к ошибке. Возможно баг.
// ответсвенный за источник запрос
return ((subject) && (subject.metaClass.getCase() == 'callNegot')) ? (subject.serviceCall.responsibleEmployee == user) : false
// Ответсвенный за запрос источник. Использовать только для соответсвующего типа: Согласование по запросу
return (subject) ? subject.serviceCall.responsibleEmployee.UUID == user.UUID : false
// Правильно: ОТВЕТСВЕННЫЙ за источник
if (subject) {
    switch (subject.metaClass.getCase()) {
        case "callNegot":
            return (subject.serviceCall.responsibleEmployee == user) ? true : false
            break
        case "docFlowNegot":
            return (subject.docFlow.responsibleEmployee == user) ? true : false
            break
        default:
            return false;
            break
    }
}
// Доступ к согласованию: пользователь, участник согласующего комитета
return (subject) ? subject.cabEmploers.contains(user) : false

//============Роли для Голосований================================
// Доступ голосующему
// Здесь сравнивал по UUID, потому что сравнение самих объектов не давало результата
// либо приводило к ошибке. Возможно баг.
return (subject) ? subject.voter_em?.UUID == user.UUID : false
// Доступ Руководителю согласующего
// Здесь сравнивал по UUID, потому что сравнение самих объектов не давало результата
// либо приводило к ошибке. Возможно баг.
return (subject) ? subject.voter.immediateSupervisor.UUID == user.UUID : false
// Доступ в голосование ответсвенному за источник
// Здесь сравнивал по UUID, потому что сравнение самих объектов не давало результата
// либо приводило к ошибке. Возможно баг.
return (subject) ? subject.parent.serviceCall.responsibleEmployee.UUID == user.UUID == user.UUID : false
// Правильно: ОТВЕТСВЕННЫЙ за источник
if (subject) {
    switch (subject.parent.metaClass.getCase()) {
        case "callNegot":
            return (subject.parent.serviceCall.responsibleEmployee == user) ? true : false
            break
        case "docFlowNegot":
            return (subject.parent.docFlow.responsibleEmployee == user) ? true : false
            break
        default:
            return false;
            break
    }
}
//============Роли для Конфигурационных единиц и хранилищ================================
//============Роли для Хранилищ================================
//Ответственный сотрудник или команда
// Здесь сравнивал по UUID, потому что сравнение самих объектов не давало результата
// либо приводило к ошибке. Возможно баг.
return (subject) ? subject.responsible_em.UUID == user.UUID || user.teams.contains(subject.responsible_te) : false
//============Роли для Конфигурационных единиц================================
// Сотрудник ответсвенной команды
return (subject) ? user.teams.contains(subject.responsible) : false
// Владелец
return (subject) ? subject.owners.contains(user) : false
// Author
return (subject) ? (subject.author.UUID == user.UUID) : false