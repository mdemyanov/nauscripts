select
    vltable.impact,
    vltable.scallType,
    vltable.firstLine,
    vltable.secondLine,
    vltable.resolutionTime,
    AVG (scall.processingTimeTimerl)
    /
    3600000.0
from (SELECT
          vrow.id as rowid,
          MAX (CASE WHEN vattr.pos = '0' THEN vvalues.val ELSE NULL END) as firstLine,
          MAX (CASE WHEN vattr.pos = '1' THEN vvalues.val ELSE NULL END) as secondLine,
          MAX (CASE WHEN vattr.pos = '2' THEN vvalues.val ELSE NULL END) as resolutionTime,
          MAX (CASE WHEN vattr.pos = '3' THEN vvalues.val ELSE NULL END) as impact,
          MAX (CASE WHEN vattr.pos = '4' THEN vvalues.val ELSE NULL END) as scallType,
          MAX (CASE WHEN vattr.pos = '5' THEN vvalues.val ELSE NULL END) as service
      FROM tbl_valuemap
           as vmap
           JOIN tbl_valuemap_row
                as vrow on vmap.id = vrow.vmapitem_id
           JOIN tbl_valuemap_attr
                as vattr on vattr.vmapitem_id = vrow.vmapitem_id
           JOIN tbl_valuemap_values
                as vvalues on
                           vvalues.row_id = vrow.id and
                           vvalues.attr_id = vattr.id
      WHERE vmap.code = 'timingRule'
      GROUP BY vrow.id) vltable
     JOIN tbl_servicecall
          as scall on (
                      scall.resolutionTime
                      /
                      3600000) = regexp_replace (
                                                vltable.resolutionTime,
                                                '\D+',
                                                '') : :bigint GROUP BY vltable . impact , vltable . scallType , vltable . firstLine , vltable . secondLine , vltable . resolutionTime
