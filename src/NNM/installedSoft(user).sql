SELECT
    genericProperties.genericProperties$description,
    sysName.sysName$sysName,
    data.hrSWInstalledTable$hrSWInstalledType,
    data.hrSWInstalledTable$hrSWInstalledName
FROM users. * . devices . * :hrSWInstalledTable AS data INNER JOIN users . * . devices . * :genericProperties as genericProperties ON data$CONTEXT_ID = genericProperties$CONTEXT_ID INNER JOIN users . * . devices . * :sysName as sysName ON data$CONTEXT_ID = sysName$CONTEXT_ID where data . hrSWInstalledTable$hrSWInstalledName in ( 'Microsoft .NET Framework 4 Extended' , 'Microsoft Office Outlook MUI (Russian) 2010' , 'Microsoft .NET Framework 4 Client Profile' )