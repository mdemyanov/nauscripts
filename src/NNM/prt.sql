SELECT
    info.info$CONTEXT_ID AS context1,
    genericProperties.genericProperties$description,
    data.prtMarkerSuppliesTable$prtMarkerSuppliesDescription,
    CASEWHEN ((data.prtMarkerSuppliesTable$prtMarkerSuppliesSupplyUnit = 19 ) , data . prtMarkerSuppliesTable$prtMarkerSuppliesLevel , 1 ) as value
FROM users. * . devices . * :prtMarkerSuppliesTable AS data LEFT OUTER JOIN users . * . devices . * :info :contextStatus AS info ON data$CONTEXT_ID = info$CONTEXT_ID INNER JOIN users . * . devices . * :genericProperties as genericProperties ON data$CONTEXT_ID = genericProperties$CONTEXT_ID
