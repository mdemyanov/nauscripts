/*! UTF-8 */
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория:
/**
 * Категория: Стандартный скрипт обработки тревоги для SD4.0.
 * Назначение:
 * Создает запрос при возникновении тревоги в системе мониторинга
 * Описание запроса задается в скрипте, используется шаблон со следующими параметрами:
 *
 * <ul>
 *  <li>${event.context} - Тип тревоги </li>
 *  <li>${event.level}        - Уровень тревоги </li>
 *  <li>${event.creationtime} - Время срабатывания тревоги </li>
 *  <li>${description} - Описание тревоги из AG </li>
 *  <li>${context}     - Имя контекста на котором сработала тревога </li>
 *  <li>${entity}      - Имя таблицы значение из которойповлекло сработываение тревоги </li>
 *  <li>${cause}       - Текстовое описание причины сработки тревоги (окультуренный вид предыдущих двух параметров) </li>
 *  <li>${message}     - Сообщение сформированное AG </li>
 *  <li>${trigger}     - Тригер сработывания тревоги (может быть null) </li>
 * </ul>
 *
 * Версия: 4.0
 *
 * В контексте скрипта доступны сл. объекты:
 * <ul>
 * <li><b>event</b> - событие тревога</li>
 * <li><b>api</b> - API со вспомогательными методами, полезными при обработке</li>
 * <li><b>utils</b> - утилитарные методы общего назначения - ru.naumen.core.server.script.spi.ScriptUtils</li>
 * <li><b>logger</b> - логгер скрипта</li>
 * </ul>
 */
import ru.naumen.metainfo.shared.ClassFqn;
import ru.naumen.commons.shared.utils.StringUtilities;
import ru.naumen.core.shared.Constants;
import ru.naumen.core.server.script.ScriptService;

// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------


def DESCRIPTION_TEMPLATE = 'Описание тревоги: ${description}\n Событие: ${trigger}\n';
def RSOURSE = '${context}';

// уровень влияния, срочность, типы запроса, указанные ниже, должны быть настроены в системе
// код элемента справочника 'Уровни влияния'
def impactCode = "normal";

//код уровня срочности
def urgencyCode = "normal";

//код типа запроса
def typeCode = "incidentCall";

//уникальный номер соглашения
def agreementInventoryNumber = "00000";

//уникальный номер услуги
def serviceInventoryNumber = "112911";

// фамилия служебного сотрудника
def employeeName = "Служебный";


//ФУНКЦИИ--------------------------------------------------------------

//ОСНОВНОЙ БЛОК------------------------------------------------------------
def scriptService = beanFactory.getBean(ScriptService.class);


def getByInventoryNumber = {collection, inventoryNumber ->
    return collection.size() == 1 ? collection.iterator().next() : collection.find{it.inventoryNumber == inventoryNumber};
}

def eventProperty = { property ->
    return event.data.records.iterator().next().getString(property);
}

def renderTemplate = {description->
    def context = [
        'event'         : event,
        'description'   : eventProperty('description'),
        'context'       : eventProperty('context'),
        'entity'        : eventProperty('entity'),
        'cause'         : eventProperty('cause'),
        'message'       : eventProperty('message'),
        'trigger'       : eventProperty('trigger')
    ];
    return scriptService.runTemplate(description, context);
}

def createServiceCall = {
    // свойства запроса
    def props = [:];
    
    // Поиск сотрудника по адресу отправителя письма
    def employee = api.mail.helper.searchEmployeeByLastName(employeeName);
    if (employee == null)
    {
        log("Сотрудник не найден");
        return null;
    }
    // Сотрудник найден
    props.client = employee;

    // поиск соглашения
    def agreement = getByInventoryNumber(employee.recipientAgreements, agreementInventoryNumber);   
    if (agreement == null)
    {
        log("Не найдено соглашение для сотрудника с uuid = " + employee.UUID);
        return null;
    }
    // Соглашение найдено
    props.agreement = agreement;

    // Поиск Услуги
    def service = getByInventoryNumber(agreement.services, serviceInventoryNumber);
    if (service == null) {
        log("Не найдена услуга для сотрудника с uuid = " + employee.UUID + " и соглашения с uuid =  " + agreement.UUID);
        return null;
    }
    // Услуга найдена
    props.service = service;

    log("Регистрация запроса");
    // свойства запроса
    def fqn = new ClassFqn(Constants.ServiceCall.CLASS_ID, typeCode);
    props.put('metaClass', fqn);
    
    // описание запроса - рендерим по шаблону
    props.descriptionRTF = renderTemplate(DESCRIPTION_TEMPLATE);
    props.resources = utils.find('KE$Hardware', ['context' : renderTemplate(RSOURSE)]);
    // Временная зона для запроса - по умолчанию временная зона сервера
    def tzCode = TimeZone.getDefault().ID;
    props.timeZone = utils.get(Constants.TimezoneCatalogItem.CLASS_ID, [(Constants.CatalogItem.ITEM_CODE) : tzCode]);
    // Уровень влияния
    props.impact = utils.get(Constants.ImpactCatalog.ITEM_CLASS_ID, [(Constants.CatalogItem.ITEM_CODE) : impactCode]);
    // Срочность
    props.urgency = utils.get(Constants.UrgencyCatalog.ITEM_CLASS_ID, [(Constants.CatalogItem.ITEM_CODE) : urgencyCode]);
    // Контактная информация из данных сотрудника
    props.clientName = employee.title;
    props.clientPhone = employee.phonesIndex;
    props.clientEmail = employee.email;
    // формируем новый запрос
    return utils.create(fqn.toString(), props);
}

createServiceCall();