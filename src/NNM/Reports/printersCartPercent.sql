SELECT
    genericProperties.genericProperties$description,
    connectionProperties.connectionProperties$address,
    sysName.sysName$sysName,
    data.prtMarkerSuppliesTable$prtMarkerSuppliesDescription,
    data.prtMarkerSuppliesTable$prtMarkerSuppliesLevel
FROM users. * . devices . * :prtMarkerSuppliesTable AS data INNER JOIN users . * . devices . * :genericProperties as genericProperties ON data$CONTEXT_ID = genericProperties$CONTEXT_ID INNER JOIN users . * . devices . * :sysName as sysName ON data$CONTEXT_ID = sysName$CONTEXT_ID INNER JOIN users . * . devices . * :connectionProperties as connectionProperties ON data$CONTEXT_ID = connectionProperties$CONTEXT_ID WHERE data . prtMarkerSuppliesTable$prtMarkerSuppliesSupplyUnit = 19