SELECT 
  info.info$CONTEXT_ID AS context1,
  genericProperties.genericProperties$description,
  data.prtMarkerSuppliesTable$prtMarkerSuppliesDescription , 
  data.prtMarkerSuppliesTable$prtMarkerSuppliesSupplyUnit , 
  data.prtMarkerSuppliesTable$prtMarkerSuppliesMaxCapacity , 
  data.prtMarkerSuppliesTable$prtMarkerSuppliesLevel , 
  data.prtMarkerSuppliesTable$entryIndex 
 FROM
  users.*.devices.*:prtMarkerSuppliesTable AS data 
	LEFT OUTER JOIN 
  users.*.devices.*:info:contextStatus AS info
  	ON 
  data$CONTEXT_ID = info$CONTEXT_ID 
  	INNER JOIN 
  users.*.devices.*:genericProperties as genericProperties
  	ON 
  data$CONTEXT_ID = genericProperties$CONTEXT_ID
