//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение: фильтрация
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
def METACLASS = 'serviceCall$headHunting';
def PARAMS_TO_SEARCH = [:];
if(null == subject)
{ 
    def ATTRS_FOR_UPDATE_ON_FORMS = [''];
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
//ФУНКЦИИ--------------------------------------------------------------

//ОСНОВНОЙ БЛОК------------------------------------------------------------
return utils.find(METACLASS, PARAMS_TO_SEARCH);