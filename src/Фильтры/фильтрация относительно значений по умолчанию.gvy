//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * фильтрация относительно значений по умолчанию
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
if(null == subject)
{
    def ATTRS_FOR_UPDATE_ON_FORMS = ['metaClass'];
    return ATTRS_FOR_UPDATE_ON_FORMS;
}
def OBJECT = subject;
//ФУНКЦИИ--------------------------------------------------------------
//ОСНОВНОЙ БЛОК------------------------------------------------------------
def metaClass = api.metainfo.getMetaClass("serviceCall\$" + subject.metaClass);
def ATTR_DEF_VALUE = metaClass.getAttribute('categories').defaultValue;
return ATTR_DEF_VALUE;