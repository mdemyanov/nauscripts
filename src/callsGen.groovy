def i = 0
def descriptionRTF = 'descriptionRTF'
def clientEmployee = 'clientEmployee'
def clientOU = 'clientOU'
def agreement = 'agreement'
def service = 'service'
def attrs = [:]
attrs[descriptionRTF] = 'Генератор запросов'
attrs[clientEmployee] = utils.find('employee$businessUser', ["firstName": 'Инна'])
attrs[clientOU] = utils.find('ou$ou', ["title": 'Пользователи'])
attrs[agreement] = utils.find('agreement$SLA', ["title": 'Базовое соглашение'])
attrs[service] = utils.find('slmService', ["title": 'Интернет'])
utils.create('serviceCall$incidentCall', attrs)
