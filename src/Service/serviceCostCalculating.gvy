//Автор: mdemyanov
//Дата создания: 23.04.2013
//Назначение:
/**
 * При смене статуса собирает инфу 
 * о всех запросах, которые были 
 * зарегистрированны в указанный 
 * период по услуге и считает 
 * общие затраты на работы
 * по формуле: кол-во минут * стоимость часа работ по услуге.
 * КРоме того, собирает данные по месячной амортизации.
 */
//Версия: 4.0
//Категория: Скрипт на смену статуса
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
} 
//ПАРАМЕТРЫ------------------------------------------------------------
def OBJECT = subject;
def SERVICE = 'parent';
def START_TIME = 'startTime';
def END_TIME = 'endTime';
def cost = 0.0;
def workCost = 0.0;
def amorCost = 0.0;
def OBSERVE_WORK = 'observeWork';
def OBSERVE_AMOR = 'observeAmor';
//Получение начальных значений
def startTime = OBJECT[START_TIME];
def endTime = OBJECT[END_TIME];
def service = OBJECT[SERVICE];
/*Формирование запроса на подсчет трудозатрат*/
def QUERY = api.db.query("""
	select service.title, (
	select sum(calls.work)
	from service.serviceCalls as calls
	where calls.registrationDate between :startTime and :endTime
	) as scalls, 
	(select sum(ke.cost * ke.normAmor / 100)
	from service.undKE as ke) as amors
from slmService as service
where service.id = :service
	""");
QUERY.set('service', service.UUID);
QUERY.set('startTime', startTime);
QUERY.set('endTime', endTime);
def WORKS_RESULT = QUERY.list()[0] && OBJECT[OBSERVE_WORK] ? QUERY.list()[0][1] : 0;
def WORKS_RESULT_COST = WORKS_RESULT*service.costWork/60;
def AMOR_RESULT_COST = QUERY.list()[0] && OBJECT[OBSERVE_AMOR] ? QUERY.list()[0][2] : 0;
/*Заполнение значения*/
def RESULT = WORKS_RESULT_COST + AMOR_RESULT_COST;
utils.edit(OBJECT, ['cost' : RESULT, 
	'workCost' : WORKS_RESULT_COST, 
	'amorCost' : AMOR_RESULT_COST]);
return '';