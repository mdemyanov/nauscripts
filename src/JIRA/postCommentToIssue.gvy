//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 * 
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
/*Делаем импорты*/
import groovyx.net.http.RESTClient;
import groovy.util.slurpersupport.GPathResult;
import static groovyx.net.http.ContentType.JSON;
import org.apache.http.HttpRequest;
import org.apache.http.protocol.HttpContext;
import org.apache.http.HttpRequestInterceptor
/*Прописываем параметры и глобальные переменные*/
def OBJECT = subject;
def commentList = utils.comments(OBJECT)
/*Компания, где прописанны параметры подключения*/
def ROOT = utils.get('root', [:]);
/*Обращение к АПИ сервиса*/
def JIRA = new RESTClient('http://192.168.241.57:9080/rest/api/2/');
/*Параметры авторизации*/
def basic = 'Basic ' + 'administrator:123456'.bytes.encodeBase64().toString();
JIRA.client.addRequestInterceptor(
	new HttpRequestInterceptor() {
    void process(HttpRequest httpRequest, 
                 HttpContext httpContext) {
      httpRequest.addHeader('Authorization', basic)
  }
});
//ФУНКЦИИ--------------------------------------------------------------
def postComment = {comment,issueCode->
	try {
	log("Публикуем твит с текстом: ${comment} в ответ на ${issueCode}")
	return JIRA.post(path : "issue/${issueCode}/comment",
    requestContentType: JSON,
    body: [body: comment]);
	}
	catch(Exception e) {
	log("Не удалось опубликовать твит, причина: ${e.toString()}");
	return 'Репост неудался';
	}
};

//ОСНОВНОЙ БЛОК------------------------------------------------------------
def jira = postComment(commentList[commentList.size() - 1].text, OBJECT?.jiraKey);
return '';