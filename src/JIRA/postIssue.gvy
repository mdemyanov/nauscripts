
/**
 * Created with IntelliJ IDEA.
 * User: mdemyanov
 * Date: 15.10.13
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
//Автор: mdemyanov
//Дата создания: 02.07.2012
//Назначение:
/**
 *
 */
//Версия: 4.0
//Категория:
// Логгирование:
def LOGGING_IS_ENABLED = true;
def log = { msg ->
    if (LOGGING_IS_ENABLED)
        logger.info(msg);
}
//ПАРАМЕТРЫ------------------------------------------------------------
/*Делаем импорты*/
import groovyx.net.http.RESTClient;
import groovy.util.slurpersupport.GPathResult;
import static groovyx.net.http.ContentType.JSON;
import org.apache.http.HttpRequest;
import org.apache.http.protocol.HttpContext;
import org.apache.http.HttpRequestInterceptor
/*Прописываем параметры и глобальные переменные*/
def OBJECT = subject;
/*Компания, где прописанны параметры подключения*/
def ROOT = utils.get('root', [:]);
/*Обращение к АПИ сервиса*/
def JIRA = new RESTClient('http://192.168.241.57:9080/rest/api/2/');
/*Параметры авторизации*/
def basic = 'Basic ' + 'administrator:123456'.bytes.encodeBase64().toString();
JIRA.client.addRequestInterceptor(
        new HttpRequestInterceptor() {
            void process(HttpRequest httpRequest,
                         HttpContext httpContext) {
                httpRequest.addHeader('Authorization', basic)
            }
        });
//ФУНКЦИИ--------------------------------------------------------------
def postIssue = {title, extDescr->
    try {
        
        return JIRA.post(path : "issue/",
                requestContentType: JSON,
                body: [fields: [
                        project: [key: "TP"],
                        summary: title,
                        description: extDescr,
                        issuetype: [name: 'Task']
                ]]
        );
    }
    catch(Exception e) {
        log("Не удалось опубликовать твит, причина: ${e.toString()}");
        return 'Репост неудался';
    }
};
def scallEdit = {scall, data->
	def jiraLink = api.types.newHyperlink(data.key, "http://192.168.241.57:9080/browse/${data.key}" );
	try {
		utils.edit(scall, [
			jiraId: data.id,
			jiraLink: jiraLink,
			jiraKey: data.key])
	}
	catch(Exception e) {
		log("Ошибка: ${e.toString()}")
	}	
};

//ОСНОВНОЙ БЛОК------------------------------------------------------------
def jira = postIssue(OBJECT?.title, OBJECT?.extDescr);
def data = jira.data;
scallEdit(OBJECT, data);
return '';