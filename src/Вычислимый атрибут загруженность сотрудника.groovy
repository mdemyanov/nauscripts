def calls = utils.find('serviceCall', ["responsibleEmployee": subject])
int i = 0
for (def call in calls) {
    i = i + call.weight
}
def load = "high"
switch (i) {
    case 1..10:
        load = "veryLow"
        break
    case 11..20:
        load = "low"
        break
    case 21..30:
        load = "normal"
        break
    case 31..40:
        load = "high"
        break
    case i > 40 || i < 100:
        load = "veryHigh"
        break
    case 0:
        load = "0"
}
return utils.get('loadof', ["code": load])